/*
	eslint-disable
	filenames/match-exported,
	filenames/match-regex,
	unicorn/prevent-abbreviations
*/

import {
	NightwatchBrowser,
	NightwatchTests
} from 'nightwatch';

const test: NightwatchTests = {
	'Render test' (browser: NightwatchBrowser): void {
		browser
			.url(browser.launch_url)
			.waitForElementVisible('body', 1000)
			.resizeWindow(1024, 768)
			// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
			// @ts-ignore
			.assert.screenshotIdenticalToBaseline(
				'#app',
				undefined,
				// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
				// @ts-ignore
				{
					threshold: 0.3
				}
			)
			// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
			// @ts-ignore
			.end();
	}
};

export default test;
