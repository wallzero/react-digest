// eslint-disable-next-line jsdoc/no-bad-blocks
/*
	eslint-disable
	import/unambiguous,
	import/no-commonjs,
	@typescript-eslint/no-var-requires
*/

// eslint-disable-next-line canonical/filename-match-exported,@typescript-eslint/no-require-imports
const jest = require('@digest/jest');

jest.testEnvironment = 'jsdom';

module.exports = jest;
