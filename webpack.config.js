/*
	eslint-disable
	import/no-commonjs,
	canonical/filename-match-exported,
	canonical/filename-match-regex
*/

// eslint-disable-next-line @typescript-eslint/no-require-imports,@typescript-eslint/no-var-requires
const path = require('path');
// eslint-disable-next-line @typescript-eslint/no-require-imports,@typescript-eslint/no-var-requires
const webpackDigest = require('@digest/webpack');

webpackDigest.resolve.alias = {
	'@babel/runtime': path.resolve(
		__dirname,
		'node_modules',
		'@babel',
		'runtime'
	),
	'react-is': path.resolve(
		__dirname,
		'node_modules',
		'react-is'
	)
};

module.exports = webpackDigest;
