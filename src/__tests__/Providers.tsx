import {
	render
} from '@testing-library/react';
import React from 'react';
import Providers from '../Providers';

describe(
	'index HOC',
	(): void => {
		it(
			'root',
			(): void => {
				const component = render(
					<Providers />
				);
				expect(component).toBeDefined();
			}
		);
	}
);
