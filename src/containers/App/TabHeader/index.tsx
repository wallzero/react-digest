import {
	connect
} from 'react-redux';
import {
	onTabSelect
} from 'src/redux/actions/tabSettings';
import defaultState from 'src/redux/defaultState';
import TabHeader from './TabHeader';
import type {
	TabHeaderProps
} from './TabHeader';

const mapDispatchToProps = {
	onTabSelect
};

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		children
	}: Pick<
	Omit<TabHeaderProps, keyof typeof mapDispatchToProps>,
	'children'
	>
): Omit<TabHeaderProps, keyof typeof mapDispatchToProps> => {
	return {
		children,
		orientation: state.orientation,
		tabIndex: state.tabSettings.index
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TabHeader);

export {
	mapStateToProps
};
