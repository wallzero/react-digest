import {
	faPlusCircle as PlusCircleIcon
} from '@fortawesome/free-solid-svg-icons/faPlusCircle';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import Fab from '@mui/material/Fab';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import type {
	TabProps
} from 'ui-router-react-digest';
import './AddTab.css';

const styles = {
	wrap: {
		mx: 0.5,
		my: 0
	}
};

const AddTab: FunctionComponent<AddTabProps> = (
	{
		onTabAdd,
		tabIndex
	}: AddTabProps
): ReactElement => {
	const handleClick = useCallback(
		(): void => {
			onTabAdd(
				undefined,
				tabIndex
			);
		},
		[
			onTabAdd,
			tabIndex
		]
	);

	return (
		<Fab
			color='primary'
			data-testid='page-add-default-tab'
			onClick={handleClick}
			size='small'
			styleName='wrap'
			sx={styles.wrap}
		>
			<FontAwesomeIcon
				icon={PlusCircleIcon}
			/>
		</Fab>
	);
};

export default AddTab;

export type AddTabProps = {
	onTabAdd: (
		tab?: Omit<TabProps, 'name'> & {name?: string},
		tabIndex?: number
	) => void;
	tabIndex: number;
};
