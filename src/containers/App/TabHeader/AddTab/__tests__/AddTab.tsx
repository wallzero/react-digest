import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import AddTab from '../AddTab';

const onTabAdd = jest.fn();

describe(
	'Add tab',
	(): void => {
		beforeEach(
			(): void => {
				onTabAdd.mockReset();
			}
		);

		it(
			'add new default tab',
			async (): Promise<void> => {
				expect.assertions(2);

				const tabIndex = 1;

				const {
					findByTestId
				} = render(
					<AddTab
						onTabAdd={onTabAdd}
						tabIndex={tabIndex}
					/>
				);

				const button = await findByTestId('page-add-default-tab');

				fireEvent.click(button);

				expect(onTabAdd.mock.calls).toHaveLength(1);

				expect(onTabAdd.mock.calls[0][1]).toBe(tabIndex);
			}
		);
	}
);
