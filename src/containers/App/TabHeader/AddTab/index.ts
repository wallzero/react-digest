import {
	connect
} from 'react-redux';
import {
	onTabAdd
} from 'src/redux/actions/tabs';
import defaultState from 'src/redux/defaultState';
import AddTab from './AddTab';
import type {
	AddTabProps
} from './AddTab';

const mapDispatchToProps = {
	onTabAdd
};

const mapStateToProps = (
	state = defaultState
): Omit<AddTabProps, keyof typeof mapDispatchToProps> => {
	return {
		tabIndex: state.tabOrder.length
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddTab);

export {
	mapStateToProps
};
