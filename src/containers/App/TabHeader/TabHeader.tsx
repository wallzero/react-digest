import type {
	Theme
} from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Toolbar from '@mui/material/Toolbar';
import type {
	SxProps
} from '@mui/system';
import React, {
	Children,
	useMemo
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	SyntheticEvent
} from 'react';
import type {
	Orientation,
	TabProps
} from 'ui-router-react-digest';
import AddTab from './AddTab';
import DrawerToggle from './DrawerToggle';
import RemoveTab from './RemoveTab';
import './TabHeader.css';

const styles = {
	tab: {
		'&.Mui-selected': {
			backgroundColor: (theme: Theme) => {
				return theme.palette.primary.dark;
			},
			color: (theme: Theme) => {
				return theme.palette.primary.contrastText;
			}
		},
		color: (theme: Theme) => {
			return theme.palette.primary.contrastText;
		},
		flexDirection: 'row'
	},
	tabs: {
		'& > .MuiTabs-scroller': {
			backgroundAttachment: 'local, local, scroll, scroll',
			backgroundImage: (theme: Theme) => {
				return `
					linear-gradient(to right, ${theme.palette.primary.main}, rgba(0, 0, 0, 0)),
					linear-gradient(to left, ${theme.palette.primary.main}, rgba(0, 0, 0, 0)),
					linear-gradient(to right, rgba(0, 0, 0, 0.87) -24px, ${theme.palette.primary.main}),
					linear-gradient(to left, rgba(0, 0, 0, 0.87) -24px, ${theme.palette.primary.main});
				`;
			},
			backgroundPosition: 'left center, right center, left center, right center',
			backgroundRepeat: 'no-repeat',
			// eslint-disable-next-line no-multi-str
			backgroundSize: '\
				80px calc(100% - 2px),\
				80px calc(100% - 2px),\
				16px calc(100% - 2px),\
				16px calc(100% - 2px);\
			'
		}
	}
} as {[key: string]: SxProps<Theme>};

const TabHeader: FunctionComponent<TabHeaderProps> = (
	{
		children,
		onTabSelect,
		orientation,
		tabIndex
	}: TabHeaderProps
): ReactElement => {
	const dynamicStyles = useMemo(
		() => {
			return {
				toolbar: {
					flexDirection: orientation === 'right' ?
						'row-reverse' :
						'row'
				}
			} as {[key: string]: SxProps<Theme>};
		},
		[
			orientation
		]
	);

	const handleTabSelect = (
		count: number
	): (
		(
			event: SyntheticEvent<Element, Event>,
			index: number
		) => void
		) => {
		return (
			_event: SyntheticEvent<Element, Event>,
			index: number
		): void => {
			if (tabIndex !== index + count) {
				onTabSelect(index + count);
			}
		};
	};

	let filterCount = 0;

	const tabList = Children.toArray(children).length > 1 ?
		// eslint-disable-next-line multiline-ternary
		Children.toArray(children).filter(
			(tab): boolean => {
				// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
				// @ts-ignore
				if (tab.props.hidden) {
					filterCount += 1;

					return false;
				}

				return true;
			}
		).map(
			(
				tab,
				index
			): ReactElement<TabProps> => {
				// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
				// @ts-ignore
				const params = tab.props?.state?.params ? // eslint-disable-line unicorn/prevent-abbreviations
					// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
					// @ts-ignore
					tab.props?.state.params :
					{};

				const text = params.title ?
					params.title :
					// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
					// @ts-ignore
					tab.props?.name;

				return (
					<Tab
						data-testid={`page-tab-${text}`}
						// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
						// @ts-ignore
						key={tab.props?.name}
						label={(
							<>
								{text}
								{
									params.tenured ?
										null :
										// eslint-disable-next-line @typescript-eslint/no-extra-parens
										(
											<RemoveTab
												// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
												// @ts-ignore
												name={tab.props.name}
												tabIndex={index + filterCount}
												title={text}
											/>
										)
								}
							</>
						)}
						styleName='tab'
						sx={styles.tab}
					/>
				);
			}
		) : null;

	return (
		<AppBar
			position='static'
			styleName='root'
		>
			<Toolbar
				styleName='toolbar'
				sx={dynamicStyles.toolbar}
			>
				<DrawerToggle />
				<div
					styleName='tabs-wrapper'
				>
					<Tabs
						onChange={handleTabSelect(filterCount)}
						scrollButtons={false}
						sx={styles.tabs}
						value={tabIndex - filterCount}
						variant='scrollable'
					>
						{tabList}
					</Tabs>
					<AddTab />
				</div>
			</Toolbar>
		</AppBar>
	);
};

export type TabHeaderProps = {
	children: Array<ReactElement<TabProps>> | ReactElement<TabProps>;
	onTabSelect: (index: number) => void;
	orientation: Orientation;
	tabIndex: number;
};

export default TabHeader;
