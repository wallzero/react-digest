/*
	eslint-disable
	jsx-a11y/tabindex-no-positive
*/

import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import {
	Provider
} from 'react-redux';
import store from 'src/redux/store';
import {
	Tab
} from 'ui-router-react-digest';
import TabHeader from '../TabHeader';

const onTabOpenToggle = jest.fn();
const onTabSelect = jest.fn();

describe(
	'Tab header',
	(): void => {
		beforeEach(
			(): void => {
				onTabOpenToggle.mockReset();
				onTabSelect.mockReset();
			}
		);

		it(
			'render with params',
			() => {
				expect.assertions(1);

				const {
					findByText
				} = render(
					<Provider
						store={store}
					>
						<TabHeader
							onTabSelect={onTabSelect}
							orientation='left'
							tabIndex={0}
						>
							<Tab
								name='tab0'
								state={{
									params: {
										title: 'Tab 0'
									}
								}}
							/>
							<Tab
								name='tab1'
								state={{
									params: {
										title: 'Tab 1'
									}
								}}
							/>
						</TabHeader>
					</Provider>
				);

				expect(findByText('Tab 1')).toBeDefined();
			}
		);

		it(
			'don\'t render remove tab button if tab is tenured',
			() => {
				expect.assertions(1);

				const {
					queryByTestId
				} = render(
					<Provider
						store={store}
					>
						<TabHeader
							onTabSelect={onTabSelect}
							orientation='left'
							tabIndex={0}
						>
							<Tab
								name='tab0'
								state={{
									params: {
										title: 'Tab 0'
									}
								}}
							/>
							<Tab
								name='tab1'
								state={{
									params: {
										tenured: true,
										title: 'Tab 1'
									}
								}}
							/>
						</TabHeader>
					</Provider>
				);

				expect(queryByTestId('page-tab-remove-tab1')).toBeNull();
			}
		);

		it(
			'select tab',
			async (): Promise<void> => {
				expect.assertions(2);

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<TabHeader
							onTabSelect={onTabSelect}
							orientation='left'
							tabIndex={0}
						>
							<Tab
								name='tab0'
							/>
							<Tab
								name='tab1'
							/>
						</TabHeader>
					</Provider>
				);

				const button = await findByTestId('page-tab-tab1');

				fireEvent.click(button);

				expect(onTabSelect.mock.calls).toHaveLength(1);

				expect(onTabSelect.mock.calls[0][0]).toBe(1);
			}
		);

		it(
			'don\'t select if tab is already selected',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<TabHeader
							onTabSelect={onTabSelect}
							orientation='left'
							tabIndex={0}
						>
							<Tab
								name='tab0'
							/>
							<Tab
								name='tab1'
							/>
						</TabHeader>
					</Provider>
				);

				const button = await findByTestId('page-tab-tab0');

				fireEvent.click(button);

				expect(onTabSelect.mock.calls).toHaveLength(0);
			}
		);

		it(
			'select index - 1 if previous tab is hidden',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<TabHeader
							onTabSelect={onTabSelect}
							orientation='left'
							tabIndex={1}
						>
							<Tab
								hidden
								name='tab0'
							/>
							<Tab
								name='tab1'
							/>
							<Tab
								name='tab2'
							/>
						</TabHeader>
					</Provider>
				);

				const button = await findByTestId('page-tab-tab2');

				fireEvent.click(button);

				expect(onTabSelect.mock.calls).toHaveLength(1);
			}
		);

		it(
			'toggle drawer',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<TabHeader
							onTabSelect={onTabSelect}
							orientation='left'
							tabIndex={1}
						>
							<Tab
								hidden
								name='tab0'
							/>
							<Tab
								name='tab1'
							/>
							<Tab
								name='tab2'
							/>
						</TabHeader>
					</Provider>
				);

				const button = await findByTestId('page-tab-tab2');

				fireEvent.click(button);

				expect(onTabSelect.mock.calls).toHaveLength(1);
			}
		);
	}
);
