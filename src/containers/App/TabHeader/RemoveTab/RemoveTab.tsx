import {
	faTimesCircle as TimesCircleIcon
} from '@fortawesome/free-solid-svg-icons/faTimesCircle';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import {
	styled
} from '@mui/system';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	KeyboardEvent,
	MouseEvent,
	ReactElement,
	ReactText
} from 'react';
import './RemoveTab.css';

const StyledFontAwesomeIcon = styled(FontAwesomeIcon)(
	({
		theme
	}) => {
		return {
			marginLeft: theme.spacing(2)
		};
	}
);

const RemoveTab: FunctionComponent<RemoveTabProps> = (
	{
		name,
		onTabRemove,
		tabIndex,
		title
	}: RemoveTabProps
): ReactElement => {
	const handleClick = useCallback(
		(
			event: KeyboardEvent<HTMLSpanElement> | MouseEvent<HTMLSpanElement>
		): void => {
			event.stopPropagation();

			onTabRemove(
				name,
				tabIndex
			);
		},
		[
			name,
			onTabRemove,
			tabIndex
		]
	);

	return (
		<span
			aria-label={`remove tab ${title}`}
			data-testid={`page-tab-remove-${name}`}
			onClick={handleClick}
			onKeyPress={handleClick}
			role='button'
			styleName='wrap'
			tabIndex={0}
		>
			<StyledFontAwesomeIcon
				icon={TimesCircleIcon}
			/>
		</span>
	);
};

export default RemoveTab;

export type RemoveTabProps = {
	name: ReactText;
	onTabRemove: (
		name: ReactText,
		tabIndex: number
	) => void;
	tabIndex: number;
	title: ReactText;
};
