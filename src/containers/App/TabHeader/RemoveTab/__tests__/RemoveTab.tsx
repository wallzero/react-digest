import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import RemoveTab from '../RemoveTab';

const onTabRemove = jest.fn();

describe(
	'Remove tab',
	(): void => {
		beforeEach(
			(): void => {
				onTabRemove.mockReset();
			}
		);

		it(
			'add new default tab',
			async (): Promise<void> => {
				expect.assertions(3);

				const name = 'tab';
				const title = 'Tab';
				const tabIndex = 1;

				const {
					findByTestId
				} = render(
					<RemoveTab
						name={name}
						onTabRemove={onTabRemove}
						tabIndex={tabIndex}
						title={title}
					/>
				);

				const button = await findByTestId(`page-tab-remove-${name}`);

				fireEvent.click(button);

				expect(onTabRemove.mock.calls).toHaveLength(1);

				expect(onTabRemove.mock.calls[0][0]).toBe(name);

				expect(onTabRemove.mock.calls[0][1]).toBe(tabIndex);
			}
		);
	}
);
