import {
	connect
} from 'react-redux';
import {
	onTabRemove
} from 'src/redux/actions/tabs';
import RemoveTab from './RemoveTab';
import type {
	RemoveTabProps
} from './RemoveTab';

const mapDispatchToProps = {
	onTabRemove
};

const mapStateToProps = (
	_: unknown,
	{
		name,
		tabIndex,
		title
	}: Pick<
	Omit<RemoveTabProps, keyof typeof mapDispatchToProps>,
	'name' |
	'tabIndex' |
	'title'
	>
): Omit<RemoveTabProps, keyof typeof mapDispatchToProps> => {
	return {
		name,
		tabIndex,
		title
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveTab);

export {
	mapStateToProps
};
