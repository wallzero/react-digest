import MenuIcon from '@mui/icons-material/Menu';
import IconButton from '@mui/material/IconButton';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const DrawerToggle: FunctionComponent<DrawerToggleProps> = (
	{
		onDrawerOpenToggle
	}: DrawerToggleProps
): ReactElement => {
	const handleDrawerToggle = useCallback(
		(): void => {
			onDrawerOpenToggle();
		},
		[
			onDrawerOpenToggle
		]
	);

	return (
		<IconButton
			aria-label='Menu'
			color='inherit'
			data-testid='page-drawer-toggle'
			onClick={handleDrawerToggle}
			size='large'
		>
			<MenuIcon />
		</IconButton>
	);
};

export type DrawerToggleProps = {
	onDrawerOpenToggle: (toggle?: boolean) => void;
};

export default DrawerToggle;
