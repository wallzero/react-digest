import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import DrawerToggle from '../DrawerToggle';

const onDrawerOpenToggle = jest.fn();

describe(
	'Drawer toggle',
	(): void => {
		beforeEach(
			(): void => {
				onDrawerOpenToggle.mockReset();
			}
		);

		it(
			'toggle drawer',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<DrawerToggle
						onDrawerOpenToggle={onDrawerOpenToggle}
					/>
				);

				const button = await findByTestId('page-drawer-toggle');

				fireEvent.click(button);

				expect(onDrawerOpenToggle.mock.calls).toHaveLength(1);
			}
		);
	}
);
