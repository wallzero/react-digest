import {
	connect
} from 'react-redux';
import {
	onDrawerOpenToggle
} from 'src/redux/actions/drawerSettings';
import DrawerToggle from './DrawerToggle';

const mapDispatchToProps = {
	onDrawerOpenToggle
};

export default connect(
	undefined,
	mapDispatchToProps
)(DrawerToggle);
