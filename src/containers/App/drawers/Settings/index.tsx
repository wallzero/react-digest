import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import {
	styled
} from '@mui/system';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import DrawerSettings from './DrawerSettings';
import './Settings.css';

const StyledDiv = styled('div')(
	({
		theme
	}) => {
		return {
			backgroundColor: theme.palette.background.default
		};
	}
);

const StyledHeader = styled('h2')(
	({
		theme
	}) => {
		return {
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			fontSize: theme.typography.h6.fontSize,
			paddingLeft: 2
		};
	}
);

const Settings: FunctionComponent = (): ReactElement => {
	return (
		<StyledDiv
			styleName='container'
		>
			<StyledHeader>
				Settings
			</StyledHeader>
			<Accordion
				defaultExpanded
				square
				styleName='expansion'
			>
				<AccordionSummary
					expandIcon={
						<ExpandMoreIcon />
					}
				>
					Drawer
				</AccordionSummary>
				<AccordionDetails>
					<DrawerSettings />
				</AccordionDetails>
			</Accordion>
		</StyledDiv>
	);
};

export default Settings;
