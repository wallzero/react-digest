import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import {
	Provider
} from 'react-redux';
import store from 'src/redux/store';
import OrientationToggle from '../OrientationToggle';

const orientation = 'left';
const onOrientationToggle = jest.fn();

describe(
	'Orientation toggle',
	(): void => {
		beforeEach(
			(): void => {
				onOrientationToggle.mockReset();
			}
		);

		it(
			'toggle orientation',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<OrientationToggle
							onOrientationToggle={onOrientationToggle}
							orientation={orientation}
						/>
					</Provider>
				);

				const button = await findByTestId('page-drawer-orientation-toggle');
				const input = button.querySelector('input');

				fireEvent.click(input);

				expect(onOrientationToggle.mock.calls).toHaveLength(1);
			}
		);
	}
);
