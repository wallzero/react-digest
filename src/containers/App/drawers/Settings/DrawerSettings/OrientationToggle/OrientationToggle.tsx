import {
	faExchangeAlt as ExchangeIcon
} from '@fortawesome/free-solid-svg-icons/faExchangeAlt';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ListItemText from '@mui/material/ListItemText';
import Switch from '@mui/material/Switch';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import type {
	Orientation
} from 'ui-router-react-digest';

const OrientationToggle: FunctionComponent<OrientationToggleProps> = (
	{
		onOrientationToggle,
		orientation
	}: OrientationToggleProps
): ReactElement => {
	const handleOrientationToggle = useCallback(
		(): void => {
			onOrientationToggle();
		},
		[
			onOrientationToggle
		]
	);

	return (
		<ListItem>
			<ListItemIcon>
				<FontAwesomeIcon
					icon={ExchangeIcon}
					size='lg'
				/>
			</ListItemIcon>
			<ListItemText
				id='switch-list-label-orientation'
				primary='Orientation'
			/>
			<ListItemSecondaryAction>
				<Switch
					checked={orientation === 'right'}
					data-testid='page-drawer-orientation-toggle'
					edge='end'
					inputProps={{
						'aria-labelledby': 'switch-list-label-wifi'
					}}
					onChange={handleOrientationToggle}
				/>
			</ListItemSecondaryAction>
		</ListItem>
	);
};

export type OrientationToggleProps = {
	onOrientationToggle: (toggle?: Orientation) => void;
	orientation: Orientation;
};

export default OrientationToggle;
