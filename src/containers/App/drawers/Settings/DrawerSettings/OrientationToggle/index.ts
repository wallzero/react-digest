import {
	connect
} from 'react-redux';
import {
	onOrientationToggle
} from 'src/redux/actions/orientation';
import defaultState from 'src/redux/defaultState';
import type {
	Orientation
} from 'ui-router-react-digest';
import DrawerSettings from './OrientationToggle';

const mapDispatchToProps = {
	onOrientationToggle
};

const mapStateToProps = (
	state = defaultState
): {
	orientation: Orientation;
} => {
	return {
		orientation: state.orientation
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(DrawerSettings);

export {
	mapStateToProps
};
