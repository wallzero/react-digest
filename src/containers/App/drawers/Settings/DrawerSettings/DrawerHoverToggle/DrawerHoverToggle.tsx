import {
	faShareSquare as ShareSquareIcon
} from '@fortawesome/free-solid-svg-icons/faShareSquare';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import Checkbox from '@mui/material/Checkbox';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ListItemText from '@mui/material/ListItemText';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const DrawerHoverToggle: FunctionComponent<DrawerHoverToggleProps> = (
	{
		drawerHover,
		onDrawerHoverToggle
	}: DrawerHoverToggleProps
): ReactElement => {
	const handleDrawerHoverToggle = useCallback(
		(): void => {
			onDrawerHoverToggle();
		},
		[
			onDrawerHoverToggle
		]
	);

	return (
		<ListItem>
			<ListItemIcon>
				<FontAwesomeIcon
					icon={ShareSquareIcon}
					size='lg'
				/>
			</ListItemIcon>
			<ListItemText
				id='switch-list-label-hover'
				primary='Hover'
			/>
			<ListItemSecondaryAction>
				<Checkbox
					checked={drawerHover}
					data-testid='page-drawer-hover-toggle'
					edge='end'
					inputProps={{
						'aria-labelledby': 'switch-list-label-hover'
					}}
					onChange={handleDrawerHoverToggle}
				/>
			</ListItemSecondaryAction>
		</ListItem>
	);
};

export type DrawerHoverToggleProps = {
	drawerHover: boolean;
	onDrawerHoverToggle: (toggle?: boolean) => void;
};

export default DrawerHoverToggle;
