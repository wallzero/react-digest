import {
	connect
} from 'react-redux';
import {
	onDrawerHoverToggle
} from 'src/redux/actions/drawerSettings';
import defaultState from 'src/redux/defaultState';
import DrawerSettings from './DrawerHoverToggle';

const mapDispatchToProps = {
	onDrawerHoverToggle
};

const mapStateToProps = (
	state = defaultState
): {
	drawerHover: boolean;
} => {
	return {
		drawerHover: state.drawerSettings.hover
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(DrawerSettings);

export {
	mapStateToProps
};
