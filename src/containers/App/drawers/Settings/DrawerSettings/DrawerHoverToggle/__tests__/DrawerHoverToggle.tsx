import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import {
	Provider
} from 'react-redux';
import store from 'src/redux/store';
import DrawerHoverToggle from '../DrawerHoverToggle';

const drawerHover = true;
const onDrawerHoverToggle = jest.fn();

describe(
	'Drawer hover toggle',
	(): void => {
		beforeEach(
			(): void => {
				onDrawerHoverToggle.mockReset();
			}
		);

		it(
			'toggle drawer hover',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<DrawerHoverToggle
							drawerHover={drawerHover}
							onDrawerHoverToggle={onDrawerHoverToggle}
						/>
					</Provider>
				);

				const button = await findByTestId('page-drawer-hover-toggle');
				const input = button.querySelector('input');

				fireEvent.click(input);

				expect(onDrawerHoverToggle.mock.calls).toHaveLength(1);
			}
		);
	}
);
