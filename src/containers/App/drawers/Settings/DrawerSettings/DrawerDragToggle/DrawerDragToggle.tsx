import {
	faHandPointUp as HandPointUpIcon
} from '@fortawesome/free-solid-svg-icons/faHandPointUp';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import Checkbox from '@mui/material/Checkbox';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ListItemText from '@mui/material/ListItemText';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const DrawerDragToggle: FunctionComponent<DrawerDragToggleProps> = (
	{
		drawerDrag,
		onDrawerDragToggle
	}: DrawerDragToggleProps
): ReactElement => {
	const handleDrawerDragToggle = useCallback(
		(): void => {
			onDrawerDragToggle();
		},
		[
			onDrawerDragToggle
		]
	);

	return (
		<ListItem>
			<ListItemIcon>
				<FontAwesomeIcon
					icon={HandPointUpIcon}
					size='lg'
				/>
			</ListItemIcon>
			<ListItemText
				id='switch-list-label-drag'
				primary='Drag'
			/>
			<ListItemSecondaryAction>
				<Checkbox
					checked={drawerDrag}
					data-testid='page-drawer-drag-toggle'
					edge='end'
					inputProps={{
						'aria-labelledby': 'switch-list-label-drag'
					}}
					onChange={handleDrawerDragToggle}
				/>
			</ListItemSecondaryAction>
		</ListItem>
	);
};

export type DrawerDragToggleProps = {
	drawerDrag: boolean;
	onDrawerDragToggle: (toggle?: boolean) => void;
};

export default DrawerDragToggle;
