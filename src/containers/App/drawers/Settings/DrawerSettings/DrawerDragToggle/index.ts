import {
	connect
} from 'react-redux';
import {
	onDrawerDragToggle
} from 'src/redux/actions/drawerSettings';
import defaultState from 'src/redux/defaultState';
import DrawerDragToggle from './DrawerDragToggle';

const mapDispatchToProps = {
	onDrawerDragToggle
};

const mapStateToProps = (
	state = defaultState
): {
	drawerDrag: boolean;
} => {
	return {
		drawerDrag: state.drawerSettings.drag
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(DrawerDragToggle);

export {
	mapStateToProps
};
