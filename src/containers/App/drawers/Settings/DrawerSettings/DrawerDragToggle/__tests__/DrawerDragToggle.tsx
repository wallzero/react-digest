import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import {
	Provider
} from 'react-redux';
import store from 'src/redux/store';
import DrawerDragToggle from '../DrawerDragToggle';

const drawerDrag = true;
const onDrawerDragToggle = jest.fn();

describe(
	'Drawer drag toggle',
	(): void => {
		beforeEach(
			(): void => {
				onDrawerDragToggle.mockReset();
			}
		);

		it(
			'toggle drawer drag',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<DrawerDragToggle
							drawerDrag={drawerDrag}
							onDrawerDragToggle={onDrawerDragToggle}
						/>
					</Provider>
				);

				const button = await findByTestId('page-drawer-drag-toggle');
				const input = button.querySelector('input');

				fireEvent.click(input);

				expect(onDrawerDragToggle.mock.calls).toHaveLength(1);
			}
		);
	}
);
