import List from '@mui/material/List';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import DrawerDockedToggle from './DrawerDockedToggle';
import DrawerDragToggle from './DrawerDragToggle';
import DrawerHoverToggle from './DrawerHoverToggle';
import OrientationToggle from './OrientationToggle';
import './DrawerSettings.css';

const DrawerSettings: FunctionComponent = (): ReactElement => {
	return (
		<List
			styleName='list'
		>
			<DrawerDockedToggle />
			<DrawerDragToggle />
			<DrawerHoverToggle />
			<OrientationToggle />
		</List>
	);
};

export default DrawerSettings;
