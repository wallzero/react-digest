import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import {
	Provider
} from 'react-redux';
import store from 'src/redux/store';
import DrawerDockedToggle from '../DrawerDockedToggle';

const drawerDocked = true;
const onDrawerDockedToggle = jest.fn();

describe(
	'Drawer docked toggle',
	(): void => {
		beforeEach(
			(): void => {
				onDrawerDockedToggle.mockReset();
			}
		);

		it(
			'toggle drawer docked',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<DrawerDockedToggle
							drawerDocked={drawerDocked}
							onDrawerDockedToggle={onDrawerDockedToggle}
						/>
					</Provider>
				);

				const button = await findByTestId('page-drawer-docked-toggle');
				const input = button.querySelector('input');

				fireEvent.click(input);

				expect(onDrawerDockedToggle.mock.calls).toHaveLength(1);
			}
		);
	}
);
