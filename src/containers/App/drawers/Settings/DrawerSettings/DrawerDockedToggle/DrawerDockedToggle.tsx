import {
	faAnchor as AnchorIcon
} from '@fortawesome/free-solid-svg-icons/faAnchor';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import Checkbox from '@mui/material/Checkbox';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ListItemText from '@mui/material/ListItemText';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const DrawerDockedToggle: FunctionComponent<DrawerDockedToggleProperties> = (
	{
		drawerDocked,
		onDrawerDockedToggle
	}: DrawerDockedToggleProperties
): ReactElement => {
	const handleDrawerDockedToggle = useCallback(
		(): void => {
			onDrawerDockedToggle();
		},
		[
			onDrawerDockedToggle
		]
	);

	return (
		<ListItem>
			<ListItemIcon>
				<FontAwesomeIcon
					icon={AnchorIcon}
					size='lg'
				/>
			</ListItemIcon>
			<ListItemText
				id='switch-list-label-docked'
				primary='Docked'
			/>
			<ListItemSecondaryAction>
				<Checkbox
					checked={drawerDocked}
					data-testid='page-drawer-docked-toggle'
					edge='end'
					inputProps={{
						'aria-labelledby': 'switch-list-label-docked'
					}}
					onChange={handleDrawerDockedToggle}
				/>
			</ListItemSecondaryAction>
		</ListItem>
	);
};

export type DrawerDockedToggleProperties = {
	drawerDocked: boolean;
	onDrawerDockedToggle: (toggle?: boolean) => void;
};

export default DrawerDockedToggle;
