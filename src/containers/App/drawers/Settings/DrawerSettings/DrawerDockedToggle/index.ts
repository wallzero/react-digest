import {
	connect
} from 'react-redux';
import {
	onDrawerDockedToggle
} from 'src/redux/actions/drawerSettings';
import defaultState from 'src/redux/defaultState';
import DrawerSettings from './DrawerDockedToggle';

const mapDispatchToProps = {
	onDrawerDockedToggle
};

const mapStateToProps = (
	state = defaultState
): {
	drawerDocked: boolean;
} => {
	return {
		drawerDocked: state.drawerSettings.docked
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(DrawerSettings);

export {
	mapStateToProps
};
