import {
	faCog
} from '@fortawesome/free-solid-svg-icons/faCog';
import {
	faWrench
} from '@fortawesome/free-solid-svg-icons/faWrench';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import type {
	ReactStateDeclaration
} from '@uirouter/react/lib/interface';
import React from 'react';
import type {
	ClassicComponentClass,
	ComponentClass,
	FunctionComponent
} from 'react';
import Settings from './Settings';

type Drawers = {
	[keys: string]: {
		component?: ClassicComponentClass | ComponentClass | FunctionComponent;
		state?: ReactStateDeclaration;
	};
};

const drawers: Drawers = {
	settings: {
		component: Settings,
		state: {
			params: {
				icon: <FontAwesomeIcon
					icon={faCog}
					size='lg'
				/>
			}
		}
	},
	tools: {
		state: {
			params: {
				icon: <FontAwesomeIcon
					icon={faWrench}
					size='lg'
				/>
			}
		}
	}
};

export default drawers;
