import {
	connect
} from 'react-redux';
import {
	onDrawerOpenToggle,
	onDrawerSelect
} from 'src/redux/actions/drawerSettings';
import {
	onTabSelect
} from 'src/redux/actions/tabSettings';
import {
	selectTabs
} from 'src/redux/selectors/tabs';
import type State from 'src/redux/state';
import App from './App';

const mapDispatchToProps = {
	onDrawerOpenToggle,
	onDrawerSelect,
	onTabSelect
};

const mapStateToProps = (state: State.All): {
	drawerDocked: State.drawerSettings.docked;
	drawerDrag: State.drawerSettings.drag;
	drawerHover: State.drawerSettings.hover;
	drawerIndex: State.drawerSettings.index;
	drawerOpen: State.drawerSettings.open;
	drawerWidth: State.drawerSettings.width;
	drawers: State.drawers;
	orientation: State.orientation;
	tabIndex: State.tabSettings.index;
	tabs: State.tab[];
	title: State.miscellaneous.title;
} => {
	return {
		drawerDocked: state.drawerSettings.docked,
		drawerDrag: state.drawerSettings.drag,
		drawerHover: state.drawerSettings.hover,
		drawerIndex: state.drawerSettings.index,
		drawerOpen: state.drawerSettings.open,
		drawers: state.drawers,
		drawerWidth: state.drawerSettings.width,
		orientation: state.orientation,
		tabIndex: state.tabSettings.index,
		tabs: selectTabs(state),
		title: state.miscellaneous.title
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(
	App
);
