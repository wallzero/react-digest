import React, {
	lazy,
	memo,
	Suspense
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import {
	ErrorBoundary
} from 'react-error-boundary';
import Error from 'src/components/Error';
import Loading from 'src/components/Loading';

const DiagramContainer = lazy(
	// eslint-disable-next-line @typescript-eslint/consistent-type-imports
	(): Promise<typeof import('src/containers/Diagram')> => {
		return import(

			/* webpackChunkName: "Components_tab" */
			/* webpackPrefetch: true */
			'src/containers/Diagram'
		);
	}
);

const Diagram: FunctionComponent<DiagramProps> = (
	{
		diagramId
	}: DiagramProps
): ReactElement => {
	return (
		<ErrorBoundary
			FallbackComponent={Error}
		>
			<Suspense
				fallback={<Loading />}
			>
				<DiagramContainer
					diagramId={diagramId}
				/>
			</Suspense>
		</ErrorBoundary>
	);
};

export default memo(Diagram);

export type DiagramProps = {
	diagramId: ReactText;
	title?: string;
};
