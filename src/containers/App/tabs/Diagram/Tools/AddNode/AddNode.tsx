import {
	faPlus
} from '@fortawesome/free-solid-svg-icons/faPlus';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import Button from '@mui/material/Button';
import {
	styled
} from '@mui/system';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';

const StyledFontAwesomeIcon = styled(FontAwesomeIcon)(
	({
		theme
	}) => {
		return {
			marginRight: theme.spacing(2)
		};
	}
);

const AddNode: FunctionComponent<AddNodeProps> = (
	{
		diagramId,
		onNodeAdd
	}: AddNodeProps
): ReactElement => {
	const handleClick = useCallback(
		(): void => {
			onNodeAdd(
				diagramId,
				{}
			);
		},
		[
			diagramId,
			onNodeAdd
		]
	);

	return (
		<Button
			color='primary'
			fullWidth
			onClick={handleClick}
			variant='contained'
		>
			<StyledFontAwesomeIcon
				icon={faPlus}
				size='sm'
			/>
			Add Node
		</Button>
	);
};

export default AddNode;

export type AddNodeProps = {
	diagramId: ReactText;
	onNodeAdd: (
		diagramId: ReactText,
		node: DNode
	) => void;
};
