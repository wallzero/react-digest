import {
	connect
} from 'react-redux';
import {
	onNodeAdd
} from 'src/redux/actions/nodes';
import AddNode from './AddNode';
import type {
	AddNodeProps
} from './AddNode';

const mapDispatchToProps = {
	onNodeAdd
};

const mapStateToProps = (
	_: unknown,
	{
		diagramId
	}: Pick<
	Omit<AddNodeProps, keyof typeof mapDispatchToProps>,
	'diagramId'
	>
): Omit<AddNodeProps, keyof typeof mapDispatchToProps> => {
	return {
		diagramId
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddNode);

export {
	mapStateToProps
};
