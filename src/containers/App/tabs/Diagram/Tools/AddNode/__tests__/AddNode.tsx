import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import defaultState from 'src/redux/defaultState';
import AddNode from '../AddNode';

const onNodeAdd = jest.fn();

describe(
	'Add node',
	(): void => {
		it(
			'click',
			async (): Promise<void> => {
				expect.assertions(1);

				const diagramId = Object.keys(defaultState.diagrams)[0];

				const {
					findByText
				} = render(
					<AddNode
						diagramId={diagramId}
						onNodeAdd={onNodeAdd}
					/>
				);

				const button = await findByText('Add Node');

				fireEvent.click(button);

				expect(onNodeAdd.mock.calls).toHaveLength(1);
			}
		);
	}
);
