import {
	connect
} from 'react-redux';
import {
	onNodesRemove
} from 'src/redux/actions/nodes';
import defaultState from 'src/redux/defaultState';
import RemoveSelections from './RemoveSelections';
import type {
	RemoveSelectionsProps
} from './RemoveSelections';

const mapDispatchToProps = {
	onNodesRemove
};

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId
	}: Pick<
	Omit<RemoveSelectionsProps, keyof typeof mapDispatchToProps>,
	'diagramId'
	>
): Omit<RemoveSelectionsProps, keyof typeof mapDispatchToProps> => {
	return {
		diagramId,
		selections: state.selections[diagramId]
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveSelections);

export {
	mapStateToProps
};
