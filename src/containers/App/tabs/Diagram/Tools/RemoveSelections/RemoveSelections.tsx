import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import React, {
	memo,
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';

const styles = {
	delete: {
		marginRight: 2
	}
};

const RemoveSelections: FunctionComponent<RemoveSelectionsProps> = (
	{
		diagramId,
		onNodesRemove,
		selections
	}: RemoveSelectionsProps
): ReactElement => {
	const selected = Object.keys(selections);

	const handleClick = useCallback(
		(): void => {
		// eslint-disable-next-line no-alert
			const remove = confirm(`Remove selected nodes? [${selected.join(', ')}]`);

			if (remove) {
				onNodesRemove(
					diagramId,
					selected
				);
			}
		},
		[
			diagramId,
			onNodesRemove,
			selected
		]
	);

	return (
		<Button
			color='secondary'
			disabled={selected.length === 0}
			fullWidth
			onClick={handleClick}
			variant='contained'
		>
			<DeleteIcon
				fontSize='small'
				sx={styles.delete}
			/>
			Remove Selected Nodes
		</Button>
	);
};

export default memo(RemoveSelections);

export type RemoveSelectionsProps = {
	diagramId: ReactText;
	onNodesRemove: (
		diagramId: ReactText,
		nodes: ReactText[]
	) => void;
	selections: Selections;
};
