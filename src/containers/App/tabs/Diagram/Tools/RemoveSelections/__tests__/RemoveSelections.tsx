import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import defaultState from 'src/redux/defaultState';
import RemoveSelections from '../RemoveSelections';

const onNodesRemove = jest.fn();

describe(
	'Remove node',
	(): void => {
		it(
			'click',
			async (): Promise<void> => {
				expect.assertions(1);

				const diagramId = Object.keys(defaultState.diagrams)[0];

				global.confirm = (): true => {
					return true;
				};

				const {
					findByText
				} = render(
					<RemoveSelections
						diagramId={diagramId}
						onNodesRemove={onNodesRemove}
						selections={{
							test: true
						}}
					/>
				);

				const button = await findByText('Remove Selected Nodes');

				fireEvent.click(button);

				expect(onNodesRemove.mock.calls).toHaveLength(1);
			}
		);

		it(
			'click and refuse',
			async (): Promise<void> => {
				expect.assertions(1);

				const diagramId = Object.keys(defaultState.diagrams)[0];

				global.confirm = (): false => {
					return false;
				};

				const {
					findByText
				} = render(
					<RemoveSelections
						diagramId={diagramId}
						onNodesRemove={onNodesRemove}
						selections={{
							test: true
						}}
					/>
				);

				const button = await findByText('Remove Selected Nodes');

				fireEvent.click(button);

				expect(onNodesRemove.mock.calls).toHaveLength(1);
			}
		);
	}
);
