import React, {
	memo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const SelectionCount: FunctionComponent<SelectionCountProps> = (
	{
		count
	}: SelectionCountProps
): ReactElement => {
	return (
		<>
			{count}
		</>
	);
};

export default memo(SelectionCount);

export type SelectionCountProps = {
	count: number;
};
