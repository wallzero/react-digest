import type {
	ReactText
} from 'react';
import {
	connect
} from 'react-redux';
import defaultState from 'src/redux/defaultState';
import SelectionCount from './SelectionCount';
import type {
	SelectionCountProps
} from './SelectionCount';

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId
	}: {
		diagramId: ReactText;
	}
): SelectionCountProps => {
	return {
		count: Object.keys(state.selections[diagramId]).length
	};
};

export default connect(
	mapStateToProps
)(SelectionCount);

export {
	mapStateToProps
};
