import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import {
	styled
} from '@mui/system';
import React, {
	memo
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import AddNode from './AddNode';
import RemoveSelections from './RemoveSelections';
import SelectionCount from './SelectionCount';
import Selections from './Selections';
import './Tools.css';

const StyledDiv = styled('div')(
	({
		theme
	}) => {
		return {
			backgroundColor: theme.palette.background.default
		};
	}
);

const StyledH2 = styled('h2')(
	({
		theme
	}) => {
		return {
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			fontSize: theme.typography.h6.fontSize,
			paddingLeft: theme.spacing(2)
		};
	}
);

const Tools: FunctionComponent<ToolsProps> = (
	{
		diagramId
	}: ToolsProps
): ReactElement => {
	return (
		<StyledDiv
			styleName='container'
		>
			<StyledH2>
				Diagram
			</StyledH2>
			<Accordion
				square
				styleName='expansion'
			>
				<AccordionSummary
					expandIcon={
						<ExpandMoreIcon />
					}
				>
					Instructions
				</AccordionSummary>
				<AccordionDetails
					styleName='expansion-details'
				>
					<ul>
						<li>
							Press moves a node
						</li>
						<li>
							Double tap activates a node
						</li>
						<li>
							Press and hold for a half second to drag a node connector
						</li>
						<li>
							To select multiple nodes, hold &lt;ctrl&gt; key or tap and hold on
							the diagram background and then tap on nodes
						</li>
					</ul>
					<p>
						This example is split into several components.
					</p>
					<p>
						The example code could be more elegant but the drag and drop
						solution combined with multiple nested node interactions complicates
						matters. Nevertheless it works well.
					</p>
					<a
						href='https://gitlab.com/wallzero/react-ag/blob/master/demo/src/containers/App/tabs/ComplexDiagram/'
					>
						Source Link
					</a>
				</AccordionDetails>
			</Accordion>
			<Accordion
				square
				styleName='expansion'
			>
				<AccordionSummary
					expandIcon={
						<ExpandMoreIcon />
					}
				>
					Controls
				</AccordionSummary>
				<AccordionDetails>
					<AddNode
						diagramId={diagramId}
					/>
				</AccordionDetails>
			</Accordion>
			<Accordion
				square
				styleName='expansion'
			>
				<AccordionSummary
					expandIcon={
						<ExpandMoreIcon />
					}
				>
					Selections (
					<SelectionCount
						diagramId={diagramId}
					/>
					)
				</AccordionSummary>
				<AccordionDetails>
					<Selections
						diagramId={diagramId}
					/>
				</AccordionDetails>
				<AccordionDetails>
					<RemoveSelections
						diagramId={diagramId}
					/>
				</AccordionDetails>
			</Accordion>
		</StyledDiv>
	);
};

export default memo(Tools);

export type ToolsProps = {
	diagramId: ReactText;
};
