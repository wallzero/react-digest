import type {
	UIRouter
} from '@uirouter/core';
import connectWithRouter from 'ui-router-react-connect';
import Tools from './Tools';
import type {
	ToolsProps
} from './Tools';

const mapRouterToProps = (
	router: UIRouter
): ToolsProps => {
	return {
		diagramId: router.stateService.params.id
	};
};

export default connectWithRouter(
	mapRouterToProps
)(Tools);
