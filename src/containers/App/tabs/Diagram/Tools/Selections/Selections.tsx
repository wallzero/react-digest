import type {
	Theme
} from '@mui/material';
import Chip from '@mui/material/Chip';
import Paper from '@mui/material/Paper';
import type {
	SxProps
} from '@mui/system';
import React, {
	memo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Selections.css';

const styles = {
	chip: {
		margin: (theme) => {
			return `0 ${theme.spacing(2)} ${theme.spacing(2)} 0`;
		}
	} as SxProps<Theme>,
	paper: {
		padding: 4
	}
};

const Selections: FunctionComponent<SelectionsProps> = (
	{
		selections
	}: SelectionsProps
): ReactElement => {
	const selected = Object.keys(selections);

	const displaySelection = selected.length > 0 ?
		selected.map(
			(
				selection
			): ReactElement => {
				const label = selection.slice(
					0,
					8
				) + '...';

				return (
					<Chip
						key={selection}
						label={label}
						sx={styles.chip}
					/>
				);
			}
		) :
		// eslint-disable-next-line @typescript-eslint/no-extra-parens
		(
			<span
				styleName='placeholder'
			>
				none selected
			</span>
		);

	return (
		<Paper
			styleName='paper'
			sx={styles.paper}
		>
			{displaySelection}
		</Paper>
	);
};

export default memo(Selections);

export type SelectionsProps = {
	selections: Selections;
};
