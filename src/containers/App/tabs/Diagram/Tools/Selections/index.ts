import type {
	ReactText
} from 'react';
import {
	connect
} from 'react-redux';
import defaultState from 'src/redux/defaultState';
import Selections from './Selections';
import type {
	SelectionsProps
} from './Selections';

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId
	}: {
		diagramId: ReactText;
	}
): SelectionsProps => {
	return {
		selections: state.selections[diagramId]
	};
};

export default connect(
	mapStateToProps
)(Selections);

export {
	mapStateToProps
};
