import {
	memoryLocationPlugin,
	servicesPlugin,
	UIRouterReact
} from '@uirouter/react';
import {
	mapRouterToProps
} from '..';

describe(
	'Diagram HOC',
	(): void => {
		describe(
			'mapRouterToProps',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = 'anything';

						const router = new UIRouterReact();
						router.plugin(memoryLocationPlugin);
						router.plugin(servicesPlugin);
						router.stateService.params.id = diagramId;

						expect(
							mapRouterToProps(
								router
							).diagramId
						).toStrictEqual(diagramId);
					}
				);
			}
		);
	}
);
