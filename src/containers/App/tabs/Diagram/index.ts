import type {
	UIRouter
} from '@uirouter/core';
import connectWithRouter from 'ui-router-react-connect';
import Diagram from './Diagram';
import type {
	DiagramProps
} from './Diagram';

const mapRouterToProps = (
	router: UIRouter
): DiagramProps => {
	return {
		diagramId: router.stateService.params.id
	};
};

export default connectWithRouter(
	mapRouterToProps,
	false
)(Diagram);

export {
	mapRouterToProps
};
