import {
	styled
} from '@mui/system';
import React, {
	memo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './404.css';

const StyledDiv = styled('div')(
	({
		theme
	}) => {
		return {
			border: `${theme.spacing(2)} solid black`,
			padding: theme.spacing(2)
		};
	}
);

const E404: FunctionComponent = (): ReactElement => {
	return (
		<StyledDiv
			styleName='e404-tab'
		>
			<h2>
				404
			</h2>
		</StyledDiv>
	);
};

export default memo(E404);
