import type {
	ClassicComponentClass,
	ComponentClass,
	FunctionComponent
} from 'react';
import E404 from './404';
// eslint-disable-next-line canonical/id-match
import E404Tools from './404/Tools';
import Components from './Components';
import ComponentsTools from './Components/Tools';
import Diagram from './Diagram';
import DiagramTools from './Diagram/Tools';
import Dynamic from './Dynamic';
import DynamicTools from './Dynamic/Tools';

type TabTypes = {
	[keys: string]: {
		component: ClassicComponentClass | ComponentClass | FunctionComponent;
		drawers: {
			[keys: string]: ClassicComponentClass | ComponentClass | FunctionComponent;
		};
		footer?: ClassicComponentClass | ComponentClass | FunctionComponent;
	};
};

const tabs: TabTypes = {
	'404': {
		component: E404,
		drawers: {
			tools: E404Tools
		}
	},
	components: {
		component: Components,
		drawers: {
			tools: ComponentsTools
		}
	},
	diagram: {
		component: Diagram,
		drawers: {
			tools: DiagramTools
		}
	},
	dynamic: {
		component: Dynamic,
		drawers: {
			tools: DynamicTools
		}
	}
};

export default tabs;
