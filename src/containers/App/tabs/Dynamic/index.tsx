import {
	styled
} from '@mui/system';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Dynamic.css';

const StyledDiv = styled('div')(
	({
		theme
	}) => {
		return {
			padding: theme.spacing(2)
		};
	}
);

const StyledH2 = styled('h2')(
	({
		theme
	}) => {
		return {
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			fontSize: theme.typography.h6.fontSize,
			paddingLeft: theme.spacing(2)
		};
	}
);

const Dynamic: FunctionComponent = (): ReactElement => {
	return (
		<StyledDiv
			styleName='dynamic'
		>
			<StyledH2>
				Dynamic Tab
			</StyledH2>
		</StyledDiv>
	);
};

export default Dynamic;
