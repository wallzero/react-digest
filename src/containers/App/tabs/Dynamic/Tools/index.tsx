import {
	styled
} from '@mui/system';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Tools.css';

const StyledDiv = styled('div')(
	({
		theme
	}) => {
		return {
			backgroundColor: theme.palette.background.default
		};
	}
);

const StyledH2 = styled('h2')(
	({
		theme
	}) => {
		return {
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			fontSize: theme.typography.h6.fontSize,
			paddingLeft: theme.spacing(2)
		};
	}
);

const Tools: FunctionComponent = (): ReactElement => {
	return (
		<StyledDiv
			styleName='tools'
		>
			<StyledH2>
				Dynamic Tools
			</StyledH2>
		</StyledDiv>
	);
};

export default Tools;
