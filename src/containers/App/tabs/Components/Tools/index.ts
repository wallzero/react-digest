import {
	connect
} from 'react-redux';
import Tools from './Tools';

const mapStateToProps = (): Record<string, unknown> => {
	return {};
};

export default connect(
	mapStateToProps
)(Tools);

export {
	mapStateToProps
};
