import React, {
	lazy,
	Suspense
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import {
	ErrorBoundary
} from 'react-error-boundary';
import Error from 'src/components/Error';
import Loading from 'src/components/Loading';

const Masonry = lazy(
	// eslint-disable-next-line @typescript-eslint/consistent-type-imports
	(): Promise<typeof import('src/containers/Components')> => {
		return import(

			/* webpackChunkName: "Components_tab" */
			/* webpackPrefetch: true */
			'src/containers/Components'
		);
	}
);

const Components: FunctionComponent = (): ReactElement => {
	return (
		<ErrorBoundary
			FallbackComponent={Error}
		>
			<Suspense
				fallback={<Loading />}
			>
				<Masonry />
			</Suspense>
		</ErrorBoundary>
	);
};

export default Components;
