import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import {
	Drawer
} from 'ui-router-react-digest';
import DrawerHeader from '../DrawerHeader';

const onDrawerSelect = jest.fn();

describe(
	'Drawer header',
	(): void => {
		beforeEach(
			(): void => {
				onDrawerSelect.mockReset();
			}
		);

		it(
			'render with params',
			() => {
				expect.assertions(1);

				const {
					findByText
				} = render(
					<DrawerHeader
						drawerIndex={0}
						onDrawerSelect={onDrawerSelect}
						orientation='left'
						title='test'
					>
						<Drawer
							name='drawer0'
							state={{
								params: {
									title: 'Drawer 0'
								}
							}}
						/>
						<Drawer
							name='drawer1'
							state={{
								params: {
									title: 'Drawer 1'
								}
							}}
						/>
					</DrawerHeader>
				);

				expect(findByText('Drawer 1')).toBeDefined();
			}
		);

		it(
			'select drawer',
			async (): Promise<void> => {
				expect.assertions(2);

				const {
					findByTestId
				} = render(
					<DrawerHeader
						drawerIndex={0}
						onDrawerSelect={onDrawerSelect}
						orientation='left'
						title='test'
					>
						<Drawer
							name='drawer0'
						/>
						<Drawer
							name='drawer1'
						/>
					</DrawerHeader>
				);

				const button = await findByTestId('page-tab-drawer1');

				fireEvent.click(button);

				expect(onDrawerSelect.mock.calls).toHaveLength(1);

				expect(onDrawerSelect.mock.calls[0][0]).toBe(1);
			}
		);

		it(
			'don\'t select if drawer is already selected',
			async (): Promise<void> => {
				expect.assertions(1);

				const {
					findByTestId
				} = render(
					<DrawerHeader
						drawerIndex={0}
						onDrawerSelect={onDrawerSelect}
						orientation='left'
						title='test'
					>
						<Drawer
							name='drawer0'
						/>
						<Drawer
							name='drawer1'
						/>
					</DrawerHeader>
				);

				const button = await findByTestId('page-tab-drawer0');

				fireEvent.click(button);

				expect(onDrawerSelect.mock.calls).toHaveLength(0);
			}
		);
	}
);
