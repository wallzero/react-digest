import type {
	Theme
} from '@mui/material';
import Paper from '@mui/material/Paper';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Toolbar from '@mui/material/Toolbar';
import type {
	SxProps
} from '@mui/system';
import React, {
	Children,
	useCallback,
	useMemo
} from 'react';
import type {
	CSSProperties,
	FunctionComponent,
	ReactElement,
	SyntheticEvent
} from 'react';
import type {
	DrawerProps,
	Orientation,
	TabProps
} from 'ui-router-react-digest';
import './DrawerHeader.css';

const DrawerHeader: FunctionComponent<DrawerHeaderProps> = (
	{
		children,
		drawerIndex,
		onDrawerSelect,
		orientation,
		title
	}: DrawerHeaderProps
): ReactElement => {
	const dynamicStyles = useMemo(
		() => {
			return {
				title: {
					textAlign: orientation === 'right' ?
						'left' :
						'right'
				},
				toolbar: {
					backgroundColor: 'grey.100',
					flexDirection: orientation === 'right' ?
						'row-reverse' :
						'row'
				}
			} as {[key: string]: SxProps<Theme>};
		},
		[
			orientation
		]
	);

	const handleDrawerSelect = useCallback(
		(
			event: SyntheticEvent<Element, Event>,
			index: number
		): void => {
			onDrawerSelect(index);
		},
		[
			onDrawerSelect
		]
	);

	const tabs = Children.toArray(children).length > 1 ?
		Children.toArray(children).map(
			(
				drawer,
				index
			): ReactElement<TabProps> => {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-expect-error
				// eslint-disable-next-line unicorn/prevent-abbreviations
				const params = drawer.props.state?.params ?
					// eslint-disable-next-line @typescript-eslint/ban-ts-comment
					// @ts-expect-error
					drawer.props.state.params :
					{};
				const text = params.title ?
					params.title :
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-expect-error
					drawer.props.name;

				return (
					<Tab
						aria-controls={`page-tab-${index}`}
						aria-label={text}
						data-testid={`page-tab-${text}`}
						disabled={drawerIndex === index}
						icon={params.icon}
						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-expect-error
						key={drawer.props.name}
						styleName='tab'
					/>
				);
			}
		) :
		null;

	return (
		<Paper
			elevation={2}
			styleName='paper'
		>
			<Toolbar
				styleName='toolbar'
				sx={dynamicStyles.toolbar}
			>
				<Tabs
					aria-label='page tabs'
					onChange={handleDrawerSelect}
					scrollButtons='auto'
					value={drawerIndex}
				>
					{tabs}
				</Tabs>
				<h1
					style={dynamicStyles.title as CSSProperties}
					styleName='title'
				>
					{title}
				</h1>
			</Toolbar>
		</Paper>
	);
};

export default DrawerHeader;

export type DrawerHeaderProps = {
	children: Array<ReactElement<DrawerProps>> | ReactElement<DrawerProps>;
	drawerIndex: number;
	onDrawerSelect: (index: number) => void;
	orientation: Orientation;
	title: string;
};
