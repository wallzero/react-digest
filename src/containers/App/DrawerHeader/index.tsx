import {
	connect
} from 'react-redux';
import {
	onDrawerSelect
} from 'src/redux/actions/drawerSettings';
import defaultState from 'src/redux/defaultState';
import DrawerHeader from './DrawerHeader';
import type {
	DrawerHeaderProps
} from './DrawerHeader';

const mapDispatchToProps = {
	onDrawerSelect
};

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		children
	}: Pick<
	Omit<DrawerHeaderProps, keyof typeof mapDispatchToProps>,
	'children'
	>
): Omit<DrawerHeaderProps, keyof typeof mapDispatchToProps> => {
	return {
		children,
		drawerIndex: state.drawerSettings.index,
		orientation: state.orientation,
		title: state.miscellaneous.title
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(
	DrawerHeader
);

export {
	mapStateToProps
};
