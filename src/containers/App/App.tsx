import {
	DSRPlugin
} from '@uirouter/dsr';
import {
	hashLocationPlugin,

	// pushStateLocationPlugin,
	servicesPlugin,
	UIRouterReact
} from '@uirouter/react';
import {
	StickyStatesPlugin
} from '@uirouter/sticky-states';
import React, {
	memo,
	useMemo,
	useRef
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import {
	Drawer,
	Drawers,
	generateStateName,
	Tab,
	Tabs,
	UIRouterReactDigest
} from 'ui-router-react-digest';
import type {
	DrawerProps,
	Orientation,
	TabProps
} from 'ui-router-react-digest';
import DrawerHeader from './DrawerHeader';
import TabHeader from './TabHeader';
import drawerMap from './drawers';
import tabMap from './tabs';

// import {Visualizer} from '@uirouter/visualizer';

let title: Element;

const rootState = {
	name: 'root',
	params: {
		lang: {
			dynamic: true,
			value: 'en'
		}
	},
	url: '/:lang'
};

const genRouter = function *(
	router: UIRouterReact
): IterableIterator<UIRouterReact> {
	router.plugin(servicesPlugin);
	router.plugin(hashLocationPlugin);

	// router.plugin(pushStateLocationPlugin);
	router.plugin(DSRPlugin);
	router.plugin(StickyStatesPlugin);

	// router.plugin(Visualizer);

	// In case of no route found, go to first tab
	router.urlService.rules.otherwise(
		(): void => {
			void router.stateService.go(
				generateStateName(
					rootState.name,
					'404'
				),
				{},
				{
					location: false,
					reload: true
				}
			);
		}
	);

	router.transitionService.onSuccess(
		{
			to: generateStateName(
				rootState.name,
				'**'
			)
		},
		(transition): void => {
			/* ignore, just lazy changing page title */
			if (!title) {
				title = document.querySelector('.urrd-header-title');
				if (title) {
					document.querySelector('.urrd-header-title').innerHTML = 'react-ag';
				}
			}

			document.title = transition.targetState().state().params.title;
		}
	);

	while (true) {
		yield router;
	}
};

const initRouter = genRouter(
	new UIRouterReact()
);

const App: FunctionComponent<AppProps> = (
	{
		drawerDocked,
		drawerDrag,
		drawerHover,
		drawerIndex,
		drawerOpen,
		drawers,
		drawerWidth,
		orientation,
		onDrawerOpenToggle = (): void => {},
		onDrawerSelect = (): void => {},
		onTabSelect = (): void => {},
		tabIndex,
		tabs
	}: AppProps
): ReactElement => {
	const router = useRef<UIRouterReact>(initRouter.next().value);

	const drawerOrder: Array<ReactElement<DrawerProps>> = useMemo(
		(): ReactElement[] => {
			return drawers.map(
				(drawer): ReactElement<DrawerProps> => {
					switch (drawer.name) {
						case 'settings': {
							const Component = drawerMap[drawer.name].component;

							return (
								<Drawer
									key={drawer.name}
									name={drawer.name}
									state={{
										...drawer.state,
										...drawerMap[drawer.name].state,
										params: {
											...drawer.state.params,
											...drawerMap[drawer.name].state.params
										}
									}}
								>
									<Component />
								</Drawer>
							);
						}

						default: {
							return (
								<Drawer
									key={drawer.name}
									name={drawer.name}
									state={{
										...drawer.state,
										...drawerMap[drawer.name].state,
										params: {
											...drawer.state.params,
											...drawerMap[drawer.name].state.params
										}
									}}
								/>
							);
						}
					}
				}
			);
		},
		[
			drawers
		]
	);

	const tabOrder: Array<ReactElement<TabProps>> = useMemo(
		(): ReactElement[] => {
			return tabs.map(
				(tab): ReactElement<TabProps> => {
					let type: string;

					switch (tab.state.params.type) {
						case 'components':
						case 'diagram':
						case '404': {
							type = tab.state.params.type;

							break;
						}

						case 'dynamic':
						default: {
							type = 'dynamic';
						}
					}

					const Component = tabMap[type].component;

					return (
						<Tab
							drawers={tabMap[type].drawers}
							footer={tabMap[type].footer}
							hidden={tab.hidden}
							key={tab.name}
							name={tab.name}
							state={tab.state}
							swipe={tab.swipe}
						>
							<Component />
						</Tab>
					);
				}
			);
		},
		[
			tabs
		]
	);

	return (
		<UIRouterReactDigest
			drawerDocked={drawerDocked}
			drawerDrag={drawerDrag}
			drawerFooter={null}
			drawerHeader={DrawerHeader}
			drawerHover={drawerHover}
			drawerIndex={drawerIndex}
			drawerOpen={drawerOpen}
			drawerWidth={drawerWidth}
			footer={null}
			header={TabHeader}
			onDrawerOpenToggle={onDrawerOpenToggle}
			onDrawerSelect={onDrawerSelect}
			onTabSelect={onTabSelect}
			orientation={orientation}
			rootState={rootState}
			router={router.current}
			tabIndex={tabIndex}
		>
			<Drawers>
				{drawerOrder}
			</Drawers>
			<Tabs>
				{tabOrder}
			</Tabs>
		</UIRouterReactDigest>
	);
};

export default memo(App);

export type AppProps = {
	drawerDocked: boolean;
	drawerDrag: boolean;
	drawerHover: boolean;
	drawerIndex: number;
	drawerOpen: boolean;
	drawerWidth: ReactText;
	drawers: DrawerProps[];
	onDrawerOpenToggle: (toggle: boolean) => void;
	onDrawerSelect: (index: number) => void;
	onTabSelect: (index: number) => void;
	orientation: Orientation;
	tabIndex: number;
	tabs: TabProps[];
	title: string;
};
