/*
	eslint-disable
	id-length
*/

import React, {
	memo,
	useCallback,
	useRef
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import type {
	DraggableData,
	DraggableEvent
} from 'react-draggable';
import {
	Graph
} from 'react-yad';
import Edges from './Edges';
import Nodes from './Nodes';

const Diagram: FunctionComponent<DiagramProps> = (
	{
		diagramId,
		maxScale,
		minScale,
		onDiagramUpdate,
		onEdgeAdd,
		onNodesMoveDelta,
		onNodeUpdate,
		onSelectionClear,
		onSelectionToggle,
		scale,
		selections,
		xOffset,
		yOffset
	}: DiagramProps
): ReactElement => {
	const transformed = useRef(false);

	const updateSelectedNodes = useCallback(
		(
			nodeId,
			deltaX: number,
			deltaY: number
		): void => {
			const {
				[nodeId]: omit, // eslint-disable-line @typescript-eslint/no-unused-vars
				...remaining
			// eslint-disable-next-line unicorn/no-array-reduce,unicorn/prefer-object-from-entries
			} = Object.keys(selections).reduce<Nodes>(
				(
					updated,
					id
				): Nodes => {
					updated[id] = {
						x: deltaX,
						y: deltaY
					};

					return updated;
				},
				{}
			);

			if (Object.keys(remaining).length > 0) {
				onNodesMoveDelta(
					diagramId,
					remaining
				);
			}
		},
		[
			diagramId,
			onNodesMoveDelta,
			selections
		]
	);

	const handleMouseUp = useCallback(
		() => {
			if (
				!transformed.current &&
				Object.keys(selections).length > 0
			) {
				onSelectionClear(diagramId);
			}

			transformed.current = false;
		},
		[
			diagramId,
			onSelectionClear,
			selections
		]
	);

	const handleTransform = useCallback(
		(
			x: number,
			y: number,
			z: number
		) => {
			if (
				x !== xOffset ||
				y !== yOffset ||
				z !== scale
			) {
				transformed.current = true;

				onDiagramUpdate(
					diagramId,
					{
						scale: z,
						xOffset: x,
						yOffset: y
					}
				);
			}
		},
		[
			diagramId,
			onDiagramUpdate,
			scale,
			xOffset,
			yOffset
		]
	);

	const handleEdgeDrop = useCallback(
		(
			sourceId: ReactText,
			targetId: ReactText
		): void => {
			if (targetId) {
				onEdgeAdd(
					diagramId,
					{
						source: sourceId.toString(),
						target: targetId.toString()
					}
				);
			}
		},
		[
			diagramId,
			onEdgeAdd
		]
	);

	const handleNodeStart = useCallback(
		(event: DraggableEvent) => {
			event.stopPropagation();
		},
		[]
	);

	const handleNodeDrag = useCallback(
		(
			event: DraggableEvent,
			nodeId: ReactText,
			data: DraggableData
		) => {
			event.stopPropagation();

			if (
				selections[nodeId]
			) {
				updateSelectedNodes(
					nodeId,
					data.deltaX,
					data.deltaY
				);
			}
		},
		[
			selections,
			updateSelectedNodes
		]
	);

	const handleNodeStop = useCallback(
		(
			event: DraggableEvent,
			nodeId: ReactText,
			data: DraggableData
		) => {
			onNodeUpdate(
				diagramId,
				nodeId,
				{
					x: data.x,
					y: data.y
				}
			);
		},
		[
			diagramId,
			onNodeUpdate
		]
	);

	const handleNodeSelect = useCallback(
		(
			nodeId: ReactText
		) => {
			onSelectionToggle(
				diagramId,
				nodeId
			);
		},
		[
			diagramId,
			onSelectionToggle
		]
	);

	return (
		<Graph
			id={diagramId}
			line='direct'
			maxScale={maxScale}
			minScale={minScale}
			onEdgeDrop={handleEdgeDrop}
			onMouseUp={handleMouseUp}
			onNodeDrag={handleNodeDrag}
			onNodeSelect={handleNodeSelect}
			onNodeStart={handleNodeStart}
			onNodeStop={handleNodeStop}
			onTouchEnd={handleMouseUp}
			onTransform={handleTransform}
			scale={scale}
			shape='oval'
			xOffset={xOffset}
			yOffset={yOffset}
		>
			<Nodes
				diagramId={diagramId}
			/>
			<Edges
				diagramId={diagramId}
			/>
		</Graph>
	);
};

export default memo(Diagram);

export type DiagramProps = {
	diagramId: ReactText;
	maxScale: number;
	minScale: number;
	onDiagramUpdate: (
		diagramId: ReactText,
		diagram: Partial<Diagram>
	) => void;
	onEdgeAdd: (
		diagramId: ReactText,
		edge: Edge
	) => void;
	onNodeUpdate: (
		diagramId: ReactText,
		nodeId: ReactText,
		node: DNode,
	) => void;
	onNodesMoveDelta: (
		diagramId: ReactText,
		nodes: Nodes,
	) => void;
	onSelectionClear: (
		diagramId: ReactText
	) => void;
	onSelectionToggle: (
		diagramId: ReactText,
		nodeId: ReactText
	) => void;
	scale: number;
	selections: Selections;
	xOffset: number;
	yOffset: number;
};
