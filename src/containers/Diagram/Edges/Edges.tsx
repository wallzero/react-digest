import React from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import {
	Edges as YadEdges
} from 'react-yad';
import Edge from './Edge';

const Edges: FunctionComponent<EdgesProps> = (
	{
		diagramId,
		edgeOrder
	}: EdgesProps
): ReactElement => {
	const edgeMap = edgeOrder.map(
		({
			id
		}): ReactElement => {
			return (
				<Edge
					diagramId={diagramId}
					edgeId={id}
					key={id}
				/>
			);
		}
	);

	return (
		<YadEdges>
			{edgeMap}
		</YadEdges>
	);
};

export default Edges;

export type EdgesProps = {
	diagramId: ReactText;
	edgeOrder: EdgeOrder;
};
