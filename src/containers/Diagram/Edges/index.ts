import {
	connect
} from 'react-redux';
import defaultState from 'src/redux/defaultState';
import Edges from './Edges';
import type {
	EdgesProps
} from './Edges';

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId
	}: Pick<EdgesProps, 'diagramId'>
): EdgesProps => {
	return {
		diagramId,
		edgeOrder: state.edgeOrders[diagramId] || []
	};
};

export default connect(
	mapStateToProps
)(Edges);

export {
	mapStateToProps
};

