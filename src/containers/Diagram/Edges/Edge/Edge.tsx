import React from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import {
	Edge as YadEdge
} from 'react-yad';
import type {
	Line
} from 'react-yad';
import RemoveEdgeButton from './RemoveEdgeButton';
import './Edge.css';

const Edge: FunctionComponent<EdgeProps> = (
	{
		diagramId,
		edgeId,
		line,
		sourceId,
		targetId
	}: EdgeProps
): ReactElement => {
	return (
		<YadEdge
			id={edgeId}
			line={line}
			sourceId={sourceId}
			targetId={targetId}
		>
			<foreignObject
				height='1'
				styleName='foreign'
				width='1'
			>
				<div
					styleName='center'
				>
					<RemoveEdgeButton
						diagramId={diagramId}
						edgeId={edgeId}
					/>
				</div>
			</foreignObject>
		</YadEdge>
	);
};

export default Edge;

export type EdgeProps = {
	diagramId: ReactText;
	edgeId: ReactText;
	line: Line;
	sourceId: ReactText;
	targetId: ReactText;
};
