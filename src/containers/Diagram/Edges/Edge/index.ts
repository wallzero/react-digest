import type {
	ReactText
} from 'react';
import {
	connect
} from 'react-redux';
import defaultState from 'src/redux/defaultState';
import Edge from './Edge';
import type {
	EdgeProps
} from './Edge';

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId,
		edgeId
	}: Pick<EdgeProps, 'edgeId'> & {diagramId: ReactText}
): EdgeProps => {
	const {
		line,
		source: sourceId,
		target: targetId
	} = state.edges[diagramId][edgeId];

	return {
		diagramId,
		edgeId,
		line: line || 'direct',
		sourceId,
		targetId
	};
};

export default connect(
	mapStateToProps
)(Edge);

export {
	mapStateToProps
};
