import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import Fab from '@mui/material/Fab';
import React, {
	useCallback,
	useEffect,
	useState
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import RemoveEdgeDialogue from './RemoveEdgeDialogue';
import './RemoveEdge.css';

const CLICK_TIME = 500;

const RemoveEdgeButton: FunctionComponent<RemoveEdgeButtonProps> = (
	{
		diagramId,
		edgeId
	}: RemoveEdgeButtonProps
): ReactElement => {
	const [
		clickTimeout,
		setClickTimeout
	] = useState<NodeJS.Timeout>(null);

	useEffect(
		(): () => void => {
			return (): void => {
				clearTimeout(clickTimeout);
			};
		},
		[
			clickTimeout
		]
	);

	const [
		open,
		setOpen
	] = useState<boolean>(false);

	const handleClose = useCallback(
		(): void => {
			clearTimeout(clickTimeout);
			setClickTimeout(null);
			setOpen(false);
		},
		[
			clickTimeout
		]
	);

	const handleOpen = useCallback(
		(): void => {
			if (clickTimeout) {
				setOpen(true);

				clearTimeout(clickTimeout);
				setClickTimeout(null);
			} else {
				setClickTimeout(
					setTimeout(
						(): void => {
							setClickTimeout(null);
						},
						CLICK_TIME
					)
				);
			}
		},
		[
			clickTimeout
		]
	);

	return (
		<>
			<Fab
				color={clickTimeout ?
					'secondary' :
					'primary'}
				data-testid='remove-edge-button'
				onMouseUp={handleOpen}
				onTouchEnd={handleOpen}
				size='small'
				styleName='edge-remove'
			>
				<CloseRoundedIcon
					fontSize='small'
					styleName='edge-remove-icon'
				/>
			</Fab>
			<RemoveEdgeDialogue
				diagramId={diagramId}
				edgeId={edgeId}
				onClose={handleClose}
				open={open}
			/>
		</>
	);
};

export default RemoveEdgeButton;

export type RemoveEdgeButtonProps = {
	diagramId: ReactText;
	edgeId: ReactText;
};
