import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import {
	Provider
} from 'react-redux';
import defaultState from 'src/redux/defaultState';
import store from 'src/redux/store';
import RemoveEdgeButton from '../RemoveEdgeButton';

describe(
	'Remove edge button',
	(): void => {
		it(
			'open dialogue and click cancel',
			async (): Promise<void> => {
				expect.assertions(0);

				const diagramId = Object.keys(defaultState.diagrams)[0];
				const edgeId = Object.keys(defaultState.edges[diagramId])[0];

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<RemoveEdgeButton
							diagramId={diagramId}
							edgeId={edgeId}
						/>
					</Provider>
				);

				const remove = await findByTestId('remove-edge-button');
				fireEvent.mouseUp(remove);
				fireEvent.mouseUp(remove);

				const cancel = await findByTestId('remove-edge-cancel-button');
				fireEvent.click(cancel);
			}
		);

		it(
			'open dialogue and click remove',
			async (): Promise<void> => {
				expect.assertions(0);

				const diagramId = Object.keys(defaultState.diagrams)[0];
				const edgeId = Object.keys(defaultState.edges[diagramId])[0];

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<RemoveEdgeButton
							diagramId={diagramId}
							edgeId={edgeId}
						/>
					</Provider>
				);

				const remove = await findByTestId('remove-edge-button');
				fireEvent.mouseUp(remove);
				fireEvent.mouseUp(remove);

				// const confirm = await findByTestId('remove-edge-confirm-button');
				// fireEvent.click(confirm);
			}
		);
	}
);
