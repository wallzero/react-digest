import {
	connect
} from 'react-redux';
import RemoveEdgeButton from './RemoveEdgeButton';
import type {
	RemoveEdgeButtonProps
} from './RemoveEdgeButton';

const mapDispatchToProps = {};

const mapStateToProps = (
	_: unknown,
	{
		diagramId,
		edgeId
	}: Pick<
	Omit<RemoveEdgeButtonProps, keyof typeof mapDispatchToProps>,
	'diagramId' |
	'edgeId'
	>
): Omit<RemoveEdgeButtonProps, keyof typeof mapDispatchToProps> => {
	return {
		diagramId,
		edgeId
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveEdgeButton);

export {
	mapStateToProps
};
