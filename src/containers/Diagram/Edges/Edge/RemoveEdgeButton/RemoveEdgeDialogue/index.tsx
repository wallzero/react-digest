import {
	connect
} from 'react-redux';
import RemoveEdge from './RemoveEdgeDialogue';
import type {
	RemoveEdgeDialogueProps
} from './RemoveEdgeDialogue';

const mapDispatchToProps = {};

const mapStateToProps = (
	_: unknown,
	{
		diagramId,
		edgeId,
		onClose,
		open
	}: Pick<
	Omit<RemoveEdgeDialogueProps, keyof typeof mapDispatchToProps>,
	'diagramId' |
	'edgeId' |
	'onClose' |
	'open'
	>
): Omit<RemoveEdgeDialogueProps, keyof typeof mapDispatchToProps> => {
	return {
		diagramId,
		edgeId,
		onClose,
		open
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveEdge);

export {
	mapStateToProps
};
