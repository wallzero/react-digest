import DeleteRoundedIcon from '@mui/icons-material/DeleteRounded';
import Button from '@mui/material/Button';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';

const styles = {
	icon: {
		marginLeft: -1,
		marginRight: 1
	}
};

const RemoveEdgeConfirm: FunctionComponent<RemoveEdgeConfirmProps> = (
	{
		diagramId,
		edgeId,
		onEdgesRemove,
		source,
		target
	}: RemoveEdgeConfirmProps
): ReactElement => {
	const handleClick = useCallback(
		(): void => {
			onEdgesRemove(
				diagramId,
				{
					[edgeId]: {
						source,
						target
					}
				}
			);
		},
		[
			diagramId,
			edgeId,
			onEdgesRemove,
			source,
			target
		]
	);

	return (
		<Button
			color='secondary'
			data-testid='remove-edge-confirm-button'
			onClick={handleClick}
			variant='contained'
		>
			<DeleteRoundedIcon
				fontSize='small'
				sx={styles.icon}
			/>
			Remove
		</Button>
	);
};

export type RemoveEdgeConfirmProps = {
	diagramId: ReactText;
	edgeId: ReactText;
	onEdgesRemove: (
		diagramId: ReactText,
		edges: Edges,
	) => void;
	source: ReactText;
	target: ReactText;
};

export default RemoveEdgeConfirm;
