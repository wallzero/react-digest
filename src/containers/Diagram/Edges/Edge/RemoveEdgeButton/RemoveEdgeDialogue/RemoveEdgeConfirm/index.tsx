import {
	connect
} from 'react-redux';
import {
	onEdgesRemove
} from 'src/redux/actions/edges';
import defaultState from 'src/redux/defaultState';
import RemoveEdge from './RemoveEdgeConfirm';
import type {
	RemoveEdgeConfirmProps
} from './RemoveEdgeConfirm';

const mapDispatchToProps = {
	onEdgesRemove
};

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId,
		edgeId
	}: Pick<
	Omit<RemoveEdgeConfirmProps, keyof typeof mapDispatchToProps>,
	'diagramId' |
	'edgeId'
	>
): Omit<RemoveEdgeConfirmProps, keyof typeof mapDispatchToProps> => {
	const {
		source,
		target
	} = state.edges[diagramId][edgeId];

	return {
		diagramId,
		edgeId,
		source,
		target
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveEdge);

export {
	mapStateToProps
};
