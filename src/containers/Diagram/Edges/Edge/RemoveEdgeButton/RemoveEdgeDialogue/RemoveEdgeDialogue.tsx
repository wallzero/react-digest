import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import RemoveEdgeConfirm from './RemoveEdgeConfirm';
import Transition from './Transition';

const styles = {
	icon: {
		marginLeft: -1,
		marginRight: 1
	}
};

const RemoveEdgeDialogue: FunctionComponent<RemoveEdgeDialogueProps> = (
	{
		diagramId,
		edgeId,
		onClose,
		open
	}: RemoveEdgeDialogueProps
): ReactElement => {
	return (
		<Dialog
			TransitionComponent={Transition}
			aria-describedby='Confirm or cancel to remove edge'
			aria-labelledby='Remove edge confirm dialogue'
			onClose={onClose}
			open={open}
		>
			<DialogTitle>
				Remove node?
			</DialogTitle>
			<DialogActions>
				<Button
					color='primary'
					data-testid='remove-edge-cancel-button'
					onClick={onClose}
					variant='contained'
				>
					<CancelRoundedIcon
						fontSize='small'
						sx={styles.icon}
					/>
					Cancel
				</Button>
				<RemoveEdgeConfirm
					diagramId={diagramId}
					edgeId={edgeId}
				/>
			</DialogActions>
		</Dialog>
	);
};

export type RemoveEdgeDialogueProps = {
	diagramId: ReactText;
	edgeId: ReactText;
	onClose: () => void;
	open: boolean;
};

export default RemoveEdgeDialogue;
