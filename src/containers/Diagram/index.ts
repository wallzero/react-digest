import {
	connect
} from 'react-redux';
import {
	onDiagramUpdate
} from 'src/redux/actions/diagrams';
import {
	onEdgeAdd
} from 'src/redux/actions/edges';
import {
	onNodesMoveDelta,
	onNodeUpdate
} from 'src/redux/actions/nodes';
import {
	onSelectionClear,
	onSelectionToggle
} from 'src/redux/actions/selections';
import defaultState from 'src/redux/defaultState';
import Diagram from './Diagram';
import type {
	DiagramProps
} from './Diagram';

const mapDispatchToProps = {
	onDiagramUpdate,
	onEdgeAdd,
	onNodesMoveDelta,
	onNodeUpdate,
	onSelectionClear,
	onSelectionToggle
};

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId
	}: Pick<
	Omit<DiagramProps, keyof typeof mapDispatchToProps>,
	'diagramId'
	>
): Omit<DiagramProps, keyof typeof mapDispatchToProps> => {
	return state.diagrams[diagramId] ?
		{
			diagramId,
			maxScale: state.diagrams[diagramId].maxScale,
			minScale: state.diagrams[diagramId].minScale,
			scale: state.diagrams[diagramId].scale,
			selections: state.selections[diagramId],
			xOffset: state.diagrams[diagramId].xOffset,
			yOffset: state.diagrams[diagramId].yOffset
		} :
		null;
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Diagram);

export {
	mapStateToProps
};
