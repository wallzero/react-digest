import {
	connect
} from 'react-redux';
import defaultState from 'src/redux/defaultState';
import Nodes from './Nodes';
import type {
	NodesProps
} from './Nodes';

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId
	}: Pick<NodesProps, 'diagramId'>
): NodesProps => {
	return {
		diagramId,
		nodes: state.nodeOrders[diagramId] || []
	};
};

export default connect(
	mapStateToProps
)(Nodes);

export {
	mapStateToProps
};

