import React from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import {
	Nodes as YadNodes
} from 'react-yad';
import Node from './Node';

const Nodes: FunctionComponent<NodesProps> = (
	{
		diagramId,
		nodes
	}: NodesProps
): ReactElement => {
	const nodeMap = nodes.map(
		(nodeId): ReactElement => {
			return (
				<Node
					diagramId={diagramId}
					key={nodeId}
					nodeId={nodeId}
				/>
			);
		}
	);

	return (
		<YadNodes>
			{nodeMap}
		</YadNodes>
	);
};

export default Nodes;

export type NodesProps = {
	diagramId: ReactText;
	nodes: NodeOrder;
};
