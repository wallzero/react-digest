import {
	faArrowsAlt as ArrowsAltIcon,
	faBezierCurve as BezierCurveIcon,
	faPlus as PlusIcon
} from '@fortawesome/free-solid-svg-icons';
import {
	FontAwesomeIcon
} from '@fortawesome/react-fontawesome';
import type {
	Theme
} from '@mui/material';
import Fab from '@mui/material/Fab';
import {
	alpha
} from '@mui/material/styles';
import type {
	SxProps
} from '@mui/system';
import React, {
	useCallback,
	useContext,
	useMemo,
	useState
} from 'react';
import type {
	FunctionComponent,
	MouseEvent,
	ReactElement,
	ReactText,
	TouchEvent
} from 'react';
import {
	ModeContext,
	Node as YadNode
} from 'react-yad';
import NodeDialogue from './NodeDialogue';
import NodePopover from './NodePopover';
import './Node.css';

const styles = {
	fab: {
		'&:active': {
			boxShadow: (theme) => {
				return `
				0px 7px 8px -4px ${alpha(
		theme.palette.secondary.main,
		0.4
	)},
				0px 12px 17px 2px ${alpha(
		theme.palette.secondary.main,
		0.28
	)},
				0px 5px 22px 4px ${alpha(
		theme.palette.secondary.main,
		0.24
	)};
				`;
			}
		},
		boxShadow: (theme) => {
			return `
					0px 3px 5px -1px ${alpha(
		theme.palette.secondary.main,
		0.4
	)},
					0px 6px 10px 0px ${alpha(
		theme.palette.secondary.main,
		0.28
	)},
					0px 1px 18px 0px ${alpha(
		theme.palette.secondary.main,
		0.24
	)}
			`;
		}
	}
} as {[key: string]: SxProps<Theme>};

const Node: FunctionComponent<NodeProps> = (
	{
		diagramId,
		height = 50,
		nodeId,
		selected,
		width = 50,
		x,
		y
	}: NodeProps
): ReactElement => {
	const mode = useContext(ModeContext) || 'move';

	const [
		anchorElement,
		setAnchorElement
	] = useState<HTMLElement>();

	const [
		openPopover,
		setOpenPopover
	] = useState(false);

	const [
		openDialogue,
		setOpenDialogue
	] = useState(false);

	const handleActivate = useCallback(
		(
			event: MouseEvent<HTMLElement> | TouchEvent<HTMLElement>
		): void => {
			setAnchorElement(event.currentTarget);
			setOpenPopover(true);
		},
		[]
	);

	const handleClosePopover = useCallback(
		(): void => {
			setAnchorElement(null);
			setOpenPopover(false);
		},
		[]
	);

	const handleEdit = useCallback(
		(): void => {
			setOpenPopover(false);
			setOpenDialogue(true);
		},
		[]
	);

	const handleCloseDialogue = useCallback(
		(): void => {
			setAnchorElement(null);
			setOpenDialogue(false);
		},
		[]
	);

	const handleMouseUp = useCallback(
		(
			event: MouseEvent<HTMLElement>
		) => {
			handleActivate(event);
		},
		[
			handleActivate
		]
	);

	const handleTouchEnd = useCallback(
		(
			event: TouchEvent<HTMLElement>
		) => {
			handleActivate(event);
		},
		[
			handleActivate
		]
	);

	const icon = useMemo(
		() => {
			switch (mode) {
				case 'pull': {
					return BezierCurveIcon;
				}

				case 'select': {
					return PlusIcon;
				}

				case 'move':
				default: {
					return ArrowsAltIcon;
				}
			}
		},
		[
			mode
		]
	);

	return (
		<>
			<YadNode
				height={height}
				id={nodeId}
				shape='oval'
				width={width}
				x={x}
				y={y}
			>
				<Fab
					color='primary'
					disableRipple
					onMouseUp={handleMouseUp}
					onTouchEnd={handleTouchEnd}
					styleName='interaction'
					sx={
						selected ?
							styles.fab :
							undefined
					}
				>
					<FontAwesomeIcon
						icon={icon}
					/>
				</Fab>
			</YadNode>
			<NodePopover
				anchorElement={anchorElement}
				diagramId={diagramId}
				nodeId={nodeId}
				onClose={handleClosePopover}
				onEdit={handleEdit}
				open={openPopover}
			/>
			<NodeDialogue
				onClose={handleCloseDialogue}
				open={openDialogue}
			/>
		</>
	);
};

export default Node;

export type NodeProps = {
	diagramId: ReactText;
	height?: number;
	nodeId: ReactText;
	selected: boolean;
	width?: number;
	x: number;
	y: number;
};
