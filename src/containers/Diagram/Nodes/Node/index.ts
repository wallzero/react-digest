import {
	connect
} from 'react-redux';
import defaultState from 'src/redux/defaultState';
import Node from './Node';
import type {
	NodeProps
} from './Node';

const mapDispatchToProps = {};

const mapStateToProps = (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState,
	{
		diagramId,
		nodeId
	}: Pick<
	Omit<NodeProps, keyof typeof mapDispatchToProps>,
	'diagramId' |
	'nodeId'
	>
): Omit<NodeProps, keyof typeof mapDispatchToProps> => {
	const {
		height,
		width,
		x,
		y
	} = state.nodes[diagramId][nodeId];

	return {
		diagramId,
		height,
		nodeId,
		selected: state.selections[diagramId][nodeId],
		width,
		x,
		y
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Node);

export {
	mapStateToProps
};

