import EditIcon from '@mui/icons-material/Edit';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import RemoveNodeButton from './RemoveNodeButton';
import './NodePopover.css';

const anchorOrigin = {
	horizontal: 'center' as const,
	vertical: 'bottom' as const
};

const transformOrigin = {
	horizontal: 'center' as const,
	vertical: 'top' as const
};

const NodePopover: FunctionComponent<NodePopoverProps> = (
	{
		anchorElement,
		diagramId,
		nodeId,
		onClose,
		onEdit,
		open
	}: NodePopoverProps
): ReactElement => {
	return (
		<Menu
			anchorEl={anchorElement}
			anchorOrigin={anchorOrigin}
			onClose={onClose}
			open={open}
			transformOrigin={transformOrigin}
		>
			<MenuItem
				onClick={onEdit}
			>
				<ListItemIcon>
					<EditIcon
						color='primary'
					/>
				</ListItemIcon>
				<ListItemText>
					Edit Node
				</ListItemText>
			</MenuItem>
			<Divider />
			<ListItem>
				<RemoveNodeButton
					diagramId={diagramId}
					nodeId={nodeId}
				/>
			</ListItem>
		</Menu>
	);
};

export default NodePopover;

export type NodePopoverProps = {
	anchorElement: EventTarget & HTMLElement;
	diagramId: ReactText;
	nodeId: ReactText;
	onClose: () => void;
	onEdit: () => void;
	open: boolean;
};
