import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import {
	Provider
} from 'react-redux';
import defaultState from 'src/redux/defaultState';
import store from 'src/redux/store';
import RemoveNodeButton from '../RemoveNodeButton';

describe(
	'Remove node button',
	(): void => {
		it(
			'open dialogue and click cancel',
			async (): Promise<void> => {
				expect.assertions(0);

				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = Object.keys(defaultState.nodes[diagramId])[0];

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<RemoveNodeButton
							diagramId={diagramId}
							nodeId={nodeId}
						/>
					</Provider>
				);

				const remove = await findByTestId('remove-node-button');
				fireEvent.click(remove);

				const cancel = await findByTestId('remove-node-cancel-button');
				fireEvent.click(cancel);
			}
		);

		it(
			'open dialogue and click remove',
			async (): Promise<void> => {
				expect.assertions(0);

				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = Object.keys(defaultState.nodes[diagramId])[0];

				const {
					findByTestId
				} = render(
					<Provider
						store={store}
					>
						<RemoveNodeButton
							diagramId={diagramId}
							nodeId={nodeId}
						/>
					</Provider>
				);

				const remove = await findByTestId('remove-node-button');
				fireEvent.click(remove);

				const confirm = await findByTestId('remove-node-confirm-button');
				fireEvent.click(confirm);
			}
		);
	}
);
