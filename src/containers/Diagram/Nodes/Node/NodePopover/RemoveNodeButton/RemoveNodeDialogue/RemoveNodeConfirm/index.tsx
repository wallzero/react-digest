import {
	connect
} from 'react-redux';
import {
	onNodesRemove
} from 'src/redux/actions/nodes';
import RemoveEdge from './RemoveNodeConfirm';
import type {
	RemoveNodeConfirmProps
} from './RemoveNodeConfirm';

const mapDispatchToProps = {
	onNodesRemove
};

const mapStateToProps = (
	_: unknown,
	{
		diagramId,
		nodeId
	}: Pick<
	Omit<RemoveNodeConfirmProps, keyof typeof mapDispatchToProps>,
	'diagramId' |
	'nodeId'
	>
): Omit<RemoveNodeConfirmProps, keyof typeof mapDispatchToProps> => {
	return {
		diagramId,
		nodeId
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveEdge);

export {
	mapStateToProps
};
