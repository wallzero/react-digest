import DeleteRoundedIcon from '@mui/icons-material/DeleteRounded';
import Button from '@mui/material/Button';
import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';

const styles = {
	icon: {
		marginLeft: -1,
		marginRight: 1
	}
};

const RemoveNodeConfirm: FunctionComponent<RemoveNodeConfirmProps> = (
	{
		diagramId,
		nodeId,
		onNodesRemove
	}: RemoveNodeConfirmProps
): ReactElement => {
	const handleClick = useCallback(
		(): void => {
			onNodesRemove(
				diagramId,
				[
					nodeId.toString()
				]
			);
		},
		[
			diagramId,
			nodeId,
			onNodesRemove
		]
	);

	return (
		<Button
			color='secondary'
			data-testid='remove-node-confirm-button'
			onClick={handleClick}
			variant='contained'
		>
			<DeleteRoundedIcon
				fontSize='small'
				sx={styles.icon}
			/>
			Remove
		</Button>
	);
};

export type RemoveNodeConfirmProps = {
	diagramId: ReactText;
	nodeId: ReactText;
	onNodesRemove: (
		diagramId: ReactText,
		nodes: ReactText[]
	) => void;
};

export default RemoveNodeConfirm;
