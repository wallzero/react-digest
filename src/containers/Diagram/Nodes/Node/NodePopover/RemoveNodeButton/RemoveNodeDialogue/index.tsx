import {
	connect
} from 'react-redux';
import RemoveEdge from './RemoveNodeDialogue';
import type {
	RemoveNodeDialogueProps
} from './RemoveNodeDialogue';

const mapDispatchToProps = {};

const mapStateToProps = (
	_: unknown,
	{
		diagramId,
		nodeId,
		onClose,
		open
	}: Pick<
	Omit<RemoveNodeDialogueProps, keyof typeof mapDispatchToProps>,
	'diagramId' |
	'nodeId' |
	'onClose' |
	'open'
	>
): Omit<RemoveNodeDialogueProps, keyof typeof mapDispatchToProps> => {
	return {
		diagramId,
		nodeId,
		onClose,
		open
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveEdge);

export {
	mapStateToProps
};
