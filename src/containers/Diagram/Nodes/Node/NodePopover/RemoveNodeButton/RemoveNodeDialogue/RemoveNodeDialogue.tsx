import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import RemoveNodeConfirm from './RemoveNodeConfirm';
import Transition from './Transition';

const styles = {
	icon: {
		marginLeft: -1,
		marginRight: 1
	}
};

const RemoveNodeDialogue: FunctionComponent<RemoveNodeDialogueProps> = (
	{
		diagramId,
		nodeId,
		onClose,
		open
	}: RemoveNodeDialogueProps
): ReactElement => {
	return (
		<Dialog
			TransitionComponent={Transition}
			aria-describedby='Confirm or cancel to remove node'
			aria-labelledby='Remove node confirm dialogue'
			onClose={onClose}
			open={open}
		>
			<DialogTitle>
				Remove node?
			</DialogTitle>
			<DialogActions>
				<Button
					color='primary'
					data-testid='remove-node-cancel-button'
					onClick={onClose}
					variant='contained'
				>
					<CancelRoundedIcon
						fontSize='small'
						sx={styles.icon}
					/>
					Cancel
				</Button>
				<RemoveNodeConfirm
					diagramId={diagramId}
					nodeId={nodeId}
				/>
			</DialogActions>
		</Dialog>
	);
};

export type RemoveNodeDialogueProps = {
	diagramId: ReactText;
	nodeId: ReactText;
	onClose: () => void;
	open: boolean;
};

export default RemoveNodeDialogue;
