import {
	connect
} from 'react-redux';
import RemoveNodeButton from './RemoveNodeButton';
import type {
	RemoveNodeButtonProps
} from './RemoveNodeButton';

const mapDispatchToProps = {};

const mapStateToProps = (
	_: unknown,
	{
		diagramId,
		nodeId
	}: Pick<
	Omit<RemoveNodeButtonProps, keyof typeof mapDispatchToProps>,
	'diagramId' |
	'nodeId'
	>
): Omit<RemoveNodeButtonProps, keyof typeof mapDispatchToProps> => {
	return {
		diagramId,
		nodeId
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveNodeButton);

export {
	mapStateToProps
};
