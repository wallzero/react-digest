import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import React, {
	useCallback,
	useState
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import RemoveNodeDialogue from './RemoveNodeDialogue';

const styles = {
	icon: {
		marginRight: 2
	}
};

const RemoveNodeButton: FunctionComponent<RemoveNodeButtonProps> = (
	{
		diagramId,
		nodeId
	}: RemoveNodeButtonProps
): ReactElement => {
	const [
		open,
		setOpen
	] = useState<boolean>(false);

	const handleClose = useCallback(
		(): void => {
			setOpen(false);
		},
		[]
	);

	const handleOpen = useCallback(
		(): void => {
			setOpen(true);
		},
		[]
	);

	return (
		<>
			<Button
				color='secondary'
				data-testid='remove-node-button'
				fullWidth
				onClick={handleOpen}
				variant='contained'
			>
				<DeleteIcon
					fontSize='small'
					sx={styles.icon}
				/>
				Remove Node
			</Button>
			<RemoveNodeDialogue
				diagramId={diagramId}
				nodeId={nodeId}
				onClose={handleClose}
				open={open}
			/>
		</>
	);
};

export default RemoveNodeButton;

export type RemoveNodeButtonProps = {
	diagramId: ReactText;
	nodeId: ReactText;
};
