import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import Transition from './Transition';

const NodeDialogue: FunctionComponent<NodeDialogueProps> = (
	{
		onClose,
		open
	}: NodeDialogueProps
): ReactElement => {
	return (
		<Dialog
			TransitionComponent={Transition}
			onClose={onClose}
			open={open}
		>
			<DialogTitle>
				Set backup account
			</DialogTitle>
		</Dialog>
	);
};

export default NodeDialogue;

export type NodeDialogueProps = {
	onClose: () => void;
	open: boolean;
};
