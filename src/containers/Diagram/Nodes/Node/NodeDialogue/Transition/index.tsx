import Grow from '@mui/material/Grow';
import type {
	TransitionProps
} from '@mui/material/transitions';
import React, {
	forwardRef
} from 'react';
import type {
	ReactElement
} from 'react';

const Transition = forwardRef<unknown, TransitionProps>(
	(
		props,
		ref
	): ReactElement => {
		return (
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			<Grow
				in
				ref={ref}
				{...props}
			/>
		);
	}
);

export default Transition;
