import type {
	Theme
} from '@mui/material';
import ButtonBase from '@mui/material/ButtonBase';
import Typography from '@mui/material/Typography';
import {
	styled
} from '@mui/system';
import type {
	SxProps
} from '@mui/system';
import React, {
	useMemo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './CustomButtons.css';

const ImageButton = styled(ButtonBase)(({
	theme
}) => {
	return {
		'&:hover, &.Mui-focusVisible': {
			'& .MuiImageBackdrop-root': {
				opacity: 0.15
			},
			'& .MuiImageMarked-root': {
				opacity: 0
			},
			'& .MuiTypography-root': {
				border: '4px solid currentColor'
			},
			zIndex: 1
		},
		height: 200,
		[theme.breakpoints.down('sm')]: {
			// Overrides inline-style
			height: 100,
			width: '100% !important'
		},
		position: 'relative'
	};
});

const Image = styled('span')(({
	theme
}) => {
	return {
		color: theme.palette.common.white
	};
});

const ImageBackdrop = styled('span')(({
	theme
}) => {
	return {
		backgroundColor: theme.palette.common.black,
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-expect-error
		transition: theme.transitions.create('opacity')
	};
});

const ImageMarked = styled('span')(({
	theme
}) => {
	return {
		backgroundColor: theme.palette.common.white,
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-expect-error
		transition: theme.transitions.create('opacity')
	};
});

const styles = {
	button: {
		'&:hover $imageTitle': {
			border: '4px solid currentColor'
		}
	},
	imageTitle: {
		px: 4,
		py: 2
	}
} as {[key: string]: SxProps<Theme>};

const images = [
	{
		height: '33%',
		title: 'Breakfast',
		url: 'https://placeimg.com/300/300/bacon'
	},
	{
		height: '33%',
		title: 'People',
		url: 'https://placeimg.com/300/300/people'
	},
	{
		height: '33%',
		title: 'Camera',
		url: 'https://placeimg.com/300/300/arch'
	}
];

const CustomButtons: FunctionComponent = (): ReactElement => {
	const buttonStyles = useMemo(
		() => {
			return images.map(
				(image) => {
					return {
						height: image.height
					};
				}
			);
		},
		[]
	);

	const spanStyles = useMemo(
		() => {
			return images.map(
				(image) => {
					return {
						backgroundImage: `url(${image.url})`
					};
				}
			);
		},
		[]
	);

	const buttons = images.map(
		(
			image,
			index
		): ReactElement => {
			return (
				<ImageButton
					focusRipple
					key={image.title}
					style={buttonStyles[index]}
					styleName='image'
				>
					<span
						style={spanStyles[index]}
						styleName='image-src'
					/>
					<ImageBackdrop
						styleName='image-backdrop'
					/>
					<Image
						styleName='image-button'
					>
						<Typography
							color='inherit'
							component='span'
							styleName='image-title'
							sx={styles.imageTitle}
						>
							{image.title}
							<ImageMarked
								styleName='image-marked'
							/>
						</Typography>
					</Image>
				</ImageButton>
			);
		}
	);

	return (
		<div
			styleName='root'
		>
			{buttons}
		</div>
	);
};

export default CustomButtons;
