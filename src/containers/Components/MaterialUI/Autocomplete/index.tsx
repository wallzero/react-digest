import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import AutocompleteMultiple from './AutocompleteMultiple';
import AutocompleteSingle from './AutocompleteSingle';

const Autocomplete: FunctionComponent = (): ReactElement => {
	return (
		<>
			<AutocompleteSingle />
			<br />
			<AutocompleteMultiple />
		</>
	);
};

export default Autocomplete;
