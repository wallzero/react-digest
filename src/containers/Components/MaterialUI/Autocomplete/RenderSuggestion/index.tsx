import MenuItem from '@mui/material/MenuItem';
import type {
	MenuItemProps
} from '@mui/material/MenuItem';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const RenderSuggestion: FunctionComponent<RenderSuggestionProps> = (
	{
		highlightedIndex,
		index,
		itemProps,
		selectedItem,
		suggestion
	}: RenderSuggestionProps
): ReactElement => {
	const isHighlighted = highlightedIndex === index;
	const isSelected = (selectedItem || '').includes(suggestion.label);

	return (
		<MenuItem
			key={suggestion.label}
			{...itemProps}
			component='div'
			selected={isHighlighted}
			style={{
				/* stylelint-disable */
				fontWeight: isSelected ?
					500 :
					400
			}}
		>
			{suggestion.label}
		</MenuItem>
	);
};

export type Suggestion = {
	label: string;
};

export type RenderSuggestionProps = {
	highlightedIndex: number | null;
	index: number;
	itemProps: MenuItemProps<'div', {button?: never}>;
	selectedItem: Suggestion['label'];
	suggestion: Suggestion;
};

export default RenderSuggestion;
