import type {
	Theme
} from '@mui/material';
import Paper from '@mui/material/Paper';
import type {
	SxProps
} from '@mui/system';
import Downshift from 'downshift';
import React from 'react';
import '../Autocomplete.css';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import RenderInput from '../RenderInput';
import RenderSuggestion from '../RenderSuggestion';
import getSuggestions from '../getSuggestions';

const styles = {
	paper: {
		left: 0,
		marginTop: 1,
		position: 'absolute',
		right: 0,
		zIndex: 1
	}
} as {[key: string]: SxProps<Theme>};

const AutocompleteSingle: FunctionComponent = (): ReactElement => {
	return (
		<Downshift>
			{
				(
					{
						getInputProps,
						getItemProps,
						getLabelProps,
						getMenuProps,
						highlightedIndex,
						inputValue,
						isOpen,
						selectedItem
					}
				): ReactElement => {
					const {
						onBlur,
						onFocus,
						...inputProperties
					} = getInputProps({
						placeholder: 'Search for a country (start with a)'
					});

					return (
						<div
							styleName='container'
						>
							<RenderInput
								InputLabelProps={
									getLabelProps({
										shrink: true
										// eslint-disable-next-line @typescript-eslint/no-explicit-any
									} as any)
								}
								InputProps={{
									onBlur,
									onFocus
								}}
								fullWidth
								inputProps={inputProperties}
								label='Country'
								variant='filled'
							/>
							<div
								{...getMenuProps()}
							>
								{
									isOpen ?
										// eslint-disable-next-line @typescript-eslint/no-extra-parens
										(
											<Paper
												square
												sx={styles.paper}
											>
												{
													getSuggestions(inputValue).map(
														(
															suggestion,
															index
														): ReactElement => {
															return (
																<RenderSuggestion
																	highlightedIndex={highlightedIndex}
																	index={index}
																	itemProps={
																		getItemProps({
																			item: suggestion.label
																		})
																	}
																	key={suggestion.label}
																	selectedItem={selectedItem}
																	suggestion={suggestion}
																/>
															);
														}
													)
												}
											</Paper>
										) :
										null
								}
							</div>
						</div>
					);
				}
			}
		</Downshift>
	);
};

export default AutocompleteSingle;

export type Suggestion = {
	[key: string]: unknown;
	label: string;
};
