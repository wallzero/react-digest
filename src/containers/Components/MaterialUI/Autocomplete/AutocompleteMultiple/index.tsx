import type {
	Theme
} from '@mui/material';
import Chip from '@mui/material/Chip';
import Paper from '@mui/material/Paper';
import type {
	SxProps
} from '@mui/system';
import Downshift from 'downshift';
import React, {
	useCallback,
	useState
} from 'react';
import type {
	ChangeEvent,
	FunctionComponent,
	KeyboardEvent,
	ReactElement
} from 'react';
import RenderInput from '../RenderInput';
import RenderSuggestion from '../RenderSuggestion';
import getSuggestions from '../getSuggestions';
import '../Autocomplete.css';

const styles = {
	chip: {
		mx: 0.25,
		my: 0.5
	},
	paper: {
		left: 0,
		marginTop: 1,
		position: 'absolute',
		right: 0,
		zIndex: 1
	}
} as {[key: string]: SxProps<Theme>};

const AutocompleteMultiple: FunctionComponent = (): ReactElement => {
	const [
		inputValue,
		setInputValue
	] = useState('');
	const [
		selectedItem,
		setSelectedItem
	] = useState<Array<Suggestion['label']>>([]);

	const handleKeyDown = (
		event: KeyboardEvent
	): void => {
		if (selectedItem.length && !inputValue.length && event.key === 'Backspace') {
			setSelectedItem(selectedItem.slice(
				0,
				selectedItem.length - 1
			));
		}
	};

	const handleInputChange = (
		event: ChangeEvent<{value: string}>
	): void => {
		setInputValue(event.target.value);
	};

	const handleChange = useCallback(
		(
			item: Suggestion['label']
		): void => {
			let newSelectedItem = [
				...selectedItem
			];
			if (!newSelectedItem.includes(item)) {
				newSelectedItem = [
					...newSelectedItem,
					item
				];
			}

			setInputValue('');
			setSelectedItem(newSelectedItem);
		},
		[
			selectedItem
		]
	);

	const handleDelete = (item: string): (() => void) => {
		return (): void => {
			const newSelectedItem = [
				...selectedItem
			];
			newSelectedItem.splice(
				newSelectedItem.indexOf(item),
				1
			);
			setSelectedItem(newSelectedItem);
		};
	};

	return (
		<Downshift
			inputValue={inputValue}
			onChange={handleChange}
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			selectedItem={selectedItem}
		>
			{
				({
					getInputProps,
					getItemProps,
					getLabelProps,
					isOpen,
					inputValue: inputValue2,
					selectedItem: selectedItem2,
					highlightedIndex
				}): ReactElement => {
					const {
						onBlur,
						onChange,
						onFocus,
						...inputProperties
					} = getInputProps({
						onKeyDown: handleKeyDown,
						placeholder: 'Select multiple countries'
					});

					return (
						<div
							styleName='container'
						>
							<RenderInput
								InputLabelProps={getLabelProps()}
								InputProps={{
									onBlur,
									onChange: (event: ChangeEvent<HTMLInputElement>): void => {
										handleInputChange(event);
										onChange(event as React.ChangeEvent<HTMLInputElement>);
									},
									onFocus,
									startAdornment: selectedItem.map(
										(item): ReactElement => {
											return (
												<Chip
													key={item}
													label={item}
													onDelete={handleDelete(item)}
													sx={styles.chip}
													tabIndex={-1}
												/>
											);
										}
									)
								}}
								fullWidth
								inputProps={inputProperties}
								label='Countries'
							/>
							{
								isOpen ?
									// eslint-disable-next-line @typescript-eslint/no-extra-parens
									(
										<Paper
											square
											sx={styles.paper}
										>
											{
												getSuggestions(inputValue2).map(
													(
														suggestion,
														index
													): ReactElement => {
														return (
															<RenderSuggestion
																highlightedIndex={highlightedIndex}
																index={index}
																itemProps={
																	getItemProps({
																		item: suggestion.label
																	})
																}
																key={suggestion.label}
																selectedItem={selectedItem2}
																suggestion={suggestion}
															/>
														);
													}
												)
											}
										</Paper>
									) :
									null
							}
						</div>
					);
				}
			}
		</Downshift>
	);
};

export default AutocompleteMultiple;

export type Suggestion = {
	[key: string]: unknown;
	label: string;
};
