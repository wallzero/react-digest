import type {
	Theme
} from '@mui/material';
import TextField from '@mui/material/TextField';
import type {
	TextFieldProps
} from '@mui/material/TextField';
import type {
	SxProps
} from '@mui/system';
import React, {
	useMemo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const styles = {
	textfield: {
		'& .MuiOutlinedInput-root': {
			flexWrap: 'wrap',
			paddingTop: 1
		},
		'& input': {
			flexGrow: 1,
			width: 'auto'
		}
	}
} as {[key: string]: SxProps<Theme>};

const RenderInput: FunctionComponent<RenderInputProperties> = (
	{
		InputProps,
		ref,
		...other
	}: RenderInputProperties
): ReactElement => {
	const inputProps = useMemo(
		() => {
			return {
				inputRef: ref,
				...InputProps
			};
		},
		[
			ref,
			InputProps
		]
	);

	return (
		<TextField
			InputProps={inputProps}
			{...other}
			sx={styles.textfield}
		/>
	);
};

export default RenderInput;

export type RenderInputProperties = TextFieldProps & {
	ref?: React.Ref<HTMLDivElement>;
};
