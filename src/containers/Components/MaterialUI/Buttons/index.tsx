import AddIcon from '@mui/icons-material/Add';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import Alarm from '@mui/icons-material/Alarm';
import CloudUpload from '@mui/icons-material/CloudUpload';
import Delete from '@mui/icons-material/Delete';
import Edit from '@mui/icons-material/Edit';
import KeyboardVoice from '@mui/icons-material/KeyboardVoice';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import Save from '@mui/icons-material/Save';
import Send from '@mui/icons-material/Send';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Fab from '@mui/material/Fab';
import IconButton from '@mui/material/IconButton';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Buttons.css';

const styles = {
	button: {
		margin: 1
	},
	leftIcon: {
		marginRight: 1
	},
	rightIcon: {
		marginLeft: 1
	}
};

const doSomething = (): void => {};

const Buttons: FunctionComponent = (): ReactElement => {
	return (
		<>
			<Button
				sx={styles.button}
			>
				Default
			</Button>
			<Button
				color='primary'
				sx={styles.button}
			>
				Primary
			</Button>
			<Button
				color='secondary'
				sx={styles.button}
			>
				Secondary
			</Button>
			<Button
				disabled
				sx={styles.button}
			>
				Disabled
			</Button>
			<Button
				href=''
				sx={styles.button}
			>
				Link
			</Button>
			<Button
				disabled
				href=''
				sx={styles.button}
			>
				Link disabled
			</Button>
			<Button
				data-something='here I am'
				onClick={doSomething}
				sx={styles.button}
			>
				Does something
			</Button>
			<Divider />
			<Button
				sx={styles.button}
				variant='contained'
			>
				Default
			</Button>
			<Button
				color='primary'
				sx={styles.button}
			>
				Primary
			</Button>
			<Button
				color='secondary'
				sx={styles.button}
				variant='contained'
			>
				Secondary
			</Button>
			<Button
				color='secondary'
				disabled
				sx={styles.button}
				variant='contained'
			>
				Disabled
			</Button>
			<input
				accept='image/*'
				id='raised-button-file'
				multiple
				styleName='input'
				type='file'
			/>
			<label htmlFor='raised-button-file'>
				<Button
					component='span'
					sx={styles.button}
					variant='contained'
				>
					Upload
				</Button>
			</label>
			<Button
				color='secondary'
				sx={styles.button}
				variant='contained'
			>
				Delete
				<Delete
					sx={styles.rightIcon}
				/>
			</Button>
			<Button
				color='primary'
				sx={styles.button}
				variant='contained'
			>
				Send
				<Send
					sx={styles.rightIcon}
				/>
			</Button>
			<Button
				sx={styles.button}
				variant='contained'
			>
				Upload
				<CloudUpload
					sx={styles.rightIcon}
				/>
			</Button>
			<Button
				color='secondary'
				disabled
				sx={styles.button}
				variant='contained'
			>
				<KeyboardVoice
					sx={styles.leftIcon}
				/>
				Talk
			</Button>
			<Button
				size='small'
				sx={styles.button}
				variant='contained'
			>
				<Save
					sx={styles.leftIcon}
				/>
				Save
			</Button>
			<Divider />
			<Fab
				aria-label='add'
				color='primary'
				sx={styles.button}
			>
				<AddIcon />
			</Fab>
			<Fab
				aria-label='edit'
				color='secondary'
				sx={styles.button}
			>
				<Edit />
			</Fab>
			<Fab
				aria-label='delete'
				disabled
				sx={styles.button}
			>
				<Delete />
			</Fab>
			<Divider />
			<IconButton
				aria-label='Delete'
				size='large'
				sx={styles.button}
			>
				<Delete />
			</IconButton>
			<IconButton
				aria-label='Delete'
				color='primary'
				disabled
				size='large'
				sx={styles.button}
			>
				<Delete />
			</IconButton>
			<IconButton
				aria-label='Add an alarm'
				color='secondary'
				size='large'
				sx={styles.button}
			>
				<Alarm />
			</IconButton>
			<IconButton
				aria-label='Add to shopping cart'
				color='primary'
				size='large'
				sx={styles.button}
			>
				<AddShoppingCartIcon />
			</IconButton>
			<input
				accept='image/*'
				id='icon-button-file'
				styleName='input'
				type='file'
			/>
			<label htmlFor='icon-button-file'>
				<IconButton
					aria-label='Upload photo'
					color='primary'
					component='span'
					size='large'
					sx={styles.button}
				>
					<PhotoCamera />
				</IconButton>
			</label>
		</>
	);
};

export default Buttons;
