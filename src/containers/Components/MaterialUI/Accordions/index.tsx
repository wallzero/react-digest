import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import type {
	Theme
} from '@mui/material';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import type {
	SxProps
} from '@mui/system';
import React, {
	useState
} from 'react';
import type {
	ChangeEvent,
	FunctionComponent,
	ReactElement
} from 'react';

const styles = {
	heading: {
		flexBasis: '33.33%',
		flexShrink: 0,
		fontSize: (theme) => {
			return theme.typography.pxToRem(15);
		}
	},
	secondaryHeading: {
		color: (theme) => {
			return theme.palette.text.secondary;
		},
		fontSize: (theme) => {
			return theme.typography.pxToRem(15);
		}
	}
} as {[key: string]: SxProps<Theme>};

const Accordions: FunctionComponent = (): ReactElement => {
	const [
		expanded,
		setExpanded
	] = useState<{
		[key: string]: boolean;
	}>({});

	const handleChange = (
		panel: string
	): (
		(
			event: ChangeEvent<unknown>,
			expand: boolean
		) => void
		) => {
		return (
			_event: ChangeEvent<unknown>,
			expand: boolean
		): void => {
			setExpanded({
				[panel]: expand
			});
		};
	};

	return (
		<>
			<Accordion
				expanded={expanded.panel1}
				onChange={handleChange('panel1')}
			>
				<AccordionSummary
					expandIcon={
						<ExpandMoreIcon />
					}
				>
					<Typography
						sx={styles.heading}
					>
						General settings
					</Typography>
					<Typography
						sx={styles.secondaryHeading}
					>
						I am an expansion panel
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<Typography>
						Nulla facilisi. Phasellus sollicitudin nulla et quam mattis feugiat. Aliquam eget
						maximus est, id dignissim quam.
					</Typography>
				</AccordionDetails>
			</Accordion>
			<Accordion
				expanded={expanded.panel2}
				onChange={handleChange('panel2')}
			>
				<AccordionSummary
					expandIcon={
						<ExpandMoreIcon />
					}
				>
					<Typography
						sx={styles.heading}
					>
						Users
					</Typography>
					<Typography
						sx={styles.secondaryHeading}
					>
						You are currently not an owner
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<Typography>
						Donec placerat, lectus sed mattis semper, neque lectus feugiat lectus, varius pulvinar
						diam eros in elit. Pellentesque convallis laoreet laoreet.
					</Typography>
				</AccordionDetails>
			</Accordion>
			<Accordion
				expanded={expanded.panel3}
				onChange={handleChange('panel3')}
			>
				<AccordionSummary
					expandIcon={
						<ExpandMoreIcon />
					}
				>
					<Typography
						sx={styles.heading}
					>
						Advanced settings
					</Typography>
					<Typography
						sx={styles.secondaryHeading}
					>
						Filtering has been entirely disabled for whole web server
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<Typography>
						Nunc vitae orci ultricies, auctor nunc in, volutpat nisl. Integer sit amet egestas
						eros, vitae egestas augue. Duis vel est augue.
					</Typography>
				</AccordionDetails>
			</Accordion>
		</>
	);
};

export default Accordions;
