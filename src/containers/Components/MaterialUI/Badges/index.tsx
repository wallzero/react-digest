import MailIcon from '@mui/icons-material/Mail';
import Badge from '@mui/material/Badge';
import IconButton from '@mui/material/IconButton';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const styles = {
	badge: {
		mx: 2,
		my: 0
	}
};

const Badges: FunctionComponent = (): ReactElement => {
	return (
		<>
			<Badge
				badgeContent={4}
				color='primary'
				sx={styles.badge}
			>
				<MailIcon />
			</Badge>
			<Badge
				badgeContent={10}
				color='secondary'
				sx={styles.badge}
			>
				<MailIcon />
			</Badge>
			<IconButton
				size='large'
				sx={styles.badge}
			>
				<Badge
					badgeContent={4}
					color='primary'
				>
					<MailIcon />
				</Badge>
			</IconButton>
		</>
	);
};

export default Badges;
