import DoneIcon from '@mui/icons-material/Done';
import FaceIcon from '@mui/icons-material/Face';
import Avatar from '@mui/material/Avatar';
import Chip from '@mui/material/Chip';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Chips.css';

const styles = {
	chip: {
		margin: 1
	}
};

const handleDelete = (): void => {
	alert('You clicked the delete icon.'); // eslint-disable-line no-alert
};

const handleClick = (): void => {
	alert('You clicked the Chip.'); // eslint-disable-line no-alert
};

const Chips: FunctionComponent = (): ReactElement => {
	return (
		<>
			<Chip
				label='Basic Chip'
				sx={styles.chip}
			/>
			<Chip
				avatar={(
					<Avatar>
						MB
					</Avatar>
				)}
				label='Clickable Chip'
				onClick={handleClick}
				sx={styles.chip}
			/>
			<Chip
				avatar={(
					<Avatar
						alt='Random placeholder image'
						src='https://placeimg.com/30/30/people'
					/>
				)}
				label='Deletable Chip'
				onDelete={handleDelete}
				sx={styles.chip}
			/>
			<Chip
				avatar={(
					<Avatar>
						<FaceIcon />
					</Avatar>
				)}
				label='Clickable Deletable Chip'
				onClick={handleClick}
				onDelete={handleDelete}
				sx={styles.chip}
			/>
			<Chip
				deleteIcon={
					<DoneIcon />
				}
				label='Custom delete icon Chip'
				onClick={handleClick}
				onDelete={handleDelete}
				sx={styles.chip}
			/>
		</>
	);
};

export default Chips;
