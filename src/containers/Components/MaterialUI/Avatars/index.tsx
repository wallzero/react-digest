import AssignmentIcon from '@mui/icons-material/Assignment';
import FolderIcon from '@mui/icons-material/Folder';
import PageviewIcon from '@mui/icons-material/Pageview';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Avatars.css';

const Avatars: FunctionComponent = (): ReactElement => {
	return (
		<>
			<div styleName='row'>
				<Avatar
					alt='Remy Sharp'
					src='https://placeimg.com/40/40/people'
					styleName='avatar'
				/>
				<Avatar
					alt='Adelle Charles'
					src='https://placeimg.com/60/60/people'
					styleName='avatar big-avatar'
				/>
			</div>
			<Divider />
			<div styleName='row'>
				<Avatar styleName='avatar'>
					<FolderIcon />
				</Avatar>
				<Avatar styleName='pink-avatar'>
					<PageviewIcon />
				</Avatar>
				<Avatar styleName='green-avatar'>
					<AssignmentIcon />
				</Avatar>
			</div>
			<Divider />
			<div styleName='row'>
				<Avatar styleName='avatar'>H</Avatar>
				<Avatar styleName='orange-avatar'>N</Avatar>
				<Avatar styleName='purple-avatar'>OP</Avatar>
			</div>
		</>
	);
};

export default Avatars;
