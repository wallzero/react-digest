import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import MasonryResize from 'src/components/MasonryResize';
import Accordions from './MaterialUI/Accordions';
import Autocomplete from './MaterialUI/Autocomplete';
import Avatars from './MaterialUI/Avatars';
import Badges from './MaterialUI/Badges';
import Buttons from './MaterialUI/Buttons';
import Chips from './MaterialUI/Chips';
import CustomButtons from './MaterialUI/CustomButtons';

const Components: FunctionComponent = (): ReactElement => {
	return (
		<MasonryResize>
			<Autocomplete key='Autocomplete' />
			<Avatars key='Avatars' />
			<Badges key='Badges' />
			<Buttons key='Buttons' />
			<Chips key='Chips' />
			<CustomButtons key='CustomButtons' />
			<Accordions key='Accordions' />
		</MasonryResize>
	);
};

export default Components;
