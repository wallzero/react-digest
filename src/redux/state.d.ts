
import {ReactStateDeclaration} from '@uirouter/react/lib/interface';
import {ReactText} from 'react';
import {
	DrawerProps,
	Orientation,
	TabProps
} from 'ui-router-react-digest';

declare module State {
	export type orientation = Orientation;
	export type drawers = DrawerProps[];

	export namespace drawerSettings {
		export type docked = boolean;
		export type drag = boolean;
		export type hover = boolean;
		export type index = number;
		export type open = boolean;
		export type width = ReactText;
	}

	export namespace miscellaneous {
		export type shortName = string;
		export type title = string;
	}

	export type tabOrder = ReactText[];
	export type tab = TabProps;
	export type tabs = {[key: string]: TabProps};

	export namespace tabSettings {
		export type index = number;
	}

	export interface All {
		diagrams: Diagrams;
		drawers: drawers;
		drawerSettings: DrawerSettings;
		edgeOrders: EdgeOrders;
		edges: DiagramEdges;
		edgeSources: DiagramEdgeSources;
		edgeTargets: DiagramEdgeTargets;
		miscellaneous: {
			shortName: miscellaneous.shortName;
			title: miscellaneous.title;
		};
		nodeOrders: NodeOrders;
		nodes: DiagramNodes;
		orientation: Orientation;
		selections: DiagramSelections;
		tabOrder: tabOrder;
		tabs: tabs;
		tabSettings: TabSettings;
	}
}

export default State;
