const generateEdgeSources = (
	edges: DiagramEdges
): DiagramEdgeSources => {
	// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
	return Object.keys(edges).reduce<DiagramEdgeSources>(
		(
			diagramSources,
			diagramId
		): DiagramEdgeSources => {
			// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
			diagramSources[diagramId] = Object.keys(edges[diagramId]).reduce<EdgeSources>(
				(
					sources,
					edgeId
				): EdgeSources => {
					const edge = edges[diagramId][edgeId];

					if (!sources[edge.source]) {
						sources[edge.source] = {};
					}

					sources[edge.source][edgeId] = edges[diagramId][edgeId].target;

					return sources;
				},
				{}
			);

			return diagramSources;
		},
		{}
	);
};

export default generateEdgeSources;
