const generateSelections = (
	diagrams: Diagrams
): DiagramSelections => {
	// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
	return Object.keys(diagrams).reduce<DiagramSelections>(
		(
			selections,
			diagramId
		): DiagramSelections => {
			selections[diagramId] = {};

			return selections;
		},
		{}
	);
};

export default generateSelections;
