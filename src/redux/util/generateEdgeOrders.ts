const generateEdgeOrders = (
	edges: DiagramEdges
): EdgeOrders => {
	// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
	return Object.keys(edges).reduce<EdgeOrders>(
		(
			edgeOrders,
			diagramId
		): EdgeOrders => {
			// eslint-disable-next-line unicorn/no-array-reduce
			edgeOrders[diagramId] = Object.keys(edges[diagramId]).reduce<EdgeOrder>(
				(
					order,
					edgeId
				): EdgeOrder => {
					order.push({
						id: edgeId,
						source: edges[diagramId][edgeId].source,
						target: edges[diagramId][edgeId].target
					});

					return order;
				},
				[]
			);

			return edgeOrders;
		},
		{}
	);
};

export default generateEdgeOrders;
