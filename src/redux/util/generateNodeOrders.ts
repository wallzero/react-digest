const generateNodeOrders = (
	nodes: DiagramNodes
): NodeOrders => {
	// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
	return Object.keys(nodes).reduce<NodeOrders>(
		(
			nodeOrder,
			diagramId
		): NodeOrders => {
			nodeOrder[diagramId] = Object.keys(nodes[diagramId]);

			return nodeOrder;
		},
		{}
	);
};

export default generateNodeOrders;
