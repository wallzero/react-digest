const generateEdgeTargets = (
	edges: DiagramEdges
): DiagramEdgeTargets => {
	// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
	return Object.keys(edges).reduce<DiagramEdgeTargets>(
		(
			diagramTargets,
			diagramId
		): DiagramEdgeTargets => {
			// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
			diagramTargets[diagramId] = Object.keys(edges[diagramId]).reduce<EdgeTargets>(
				(
					targets,
					edgeId
				): EdgeTargets => {
					const edge = edges[diagramId][edgeId];

					if (!targets[edge.target]) {
						targets[edge.target] = {};
					}

					targets[edge.target][edgeId] = edges[diagramId][edgeId].source;

					return targets;
				},
				{}
			);

			return diagramTargets;
		},
		{}
	);
};

export default generateEdgeTargets;
