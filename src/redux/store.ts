import {
	applyMiddleware,
	createStore
} from 'redux';
import {
	composeWithDevTools
} from 'redux-devtools-extension';
import defaultState from 'src/redux/defaultState';
import reducers from 'src/redux/reducers';
import type State from 'src/redux/state';
import {
	loadState,
	saveState
} from './localStorage';
import middleware from './middleware';

const composeEnhancers = composeWithDevTools({
	actionsBlacklist: [
		'NODES_MOVE_DELTA'
	]
});

const store = createStore(
	reducers,
	{
		...defaultState,
		...loadState()
	},
	composeEnhancers(
		applyMiddleware(...middleware)
	)
);

const subscribe = (): void => {
	saveState(store.getState() as State.All);
};

store.subscribe(subscribe);

export default store;
