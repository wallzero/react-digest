import type {
	ReactText
} from 'react';
import {
	v4 as uuidv4
} from 'uuid';
import {
	EDGE_ADD,
	EDGE_UPDATE,
	EDGES_REMOVE
} from './ActionTypes';

export type ActionEdgeAdd = {
	diagramId: ReactText;
	edge: Edge;
	edgeId?: ReactText;
	type: typeof EDGE_ADD;
};

export const onEdgeAdd = (
	diagramId: ReactText,
	edge: Edge,
	edgeId?: ReactText
): ActionEdgeAdd => {
	return {
		diagramId,
		edge,
		edgeId: edgeId || uuidv4(),
		type: EDGE_ADD
	};
};

export type ActionEdgesRemove = {
	diagramId: ReactText;
	edges: Edges;
	type: typeof EDGES_REMOVE;
};

export const onEdgesRemove = (
	diagramId: ReactText,
	edges: Edges
): ActionEdgesRemove => {
	return {
		diagramId,
		edges,
		type: EDGES_REMOVE
	};
};

export type ActionEdgeUpdate = {
	diagramId: ReactText;
	edge: Edge;
	edgeId: ReactText;
	type: typeof EDGE_UPDATE;
};

export const onEdgeUpdate = (
	diagramId: ReactText,
	edgeId: ReactText,
	edge: Edge
): ActionEdgeUpdate => {
	return {
		diagramId,
		edge,
		edgeId,
		type: EDGE_UPDATE
	};
};
