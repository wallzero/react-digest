import type {
	ReactText
} from 'react';
import {
	SELECTION_ADD,
	SELECTION_CLEAR,
	SELECTION_REMOVE,
	SELECTION_TOGGLE,
	SELECTION_UPDATE
} from './ActionTypes';

export type ActionSelectionAdd = {
	diagramId: ReactText;
	nodeId: ReactText;
	type: typeof SELECTION_ADD;
};

export const onSelectionAdd = (
	diagramId: ReactText,
	nodeId: ReactText
): ActionSelectionAdd => {
	return {
		diagramId,
		nodeId,
		type: SELECTION_ADD
	};
};

export type ActionSelectionClear = {
	diagramId: ReactText;
	type: typeof SELECTION_CLEAR;
};

export const onSelectionClear = (
	diagramId: ReactText
): ActionSelectionClear => {
	return {
		diagramId,
		type: SELECTION_CLEAR
	};
};

export type ActionSelectionRemove = {
	diagramId: ReactText;
	nodeId: ReactText;
	type: typeof SELECTION_REMOVE;
};

export const onSelectionRemove = (
	diagramId: ReactText,
	nodeId: ReactText
): ActionSelectionRemove => {
	return {
		diagramId,
		nodeId,
		type: SELECTION_REMOVE
	};
};

export type ActionSelectionToggle = {
	diagramId: ReactText;
	nodeId: ReactText;
	type: typeof SELECTION_TOGGLE;
};

export const onSelectionToggle = (
	diagramId: ReactText,
	nodeId: ReactText
): ActionSelectionToggle => {
	return {
		diagramId,
		nodeId,
		type: SELECTION_TOGGLE
	};
};

export type ActionSelectionUpdate = {
	diagramId: ReactText;
	selections: Selections;
	type: typeof SELECTION_UPDATE;
};

export const onSelectionUpdate = (
	diagramId: ReactText,
	selections: Selections
): ActionSelectionUpdate => {
	return {
		diagramId,
		selections,
		type: SELECTION_UPDATE
	};
};
