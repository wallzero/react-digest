import type {
	ReactText
} from 'react';
import {
	v4 as uuidv4
} from 'uuid';
import {
	DIAGRAM_ADD,
	DIAGRAM_REMOVE,
	DIAGRAM_UPDATE
} from './ActionTypes';

export type ActionDiagramAdd = {
	diagram?: Partial<Diagram>;
	diagramId: ReactText;
	type: typeof DIAGRAM_ADD;
};

export const onDiagramAdd = (
	diagram?: Partial<Diagram>,
	diagramId?: ReactText
): ActionDiagramAdd => {
	return {
		diagram,
		diagramId: diagramId || uuidv4(),
		type: DIAGRAM_ADD
	};
};

export type ActionDiagramRemove = {
	diagramId: ReactText;
	type: typeof DIAGRAM_REMOVE;
};

export const onDiagramRemove = (
	diagramId: ReactText
): ActionDiagramRemove => {
	return {
		diagramId,
		type: DIAGRAM_REMOVE
	};
};

export type ActionDiagramUpdate = {
	diagram: Partial<Diagram>;
	diagramId: ReactText;
	type: typeof DIAGRAM_UPDATE;
};

export const onDiagramUpdate = (
	diagramId: ReactText,
	diagram: Partial<Diagram>
): ActionDiagramUpdate => {
	return {
		diagram,
		diagramId,
		type: DIAGRAM_UPDATE
	};
};
