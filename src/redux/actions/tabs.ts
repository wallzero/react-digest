import type {
	ReactText
} from 'react';
import type {
	TabProps
} from 'ui-router-react-digest';
import {
	v4 as uuidv4
} from 'uuid';
import {
	TAB_ADD,
	TAB_REMOVE
} from './ActionTypes';

export type ActionTabAdd = {
	jump?: boolean;
	tab?: TabProps;
	tabIndex?: number;
	type: typeof TAB_ADD;
};

export const onTabAdd = (
	tab?: Omit<TabProps, 'name'> & {name?: string},
	tabIndex?: number,
	jump = true
): ActionTabAdd => {
	return {
		jump,
		tab: {
			...tab,
			name: tab?.name || uuidv4()
		},
		tabIndex,
		type: TAB_ADD
	};
};

export type ActionTabRemove = {
	name: ReactText;
	tabIndex: number;
	type: typeof TAB_REMOVE;
};

export const onTabRemove = (
	name: ReactText,
	tabIndex: number
): ActionTabRemove => {
	return {
		name,
		tabIndex,
		type: TAB_REMOVE
	};
};
