import {
	SELECTION_ADD,
	SELECTION_CLEAR,
	SELECTION_REMOVE,
	SELECTION_TOGGLE,
	SELECTION_UPDATE
} from '../ActionTypes';
import {
	onSelectionAdd,
	onSelectionClear,
	onSelectionRemove,
	onSelectionToggle,
	onSelectionUpdate
} from '../selections';
import type {
	ActionSelectionAdd,
	ActionSelectionClear,
	ActionSelectionRemove,
	ActionSelectionToggle,
	ActionSelectionUpdate
} from '../selections';

describe(
	'Selection actions',
	(): void => {
		describe(
			'add',
			(): void => {
				it(
					'Add node to diagram selection',
					(): void => {
						const diagramId = '2';
						const nodeId = '4';
						const expectedAction: ActionSelectionAdd = {
							diagramId,
							nodeId,
							type: SELECTION_ADD
						};

						expect(
							onSelectionAdd(
								diagramId,
								nodeId
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'clear',
			(): void => {
				it(
					'Clear diagram selection',
					(): void => {
						const diagramId = '2';
						const expectedAction: ActionSelectionClear = {
							diagramId,
							type: SELECTION_CLEAR
						};

						expect(
							onSelectionClear(
								diagramId
							)
						).toStrictEqual(expectedAction);
					}
				);

				it(
					'Clear entire diagram selection',
					(): void => {
						const diagramId = '2';
						const expectedAction: ActionSelectionClear = {
							diagramId,
							type: SELECTION_CLEAR
						};

						expect(
							onSelectionClear(
								diagramId
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'remove',
			(): void => {
				it(
					'Remove node from diagram selection',
					(): void => {
						const diagramId = '2';
						const nodeId = '4';
						const expectedAction: ActionSelectionRemove = {
							diagramId,
							nodeId,
							type: SELECTION_REMOVE
						};

						expect(
							onSelectionRemove(
								diagramId,
								nodeId
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'toggle',
			(): void => {
				it(
					'Toggle node in diagram selection',
					(): void => {
						const diagramId = '2';
						const nodeId = '4';
						const expectedAction: ActionSelectionToggle = {
							diagramId,
							nodeId,
							type: SELECTION_TOGGLE
						};

						expect(
							onSelectionToggle(
								diagramId,
								nodeId
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'update',
			(): void => {
				it(
					'Update diagram selection',
					(): void => {
						const diagramId = '2';
						const selections = {
							'4': true
						};
						const expectedAction: ActionSelectionUpdate = {
							diagramId,
							selections,
							type: SELECTION_UPDATE
						};

						expect(
							onSelectionUpdate(
								diagramId,
								selections
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);
	}
);
