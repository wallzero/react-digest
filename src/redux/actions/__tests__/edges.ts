import {
	EDGE_ADD,
	EDGE_UPDATE,
	EDGES_REMOVE
} from '../ActionTypes';
import {
	onEdgeAdd,
	onEdgeUpdate,
	onEdgesRemove
} from '../edges';
import type {
	ActionEdgeAdd,
	ActionEdgeUpdate,
	ActionEdgesRemove
} from '../edges';

describe(
	'Edge actions',
	(): void => {
		describe(
			'Add new edge',
			(): void => {
				it(
					'with id',
					(): void => {
						const diagramId = '2';
						const edge: Edge = {
							source: 1,
							target: 2
						};
						const edgeId = 3;
						const expectedAction: ActionEdgeAdd = {
							diagramId,
							edge,
							edgeId,
							type: EDGE_ADD
						};

						expect(
							onEdgeAdd(
								diagramId,
								edge,
								edgeId
							)
						).toStrictEqual(expectedAction);
					}
				);

				it(
					'without id (expect random id to be generated)',
					(): void => {
						const diagramId = '2';
						const edge: Edge = {
							source: 1,
							target: 2
						};

						expect(
							typeof onEdgeAdd(
								diagramId,
								edge
							).edgeId
						).toBe('string');
					}
				);
			}
		);

		describe(
			'Remove edges',
			(): void => {
				it(
					'with id',
					(): void => {
						const diagramId = '2';
						const edges: Edges = {
							'3': {
								source: 1,
								target: 2
							}
						};
						const expectedAction: ActionEdgesRemove = {
							diagramId,
							edges,
							type: EDGES_REMOVE
						};

						expect(
							onEdgesRemove(
								diagramId,
								edges
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'Update edge',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = '2';
						const edge: Edge = {
							source: 1,
							target: 2
						};
						const edgeId = 3;
						const expectedAction: ActionEdgeUpdate = {
							diagramId,
							edge,
							edgeId,
							type: EDGE_UPDATE
						};

						expect(
							onEdgeUpdate(
								diagramId,
								edgeId,
								edge
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);
	}
);
