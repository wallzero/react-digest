import {
	DIAGRAM_ADD,
	DIAGRAM_REMOVE,
	DIAGRAM_UPDATE
} from '../ActionTypes';
import {
	onDiagramAdd,
	onDiagramRemove,
	onDiagramUpdate
} from '../diagrams';
import type {
	ActionDiagramAdd,
	ActionDiagramRemove,
	ActionDiagramUpdate
} from '../diagrams';

describe(
	'Diagram actions',
	(): void => {
		describe(
			'add',
			(): void => {
				it(
					'Create new diagram with random name',
					(): void => {
						expect(typeof onDiagramAdd().diagramId).toBe('string');
					}
				);

				it(
					'Create new diagram with defined id and default properties',
					(): void => {
						const diagramId = '2';
						const expectedAction: ActionDiagramAdd = {
							diagram: undefined,
							diagramId,
							type: DIAGRAM_ADD
						};

						expect(
							onDiagramAdd(
								undefined,
								diagramId
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'remove',
			(): void => {
				it(
					'Remove diagram',
					(): void => {
						const diagramId = '2';
						const expectedAction: ActionDiagramRemove = {
							diagramId,
							type: DIAGRAM_REMOVE
						};

						expect(onDiagramRemove(diagramId)).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'update',
			(): void => {
				it(
					'Update diagram',
					(): void => {
						const diagramId = '2';
						const update: Partial<Diagram> = {
							maxScale: 3
						};
						const expectedAction: ActionDiagramUpdate = {
							diagram: update,
							diagramId,
							type: DIAGRAM_UPDATE
						};

						expect(
							onDiagramUpdate(
								diagramId,
								update
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);
	}
);
