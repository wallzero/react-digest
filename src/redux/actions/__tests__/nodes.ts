import {
	NODE_ADD,
	NODE_UPDATE,
	NODES_MOVE_DELTA,
	NODES_REMOVE,
	NODES_UPDATE
} from '../ActionTypes';
import {
	onNodeAdd,
	onNodeUpdate,
	onNodesMoveDelta,
	onNodesRemove,
	onNodesUpdate
} from '../nodes';
import type {
	ActionNodeAdd,
	ActionNodeUpdate,
	ActionNodesMoveDelta,
	ActionNodesRemove,
	ActionNodesUpdate
} from '../nodes';

describe(
	'Node actions',
	(): void => {
		describe(
			'Add new node',
			(): void => {
				it(
					'with id',
					(): void => {
						const diagramId = '2';
						const node: DNode = {};
						const nodeId = 3;
						const expectedAction: ActionNodeAdd = {
							diagramId,
							node,
							nodeId,
							type: NODE_ADD
						};

						expect(
							onNodeAdd(
								diagramId,
								node,
								nodeId
							)
						).toStrictEqual(expectedAction);
					}
				);

				it(
					'without id (expect random id to be generated)',
					(): void => {
						const diagramId = '2';
						const node: DNode = {};

						expect(
							typeof onNodeAdd(
								diagramId,
								node
							).nodeId
						).toBe('string');
					}
				);
			}
		);

		describe(
			'Remove nodes',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = '2';
						const nodes = [
							3
						];
						const expectedAction: ActionNodesRemove = {
							diagramId,
							nodes,
							type: NODES_REMOVE
						};

						expect(
							onNodesRemove(
								diagramId,
								nodes
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'Move nodes',
			(): void => {
				it(
					'given delta',
					(): void => {
						const diagramId = '2';
						const nodes = {
							'3': {
								x: 1,
								y: 1
							}
						};
						const expectedAction: ActionNodesMoveDelta = {
							diagramId,
							nodes,
							type: NODES_MOVE_DELTA
						};

						expect(
							onNodesMoveDelta(
								diagramId,
								nodes
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'Update nodes',
			(): void => {
				it(
					'single',
					(): void => {
						const diagramId = '2';
						const node: DNode = {};
						const nodeId = 3;
						const expectedAction: ActionNodeUpdate = {
							diagramId,
							node,
							nodeId,
							type: NODE_UPDATE
						};

						expect(
							onNodeUpdate(
								diagramId,
								nodeId,
								node
							)
						).toStrictEqual(expectedAction);
					}
				);

				it(
					'many',
					(): void => {
						const diagramId = '2';
						const nodes: Nodes = {
							'1': {},
							'2': {}
						};
						const expectedAction: ActionNodesUpdate = {
							diagramId,
							nodes,
							type: NODES_UPDATE
						};

						expect(
							onNodesUpdate(
								diagramId,
								nodes
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);
	}
);
