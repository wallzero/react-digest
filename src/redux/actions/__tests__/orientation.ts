import {
	ORIENTATION
} from '../ActionTypes';
import {
	onOrientationToggle
} from '../orientation';
import type {
	ActionOrientation
} from '../orientation';

describe(
	'orientation actions',
	(): void => {
		it(
			'Select left position',
			(): void => {
				const position = 'left';
				const expectedAction: ActionOrientation = {
					position,
					type: ORIENTATION
				};

				expect(onOrientationToggle(position)).toStrictEqual(expectedAction);
			}
		);
	}
);
