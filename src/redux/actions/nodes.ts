import type {
	ReactText
} from 'react';
import {
	v4 as uuidv4
} from 'uuid';
import {
	NODE_ADD,
	NODE_UPDATE,
	NODES_MOVE_DELTA,
	NODES_REMOVE,
	NODES_UPDATE
} from './ActionTypes';

export type ActionNodeAdd = {
	diagramId: ReactText;
	node: DNode;
	nodeId: ReactText;
	type: typeof NODE_ADD;
};

export const onNodeAdd = (
	diagramId: ReactText,
	node: DNode,
	nodeId?: ReactText
): ActionNodeAdd => {
	return {
		diagramId,
		node,
		nodeId: nodeId || uuidv4(),
		type: NODE_ADD
	};
};

export type ActionNodesMoveDelta = {
	diagramId: ReactText;
	nodes: Nodes;
	type: typeof NODES_MOVE_DELTA;
};

export const onNodesMoveDelta = (
	diagramId: ReactText,
	nodes: Nodes
): ActionNodesMoveDelta => {
	return {
		diagramId,
		nodes,
		type: NODES_MOVE_DELTA
	};
};

export type ActionNodesRemove = {
	diagramId: ReactText;
	nodes: ReactText[];
	type: typeof NODES_REMOVE;
};

export const onNodesRemove = (
	diagramId: ReactText,
	nodes: ReactText[]
): ActionNodesRemove => {
	return {
		diagramId,
		nodes,
		type: NODES_REMOVE
	};
};

export type ActionNodesUpdate = {
	diagramId: ReactText;
	nodes: Nodes;
	type: typeof NODES_UPDATE;
};

export const onNodesUpdate = (
	diagramId: ReactText,
	nodes: Nodes
): ActionNodesUpdate => {
	return {
		diagramId,
		nodes,
		type: NODES_UPDATE
	};
};

export type ActionNodeUpdate = {
	diagramId: ReactText;
	node: DNode;
	nodeId: ReactText;
	type: typeof NODE_UPDATE;
};

export const onNodeUpdate = (
	diagramId: ReactText,
	nodeId: ReactText,
	node: DNode
): ActionNodeUpdate => {
	return {
		diagramId,
		node,
		nodeId,
		type: NODE_UPDATE
	};
};
