import type {
	ReactText
} from 'react';
import {
	DRAWER_DOCK,
	DRAWER_DRAG,
	DRAWER_HOVER,
	DRAWER_SELECT,
	DRAWER_OPEN,
	DRAWER_WIDTH
} from './ActionTypes';

export type ActionDrawerDock = {
	docked?: boolean;
	type: typeof DRAWER_DOCK;
};

export const onDrawerDockedToggle = (docked?: boolean): ActionDrawerDock => {
	return {
		docked,
		type: DRAWER_DOCK
	};
};

export type ActionDrawerDrag = {
	drag?: boolean;
	type: typeof DRAWER_DRAG;
};

export const onDrawerDragToggle = (drag?: boolean): ActionDrawerDrag => {
	return {
		drag,
		type: DRAWER_DRAG
	};
};

export type ActionDrawerHover = {
	hover?: boolean;
	type: typeof DRAWER_HOVER;
};

export const onDrawerHoverToggle = (hover?: boolean): ActionDrawerHover => {
	return {
		hover,
		type: DRAWER_HOVER
	};
};

export type ActionDrawerSelect = {
	drawerIndex: number;
	type: typeof DRAWER_SELECT;
};

export const onDrawerSelect = (drawerIndex: number): ActionDrawerSelect => {
	return {
		drawerIndex,
		type: DRAWER_SELECT
	};
};

export type ActionDrawerOpen = {
	open?: boolean;
	type: typeof DRAWER_OPEN;
};

export const onDrawerOpenToggle = (open?: boolean): ActionDrawerOpen => {
	return {
		open,
		type: DRAWER_OPEN
	};
};

export type ActionDrawerWidth = {
	type: typeof DRAWER_WIDTH;
	width: ReactText;
};

export const onDrawerSetWidth = (width: ReactText): ActionDrawerWidth => {
	return {
		type: DRAWER_WIDTH,
		width
	};
};
