import generateSelections from '../util/generateSelections';
import diagrams from './diagrams';

export default generateSelections(diagrams);
