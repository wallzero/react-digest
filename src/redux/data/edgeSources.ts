import generateSources from '../util/generateEdgeSources';
import edges from './edges';

export default generateSources(edges);
