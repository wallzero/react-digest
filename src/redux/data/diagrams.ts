const diagrams: {[key: string]: Partial<Diagram>} = {
	diagram: {
		maxScale: 2,
		minScale: 0.5,
		scale: 1,
		xOffset: 0,
		yOffset: 0
	}
};

export default diagrams as Diagrams;
