import generateNodeOrders from '../util/generateNodeOrders';
import nodes from './nodes';

export default generateNodeOrders(
	nodes
);
