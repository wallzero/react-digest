import generateEdgeOrders from '../util/generateEdgeOrders';
import edges from './edges';

export default generateEdgeOrders(
	edges
);
