/* eslint-disable quote-props */
const edges: {
	[key: string]: {
		[key: string]: Omit<Edge, 'id'>;
	};
} = {
	diagram: {
		'2': {
			source: '5',
			target: '2'
		},
		'2a': {
			source: '67b70b83-d964-43f3-a1a0-f2bacb53c1bc',
			target: '2'
		},
		'2b': {
			source: '741a4a95-fe28-4266-a2ed-c799732499c8',
			target: '2'
		},
		'2c': {
			source: '8c57a51e-f2af-4f1e-9181-59d948ba2de3',
			target: '2'
		},
		'2d': {
			source: 'a630e804-18a0-4dba-81cd-6cf34e4354ee',
			target: '2'
		},
		'2dc1dc76-4483-4c80-a04b-b26e2847053c': {
			source: '45',
			target: '2dc1dc76-4483-4c80-a04b-b26e2847053c'
		},
		'2e': {
			source: 'e55b2602-f988-4e4c-9339-f231ac70705f',
			target: '2'
		},
		'2f': {
			source: 'f2d2a5b0-3392-4d27-9b0a-f4130af8d9f9',
			target: '2'
		},
		'3': {
			source: 'c9dd4f48-f49c-44fb-b723-3ce8141f4da4',
			target: '3'
		},
		'4': {
			source: '1',
			target: '4'
		},
		'5': {
			source: '1',
			target: '5'
		},
		'8c57a51e-f2af-4f1e-9181-59d948ba2de3': {
			source: 'ecc7ec1b-1a6f-46fd-a485-7268bae1bb72',
			target: '8c57a51e-f2af-4f1e-9181-59d948ba2de3'
		},
		'35': {
			source: '1',
			target: '35'
		},
		'45': {
			source: '1',
			target: '45'
		},
		'67b70b83-d964-43f3-a1a0-f2bacb53c1bc': {
			source: 'ecc7ec1b-1a6f-46fd-a485-7268bae1bb72',
			target: '67b70b83-d964-43f3-a1a0-f2bacb53c1bc'
		},
		'741a4a95-fe28-4266-a2ed-c799732499c8': {
			source: '35',
			target: '741a4a95-fe28-4266-a2ed-c799732499c8'
		},
		'a630e804-18a0-4dba-81cd-6cf34e4354ee': {
			source: '1',
			target: 'a630e804-18a0-4dba-81cd-6cf34e4354ee'
		},
		'c9dd4f48-f49c-44fb-b723-3ce8141f4da4': {
			source: '1',
			target: 'c9dd4f48-f49c-44fb-b723-3ce8141f4da4'
		},
		'e55b2602-f988-4e4c-9339-f231ac70705f': {
			source: '4',
			target: 'e55b2602-f988-4e4c-9339-f231ac70705f'
		},
		'ecc7ec1b-1a6f-46fd-a485-7268bae1bb72': {
			source: '3',
			target: 'ecc7ec1b-1a6f-46fd-a485-7268bae1bb72'
		},
		'f2d2a5b0-3392-4d27-9b0a-f4130af8d9f9': {
			source: '2dc1dc76-4483-4c80-a04b-b26e2847053c',
			target: 'f2d2a5b0-3392-4d27-9b0a-f4130af8d9f9'
		}
	}
};

export default edges as DiagramEdges;
