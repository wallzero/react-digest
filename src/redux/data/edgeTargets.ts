import generateTargets from '../util/generateEdgeTargets';
import edges from './edges';

export default generateTargets(edges);
