/*
	eslint-disable
	quote-props
*/
const nodes: {
	[key: string]: {
		[key: string]: Omit<DNode, 'id'>;
	};
} = {
	diagram: {
		'1': {
			label: 'Source Node',
			x: 52.5,
			y: 223
		},
		'2': {
			label: 'Sink Node',
			x: 1_277.5,
			y: 161
		},
		'2dc1dc76-4483-4c80-a04b-b26e2847053c': {
			label: 'transform81089',
			x: 567.5,
			y: 33
		},
		'3': {
			label: 'Transform Node 3',
			x: 362.5,
			y: 375
		},
		'4': {
			label: 'Transform Node 4',
			x: 667.5,
			y: 109
		},
		'5': {
			label: 'Transform Node 5',
			x: 872.5,
			y: 185
		},
		'8c57a51e-f2af-4f1e-9181-59d948ba2de3': {
			label: 'transform55724',
			x: 872.5,
			y: 337
		},
		'35': {
			label: 'Transform Node 3.5',
			x: 167.5,
			y: 489
		},
		'45': {
			label: 'Transform Node 4.5',
			x: 362.5,
			y: 33
		},
		'67b70b83-d964-43f3-a1a0-f2bacb53c1bc': {
			label: 'transform22356',
			x: 872.5,
			y: 413
		},
		'741a4a95-fe28-4266-a2ed-c799732499c8': {
			label: 'transform47432',
			x: 872.5,
			y: 489
		},
		'a630e804-18a0-4dba-81cd-6cf34e4354ee': {
			label: 'transform22804',
			x: 872.5,
			y: 261
		},
		'c9dd4f48-f49c-44fb-b723-3ce8141f4da4': {
			label: 'transform22620',
			x: 157.5,
			y: 375
		},
		'e55b2602-f988-4e4c-9339-f231ac70705f': {
			label: 'transform81400',
			x: 872.5,
			y: 109
		},
		'ecc7ec1b-1a6f-46fd-a485-7268bae1bb72': {
			label: 'transform55875',
			x: 567.5,
			y: 375
		},
		'f2d2a5b0-3392-4d27-9b0a-f4130af8d9f9': {
			label: 'transform81249',
			x: 872.5,
			y: 33
		}
	}
};

export default nodes as DiagramNodes;
