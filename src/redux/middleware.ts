import type {
	Middleware
} from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';

const middleware: Middleware[] = [];

if (process.env.NODE_ENV !== 'production') {
	middleware.unshift(reduxImmutableStateInvariant());
}

export default middleware;
