import type State from './state';
import generateSources from './util/generateEdgeSources';
import generateTargets from './util/generateEdgeTargets';

declare const process: {
	env: {
		BUILD: {
			DATE: string;
		};
	};
};

type LoadedState = State.All & {
	BUILD_DATE: string;
};

export const loadState = (): Partial<State.All> | null => {
	try {
		const serializedState = localStorage.getItem('state');
		if (serializedState === null) {
			return null;
		}

		const state = JSON.parse(serializedState) as LoadedState;

		if (
			process.env.BUILD &&
			process.env.BUILD.DATE === state.BUILD_DATE
		) {
			return {
				diagrams: state.diagrams,
				drawerSettings: state.drawerSettings,
				edgeOrders: state.edgeOrders,
				edges: state.edges,
				edgeSources: state.edges ?
					generateSources(state.edges) :
					undefined,
				edgeTargets: state.edges ?
					generateTargets(state.edges) :
					undefined,
				nodeOrders: state.nodeOrders,
				nodes: state.nodes,
				orientation: state.orientation,
				tabOrder: state.tabOrder,
				tabs: state.tabs,
				tabSettings: state.tabSettings
			};
		}

		return null;
	} catch (error) {
		// eslint-disable-next-line no-console
		console.error(error);

		return null;
	}
};

export const saveState = (state: State.All): void => {
	try {
		localStorage.setItem(
			'state',
			JSON.stringify({
				BUILD_DATE: process.env.BUILD?.DATE,
				diagrams: state.diagrams,
				drawerSettings: state.drawerSettings,
				edgeOrders: state.edgeOrders,
				edges: state.edges,
				nodeOrders: state.nodeOrders,
				nodes: state.nodes,
				orientation: state.orientation,
				tabOrder: state.tabOrder,
				tabs: state.tabs,
				tabSettings: state.tabSettings
			})
		);
	} catch (error) {
		// eslint-disable-next-line no-console
		console.error(error);
	}
};
