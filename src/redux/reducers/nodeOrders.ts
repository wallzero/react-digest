import {
	DIAGRAM_REMOVE,
	NODE_ADD,
	NODE_UPDATE,
	NODES_REMOVE
} from '../actions/ActionTypes';
import type {
	ActionDiagramRemove
} from '../actions/diagrams';
import type {
	ActionNodeAdd,
	ActionNodesRemove,
	ActionNodeUpdate
} from '../actions/nodes';
import defaultState from '../defaultState';

export default function nodeOrders (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.nodeOrders,
	action: ActionDiagramRemove |
	ActionNodeAdd |
	ActionNodesRemove |
	ActionNodeUpdate
): NodeOrders {
	switch (action.type) {
		case DIAGRAM_REMOVE: {
			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[action.diagramId]: omit,
				...remaining
			} = state;

			return remaining;
		}

		case NODE_ADD: {
			const {
				diagramId
			} = action;

			return {
				...state,
				[diagramId]: [
					...state[diagramId],
					action.nodeId
				]
			};
		}

		case NODES_REMOVE: {
			const {
				diagramId
			} = action;

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: action.nodes.reduce<NodeOrder>(
					(
						nodes,
						nodeId
					): NodeOrder => {
						const index = nodes.indexOf(nodeId);

						return [
							...nodes.slice(
								0,
								index
							),
							...nodes.slice(index + 1)
						];
					},
					state[diagramId]
				)
			};
		}

		case NODE_UPDATE: {
			const {
				diagramId
			} = action;
			const nodes = state[diagramId];
			const index = nodes.indexOf(action.nodeId);

			if (index === nodes.length - 1) {
				return state;
			}

			return {
				...state,
				[diagramId]: [
					...nodes.slice(
						0,
						index
					),
					...nodes.slice(index + 1),
					action.nodeId
				]
			};
		}

		default: {
			return state;
		}
	}
}
