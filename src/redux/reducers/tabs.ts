import {
	TAB_ADD,
	TAB_REMOVE
} from '../actions/ActionTypes';
import type {
	ActionTabAdd,
	ActionTabRemove
} from '../actions/tabs';
import defaultState from '../defaultState';
import type State from '../state';

const tabDefaults = {
	state: {
		deepStateRedirect: true,
		params: {
			title: 'New Tab',
			type: 'dynamic'
		},
		sticky: true
	}
};

export default (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.tabs,
	action: ActionTabAdd |
	ActionTabRemove
): State.tabs => {
	switch (action.type) {
		case TAB_ADD: {
			return {
				...state,
				[action.tab.name]: {
					...tabDefaults,
					...action.tab,
					state: {
						...tabDefaults.state,
						...action.tab.state,

						params: {
							...tabDefaults.state.params,
							...action.tab.state ?
								action.tab.state.params :
								{}
						}
					}
				}
			};
		}

		case TAB_REMOVE: {
			if (state[action.name]) {
				const {
					// eslint-disable-next-line @typescript-eslint/no-unused-vars
					[action.name]: omit,
					...remaining
				} = state;

				return remaining;
			}

			return state;
		}

		default: {
			return state;
		}
	}
};
