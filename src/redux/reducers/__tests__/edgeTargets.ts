import type {
	ReactText
} from 'react';
import {
	DIAGRAM_REMOVE,
	EDGE_ADD,
	EDGES_REMOVE,
	NODES_REMOVE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../edgeTargets';

describe(
	'Edge targets reducer',
	(): void => {
		describe(
			'Add edge',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = 'newedge';
						const edge: Edge = {
							source: 6,
							target: 6
						};

						expect(
							reducer(
								defaultState.edgeTargets,
								{
									diagramId,
									edge,
									edgeId,
									type: EDGE_ADD
								}
							)
						).toStrictEqual({
							...defaultState.edgeTargets,
							[diagramId]: {
								...defaultState.edgeTargets[diagramId],
								[edge.target]: {
									[edgeId]: edge.source
								}
							}
						});
					}
				);
			}
		);
		describe(
			'Remove edge',
			(): void => {
				it(
					'manually',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = '4';
						const targetId = '2';
						const edges: Edges = {
							[edgeId]: {
								source: 1,
								target: targetId
							}
						};

						const {
							// eslint-disable-next-line @typescript-eslint/no-unused-vars
							[edgeId]: omit,
							...remaining
						} = defaultState.edgeTargets[diagramId][targetId];

						expect(
							reducer(
								defaultState.edgeTargets,
								{
									diagramId,
									edges,
									type: EDGES_REMOVE
								}
							)
						).toStrictEqual({
							...defaultState.edgeTargets,
							[diagramId]: {
								...defaultState.edgeTargets[diagramId],
								[targetId]: remaining
							}
						});
					}
				);

				it(
					'via node removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeTargets = defaultState.edgeTargets[diagramId];
						const targetId = '2';
						const nodes: ReactText[] = [
							targetId
						];

						expect(edgeTargets[targetId]).toBeDefined();

						const remaining = reducer(
							defaultState.edges,
							{
								diagramId,
								nodes,
								type: NODES_REMOVE
							}
						)[diagramId];

						expect(remaining[targetId]).toBeUndefined();
					}
				);

				it(
					'via diagram removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];

						const expected = {
							...defaultState.edgeTargets
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[diagramId];

						expect(
							reducer(
								defaultState.edgeTargets,
								{
									diagramId,
									type: DIAGRAM_REMOVE
								}
							)
						).toStrictEqual(expected);
					}
				);
			}
		);

		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.edgeTargets);
			}
		);
	}
);
