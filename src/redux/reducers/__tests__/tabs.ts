import {
	TAB_ADD,
	TAB_REMOVE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../tabs';

describe(
	'Tab properties',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.tabs);
			}
		);

		it(
			'adding a tab',
			(): void => {
				const tab = {
					name: 'new tab'
				};
				const complexTab = {
					name: 'new tab',
					state: {
						sticky: false
					}
				};
				const tabIndex = 1;

				expect(
					reducer(
						defaultState.tabs,
						{
							tab,
							type: TAB_ADD
						}
					)[tab.name]
				).toBeDefined();

				expect(
					reducer(
						defaultState.tabs,
						{
							tab: complexTab,
							type: TAB_ADD
						}
					)[tab.name].state.sticky
				).toStrictEqual(complexTab.state.sticky);

				expect(
					reducer(
						defaultState.tabs,
						{
							tab,
							tabIndex,
							type: TAB_ADD
						}
					)[tab.name]
				).toBeDefined();
			}
		);

		it(
			'removing a tab',
			(): void => {
				const name = Object.keys(defaultState.tabs)[1];
				const tabIndex = 2;

				expect(
					reducer(
						defaultState.tabs,
						{
							name,
							tabIndex,
							type: TAB_REMOVE
						}
					)[name]
				).toBeUndefined();
			}
		);

		it(
			'removing a tab that doesn\'t exist',
			(): void => {
				const name = 'awdasdasdawdawd';
				const tabIndex = 2;

				expect(
					reducer(
						defaultState.tabs,
						{
							name,
							tabIndex,
							type: TAB_REMOVE
						}
					)
				).toStrictEqual(defaultState.tabs);
			}
		);
	}
);
