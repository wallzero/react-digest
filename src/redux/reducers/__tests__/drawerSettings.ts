import {
	DRAWER_DOCK,
	DRAWER_DRAG,
	DRAWER_HOVER,
	DRAWER_OPEN,
	DRAWER_SELECT
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../drawerSettings';

describe(
	'Drawer settings reducer',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.drawerSettings);
			}
		);

		it(
			'handle dock toggle',
			(): void => {
				expect(
					reducer(
						defaultState.drawerSettings,
						{
							type: DRAWER_DOCK
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					docked: !defaultState.drawerSettings.docked
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							docked: true,
							type: DRAWER_DOCK
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					docked: true
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							docked: false,
							type: DRAWER_DOCK
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					docked: false
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							docked: 'awdawd',
							type: DRAWER_DOCK
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					docked: true
				});
			}
		);

		it(
			'handle drag toggle',
			(): void => {
				expect(
					reducer(
						defaultState.drawerSettings,
						{
							type: DRAWER_DRAG
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					drag: !defaultState.drawerSettings.drag
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							drag: true,
							type: DRAWER_DRAG
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					drag: true
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							drag: false,
							type: DRAWER_DRAG
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					drag: false
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							drag: 'awdawd',
							type: DRAWER_DRAG
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					drag: true
				});
			}
		);

		it(
			'handle hover toggle',
			(): void => {
				expect(
					reducer(
						defaultState.drawerSettings,
						{
							type: DRAWER_HOVER
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					hover: !defaultState.drawerSettings.hover
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							hover: true,
							type: DRAWER_HOVER
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					hover: true
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							hover: false,
							type: DRAWER_HOVER
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					hover: false
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							hover: 'awdawd',
							type: DRAWER_HOVER
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					hover: true
				});
			}
		);

		it(
			'handle open toggle',
			(): void => {
				expect(
					reducer(
						defaultState.drawerSettings,
						{
							type: DRAWER_OPEN
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					open: !defaultState.drawerSettings.open
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							open: true,
							type: DRAWER_OPEN
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					open: true
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							open: false,
							type: DRAWER_OPEN
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					open: false
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							open: 'awdawd',
							type: DRAWER_OPEN
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					open: true
				});
			}
		);

		it(
			'handle select',
			(): void => {
				const drawerIndex = 2;

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							drawerIndex,
							type: DRAWER_SELECT
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					index: drawerIndex
				});

				expect(
					reducer(
						defaultState.drawerSettings,

						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-expect-error
						{
							type: DRAWER_SELECT
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					index: defaultState.drawerSettings.index
				});

				expect(
					reducer(
						defaultState.drawerSettings,
						{
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							index: 'test',
							type: DRAWER_SELECT
						}
					)
				).toStrictEqual({
					...defaultState.drawerSettings,
					index: defaultState.drawerSettings.index
				});
			}
		);
	}
);
