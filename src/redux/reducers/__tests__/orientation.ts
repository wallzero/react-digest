import {
	ORIENTATION
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../orientation';

describe(
	'App orientation',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.orientation);
			}
		);

		it(
			'handle position toggle',
			(): void => {
				expect(
					reducer(
						'left',
						{
							type: ORIENTATION
						}
					)
				).toBe('right');

				expect(
					reducer(
						'right',
						{
							type: ORIENTATION
						}
					)
				).toBe('left');

				expect(
					reducer(
						defaultState.orientation,
						{
							position: defaultState.orientation,
							type: ORIENTATION
						}
					)
				).toStrictEqual(defaultState.orientation);

				expect(
					reducer(
						defaultState.orientation,
						{
							position: 'left',
							type: ORIENTATION
						}
					)
				).toBe('left');

				expect(
					reducer(
						defaultState.orientation,
						{
							position: 'right',
							type: ORIENTATION
						}
					)
				).toBe('right');

				expect(
					reducer(
						defaultState.orientation,
						{
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							position: 'test',
							type: ORIENTATION
						}
					)
				).toStrictEqual(defaultState.orientation);
			}
		);
	}
);
