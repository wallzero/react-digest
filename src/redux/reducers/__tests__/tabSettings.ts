import {
	TAB_ADD,
	TAB_REMOVE,
	TAB_SELECT
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../tabSettings';

describe(
	'Tab settings',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.tabSettings);
			}
		);

		it(
			'adding a tab',
			(): void => {
				const tabIndex = 1;

				expect(
					reducer(
						defaultState.tabSettings,
						{
							tabIndex,
							type: TAB_ADD
						}
					)
				).toStrictEqual(defaultState.tabSettings);

				expect(
					reducer(
						defaultState.tabSettings,
						{
							jump: true,
							tabIndex,
							type: TAB_ADD
						}
					).index
				).toStrictEqual(tabIndex);

				expect(
					reducer(
						defaultState.tabSettings,
						{
							type: TAB_ADD
						}
					)
				).toStrictEqual(defaultState.tabSettings);
			}
		);

		it(
			'removing a tab',
			(): void => {
				expect(
					reducer(
						defaultState.tabSettings,
						{
							name: 't',
							tabIndex: 1,
							type: TAB_REMOVE
						}
					)
				).toStrictEqual(defaultState.tabSettings);

				const settings = {
					...defaultState.tabSettings,
					index: 2
				};

				expect(
					reducer(
						settings,
						{
							name: 't',
							tabIndex: 2,
							type: TAB_REMOVE
						}
					).index
				).toStrictEqual(settings.index - 1);
			}
		);

		it(
			'selecting a tab',
			(): void => {
				const tabIndex = 1;

				expect(
					reducer(
						defaultState.tabSettings,
						{
							tabIndex,
							type: TAB_SELECT
						}
					).index
				).toStrictEqual(tabIndex);

				expect(
					reducer(
						defaultState.tabSettings,
						{
							tabIndex: 0,
							type: TAB_SELECT
						}
					).index
				).toBe(0);
			}
		);
	}
);
