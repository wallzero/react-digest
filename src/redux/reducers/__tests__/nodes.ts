import {
	DIAGRAM_REMOVE,
	NODE_ADD,
	NODE_UPDATE,
	NODES_MOVE_DELTA,
	NODES_REMOVE,
	NODES_UPDATE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../nodes';

describe(
	'Nodes reducer',
	(): void => {
		describe(
			'Add node',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = 'newnode';
						const node: DNode = {
							x: 0,
							y: 0
						};

						expect(
							reducer(
								defaultState.nodes,
								{
									diagramId,
									node,
									nodeId,
									type: NODE_ADD
								}
							)
						).toStrictEqual({
							...defaultState.nodes,
							[diagramId]: {
								...defaultState.nodes[diagramId],
								[nodeId]: node
							}
						});
					}
				);
			}
		);

		describe(
			'Remove node',
			(): void => {
				it(
					'manually',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const nodes = [
							nodeId
						];

						const expected = {
							...defaultState.nodes[diagramId]
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[nodeId];

						expect(
							reducer(
								defaultState.nodes,
								{
									diagramId,
									nodes,
									type: NODES_REMOVE
								}
							)
						).toStrictEqual({
							...defaultState.nodes,
							[diagramId]: expected
						});
					}
				);

				it(
					'via diagram removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];

						const expected = {
							...defaultState.nodes
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[diagramId];

						expect(
							reducer(
								defaultState.nodes,
								{
									diagramId,
									type: DIAGRAM_REMOVE
								}
							)
						).toStrictEqual(expected);
					}
				);
			}
		);

		describe(
			'Update nodes',
			(): void => {
				it(
					'single update',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const node: DNode = {
							label: 'updated'
						};

						expect(
							reducer(
								defaultState.nodes,
								{
									diagramId,
									node,
									nodeId,
									type: NODE_UPDATE
								}
							)
						).toStrictEqual({
							...defaultState.nodes,
							[diagramId]: {
								...defaultState.nodes[diagramId],
								[nodeId]: {
									...defaultState.nodes[diagramId][nodeId],
									...node
								}
							}
						});
					}
				);

				it(
					'multiple update',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const nodes: Nodes = {
							[nodeId]: {
								label: 'updated'
							}
						};

						expect(
							reducer(
								defaultState.nodes,
								{
									diagramId,
									nodes,
									type: NODES_UPDATE
								}
							)
						).toStrictEqual({
							...defaultState.nodes,
							[diagramId]: {
								...defaultState.nodes[diagramId],
								[nodeId]: {
									...defaultState.nodes[diagramId][nodeId],
									...nodes[nodeId]
								}
							}
						});
					}
				);

				it(
					'update node position given delta change',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const nodes: Nodes = {
							[nodeId]: {
								x: 5,
								y: 5
							}
						};

						expect(
							reducer(
								defaultState.nodes,
								{
									diagramId,
									nodes,
									type: NODES_MOVE_DELTA
								}
							)
						).toStrictEqual({
							...defaultState.nodes,
							[diagramId]: {
								...defaultState.nodes[diagramId],
								[nodeId]: {
									...defaultState.nodes[diagramId][nodeId],
									x: defaultState.nodes[diagramId][nodeId].x + nodes[nodeId].x,
									y: defaultState.nodes[diagramId][nodeId].y + nodes[nodeId].y
								}
							}
						});
					}
				);
			}
		);

		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.nodes);
			}
		);
	}
);
