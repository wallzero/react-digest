import type {
	ReactText
} from 'react';
import {
	NODE_ADD,
	NODE_UPDATE,
	NODES_REMOVE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../nodeOrders';

describe(
	'Nodes reducer',
	(): void => {
		describe(
			'Add node',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = 'newnode';
						const node: DNode = {};

						expect(
							reducer(
								defaultState.nodeOrders,
								{
									diagramId,
									node,
									nodeId,
									type: NODE_ADD
								}
							)
						).toStrictEqual({
							...defaultState.nodeOrders,
							[diagramId]: [
								...defaultState.nodeOrders[diagramId],
								nodeId
							]
						});
					}
				);
			}
		);

		describe(
			'Remove node',
			(): void => {
				it(
					'manually',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const nodes = [
							nodeId
						];

						expect(
							reducer(
								defaultState.nodeOrders,
								{
									diagramId,
									nodes,
									type: NODES_REMOVE
								}
							)
						).toStrictEqual({
							...defaultState.nodeOrders,
							[diagramId]: defaultState.nodeOrders[diagramId].filter(
								(node): boolean => {
									return node !== nodeId;
								}
							)
						});
					}
				);
			}
		);

		describe(
			'Update node',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const node: DNode = {};

						expect(
							reducer(
								defaultState.nodeOrders,
								{
									diagramId,
									node,
									nodeId,
									type: NODE_UPDATE
								}
							)
						).toStrictEqual({
							...defaultState.nodeOrders,
							[diagramId]: [
								...defaultState.nodeOrders[diagramId].filter(
									(id): boolean => {
										return id !== nodeId;
									}
								),
								nodeId
							]
						});
					}
				);

				it(
					'skip update if only node',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '1';
						const node: DNode & {id: ReactText} = {
							id: defaultState.nodeOrders[diagramId][0]
						};

						expect(
							reducer(
								{
									[diagramId]: [
										defaultState.nodeOrders[diagramId][0]
									]
								},
								{
									diagramId,
									node,
									nodeId,
									type: NODE_UPDATE
								}
							)
						).toStrictEqual({
							[diagramId]: [
								defaultState.nodeOrders[diagramId][0]
							]
						});
					}
				);

				it(
					'update order when node updated',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const node = {};

						const updatedNodes = defaultState.nodeOrders[diagramId].filter(
							(id): boolean => {
								return id === nodeId;
							}
						);

						const remainingNodes = defaultState.nodeOrders[diagramId].filter(
							(id): boolean => {
								return id !== nodeId;
							}
						);

						expect(
							reducer(
								defaultState.nodeOrders,
								{
									diagramId,
									node,
									nodeId,
									type: NODE_UPDATE
								}
							)
						).toStrictEqual({
							[diagramId]: [
								...remainingNodes,
								...updatedNodes
							]
						});
					}
				);

				it(
					'skip update order node === 1',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '1';
						const node = {};

						expect(
							reducer(
								{
									[diagramId]: [
										defaultState.nodeOrders[diagramId][0]
									]
								},
								{
									diagramId,
									node,
									nodeId,
									type: NODE_UPDATE
								}
							)
						).toStrictEqual({
							[diagramId]: [
								defaultState.nodeOrders[diagramId][0]
							]
						});
					}
				);
			}
		);

		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.nodeOrders);
			}
		);
	}
);
