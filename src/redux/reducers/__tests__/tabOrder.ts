import {
	TAB_ADD,
	TAB_REMOVE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../tabOrder';

describe(
	'Tab order',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.tabOrder);
			}
		);

		it(
			'adding a tab',
			(): void => {
				const tab = {
					name: 'new tab'
				};
				const tabIndex = 1;

				expect(
					reducer(
						defaultState.tabOrder,
						{
							tab,
							type: TAB_ADD
						}
					)
				).toStrictEqual(defaultState.tabOrder.concat(tab.name));

				expect(
					reducer(
						defaultState.tabOrder,
						{
							tab,
							tabIndex,
							type: TAB_ADD
						}
					)
				).toStrictEqual([
					...defaultState.tabOrder.slice(
						0,
						tabIndex
					),
					tab.name,
					...defaultState.tabOrder.slice(tabIndex + 1)
				]);

				expect(
					reducer(
						defaultState.tabOrder,
						{
							type: TAB_ADD
						}
					)
				).toStrictEqual(defaultState.tabOrder);
			}
		);

		it(
			'removing a tab',
			(): void => {
				const tabIndex = 1;
				const name = Object.keys(defaultState.tabs)[tabIndex];

				expect(
					reducer(
						defaultState.tabOrder,
						{
							name,
							tabIndex,
							type: TAB_REMOVE
						}
					)
				).toStrictEqual(
					defaultState.tabOrder.filter(
						(tab): boolean => {
							return tab !== name;
						}
					)
				);
			}
		);

		it(
			'removing a tab that doesn\'t exist',
			(): void => {
				const name = 'sdfcsefsecvsdefs';
				const tabIndex = 1;

				expect(
					reducer(
						defaultState.tabOrder,
						{
							name,
							tabIndex,
							type: TAB_REMOVE
						}
					)
				).toStrictEqual(defaultState.tabOrder);
			}
		);
	}
);
