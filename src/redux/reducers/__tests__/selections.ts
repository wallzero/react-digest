import {
	DIAGRAM_REMOVE,
	NODES_REMOVE,
	SELECTION_ADD,
	SELECTION_CLEAR,
	SELECTION_REMOVE,
	SELECTION_TOGGLE,
	SELECTION_UPDATE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../selections';

describe(
	'Nodes reducer',
	(): void => {
		describe(
			'Add selection',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = 'newnode';

						expect(
							reducer(
								defaultState.selections,
								{
									diagramId,
									nodeId,
									type: SELECTION_ADD
								}
							)
						).toStrictEqual({
							...defaultState.selections,
							[diagramId]: {
								...defaultState.selections[diagramId],
								[nodeId]: true
							}
						});
					}
				);

				it(
					'toggle',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = 'newnode';

						const initial = reducer(
							defaultState.selections,
							{
								diagramId,
								nodeId,
								type: SELECTION_TOGGLE
							}
						);

						expect(initial).toStrictEqual({
							...defaultState.selections,
							[diagramId]: {
								...defaultState.selections[diagramId],
								[nodeId]: true
							}
						});

						const expected = {
							...defaultState.selections[diagramId]
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[nodeId];

						expect(
							reducer(
								initial,
								{
									diagramId,
									nodeId,
									type: SELECTION_TOGGLE
								}
							)
						).toStrictEqual({
							...defaultState.selections,
							[diagramId]: expected
						});
					}
				);
			}
		);

		describe(
			'Remove selection',
			(): void => {
				it(
					'only one selection',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = 'newnode';

						expect(
							reducer(
								{
									[diagramId]: {
										[nodeId]: true
									}
								},
								{
									diagramId,
									nodeId,
									type: SELECTION_REMOVE
								}
							)
						).toStrictEqual({
							...defaultState.selections,
							[diagramId]: {}
						});
					}
				);

				it(
					'clear',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];

						expect(
							reducer(
								defaultState.selections,
								{
									diagramId,
									type: SELECTION_CLEAR
								}
							)
						).toStrictEqual({
							...defaultState.selections,
							[diagramId]: {}
						});
					}
				);

				it(
					'via node removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = 'newnode';
						const selections = {
							[diagramId]: {
								[nodeId]: false
							}
						};

						const expected = {
							...defaultState.selections[diagramId]
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[nodeId];

						expect(
							reducer(
								selections,
								{
									diagramId,
									nodes: [
										nodeId
									],
									type: NODES_REMOVE
								}
							)
						).toStrictEqual({
							...defaultState.selections,
							[diagramId]: expected
						});
					}
				);

				it(
					'via diagram removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];

						const expected = {
							...defaultState.selections
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[diagramId];

						expect(
							reducer(
								defaultState.selections,
								{
									diagramId,
									type: DIAGRAM_REMOVE
								}
							)
						).toStrictEqual(expected);
					}
				);
			}
		);

		describe(
			'Update selection',
			(): void => {
				it(
					'raw update',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = 'newnode';
						const selections = {
							[diagramId]: {
								[nodeId]: false
							}
						};

						expect(
							reducer(
								selections,
								{
									diagramId,
									selections: selections[diagramId],
									type: SELECTION_UPDATE
								}
							)
						).toStrictEqual({
							...defaultState.selections,
							[diagramId]: {
								...defaultState.selections[diagramId],
								[nodeId]: false
							}
						});
					}
				);
			}
		);

		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.selections);
			}
		);
	}
);
