import type {
	ReactText
} from 'react';
import {
	DIAGRAM_REMOVE,
	EDGE_ADD,
	EDGES_REMOVE,
	NODES_REMOVE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../edgeSources';

describe(
	'Edge sources reducer',
	(): void => {
		describe(
			'Add edge',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = 'newedge';
						const edge: Edge = {
							source: 6,
							target: 6
						};

						expect(
							reducer(
								defaultState.edgeSources,
								{
									diagramId,
									edge,
									edgeId,
									type: EDGE_ADD
								}
							)
						).toStrictEqual({
							...defaultState.edgeSources,
							[diagramId]: {
								...defaultState.edgeSources[diagramId],
								[edge.source]: {
									[edgeId]: edge.target
								}
							}
						});
					}
				);
			}
		);
		describe(
			'Remove edge',
			(): void => {
				it(
					'manually',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = '4';
						const sourceId = '1';
						const edges: Edges = {
							[edgeId]: {
								source: sourceId,
								target: 1
							}
						};

						const {
							// eslint-disable-next-line @typescript-eslint/no-unused-vars
							[edgeId]: omit,
							...remaining
						} = defaultState.edgeSources[diagramId][sourceId];

						expect(
							reducer(
								defaultState.edgeSources,
								{
									diagramId,
									edges,
									type: EDGES_REMOVE
								}
							)
						).toStrictEqual({
							...defaultState.edgeSources,
							[diagramId]: {
								...defaultState.edgeSources[diagramId],
								[sourceId]: remaining
							}
						});
					}
				);

				it(
					'via node removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeSources = defaultState.edgeSources[diagramId];
						const sourceId = '1';
						const nodes: ReactText[] = [
							sourceId
						];

						expect(edgeSources[sourceId]).toBeDefined();

						const remaining = reducer(
							defaultState.edges,
							{
								diagramId,
								nodes,
								type: NODES_REMOVE
							}
						)[diagramId];

						expect(remaining[sourceId]).toBeUndefined();
					}
				);

				it(
					'via diagram removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];

						const expected = {
							...defaultState.edgeSources
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[diagramId];

						expect(
							reducer(
								defaultState.edgeSources,
								{
									diagramId,
									type: DIAGRAM_REMOVE
								}
							)
						).toStrictEqual(expected);
					}
				);
			}
		);

		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.edgeSources);
			}
		);
	}
);
