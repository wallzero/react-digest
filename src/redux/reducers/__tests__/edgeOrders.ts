import type {
	ReactText
} from 'react';
import {
	DIAGRAM_REMOVE,
	EDGE_ADD,
	EDGES_REMOVE,
	EDGE_UPDATE,
	NODE_UPDATE,
	NODES_REMOVE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../edgeOrders';

describe(
	'Edges reducer',
	(): void => {
		describe(
			'Add edge',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = 'newedge';
						const edge: Edge & {id: ReactText} = {
							id: edgeId,
							source: 6,
							target: 6
						};

						expect(
							reducer(
								defaultState.edgeOrders,
								{
									diagramId,
									edge,
									edgeId,
									type: EDGE_ADD
								}
							)
						).toStrictEqual({
							...defaultState.edgeOrders,
							[diagramId]: [
								...defaultState.edgeOrders[diagramId],
								edge
							]
						});
					}
				);
			}
		);

		describe(
			'Remove edge',
			(): void => {
				it(
					'manually',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = '2';
						const edges: Edges & {[key: string]: {id: ReactText}} = {
							[edgeId]: {
								id: edgeId,
								source: 1,
								target: 1
							}
						};

						expect(
							reducer(
								defaultState.edgeOrders,
								{
									diagramId,
									edges,
									type: EDGES_REMOVE
								}
							)
						).toStrictEqual({
							...defaultState.edgeOrders,
							[diagramId]: defaultState.edgeOrders[diagramId].filter(
								(edge): boolean => {
									return edge.id !== edgeId;
								}
							)
						});
					}
				);

				it(
					'via node removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edges = defaultState.edgeOrders[diagramId];
						const nodes: ReactText[] = [
							'1',
							'2'
						];

						expect(
							edges.find(
								(edge): boolean => {
									return nodes.includes(edge.source) || nodes.includes(edge.target);
								}
							)
						).toBeDefined();

						const remaining = reducer(
							defaultState.edgeOrders,
							{
								diagramId,
								nodes,
								type: NODES_REMOVE
							}
						)[diagramId];

						expect(
							remaining.find(
								(edge): boolean => {
									return nodes.includes(edge.source) || nodes.includes(edge.target);
								}
							)
						).toBeUndefined();
					}
				);

				it(
					'via diagram removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];

						const expected = {
							...defaultState.edgeOrders
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[diagramId];

						expect(
							reducer(
								defaultState.edgeOrders,
								{
									diagramId,
									type: DIAGRAM_REMOVE
								}
							)
						).toStrictEqual(expected);
					}
				);
			}
		);

		describe(
			'Update edge',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = '2';
						const edge: Edge & {id: ReactText} = {
							id: edgeId,
							source: 1,
							target: 1
						};

						expect(
							reducer(
								defaultState.edgeOrders,
								{
									diagramId,
									edge,
									edgeId,
									type: EDGE_UPDATE
								}
							)
						).toStrictEqual({
							...defaultState.edgeOrders,
							[diagramId]: [
								...defaultState.edgeOrders[diagramId].filter(
									(edg): boolean => {
										return edg.id !== edgeId;
									}
								),
								edge
							]
						});
					}
				);

				it(
					'skip update if only edge',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = '2';
						const edge = defaultState.edgeOrders[diagramId][0];

						expect(
							reducer(
								{
									[diagramId]: [
										defaultState.edgeOrders[diagramId][0]
									]
								},
								{
									diagramId,
									edge,
									edgeId,
									type: EDGE_UPDATE
								}
							)
						).toStrictEqual({
							[diagramId]: [
								defaultState.edgeOrders[diagramId][0]
							]
						});
					}
				);

				it(
					'update order when node updated',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const node = {};

						const updatedEdges = defaultState.edgeOrders[diagramId].filter(
							(edge): boolean => {
								return edge.source === nodeId || edge.target === nodeId;
							}
						);

						const remainingEdges = defaultState.edgeOrders[diagramId].filter(
							(edge): boolean => {
								return edge.source !== nodeId && edge.target !== nodeId;
							}
						);

						expect(
							reducer(
								defaultState.edgeOrders,
								{
									diagramId,
									node,
									nodeId,
									type: NODE_UPDATE
								}
							)
						).toStrictEqual({
							[diagramId]: [
								...remainingEdges,
								...updatedEdges
							]
						});
					}
				);

				it(
					'skip update order edge === 1',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const nodeId = '2';
						const node = {};

						expect(
							reducer(
								{
									[diagramId]: [
										defaultState.edgeOrders[diagramId][0]
									]
								},
								{
									diagramId,
									node,
									nodeId,
									type: NODE_UPDATE
								}
							)
						).toStrictEqual({
							[diagramId]: [
								defaultState.edgeOrders[diagramId][0]
							]
						});
					}
				);
			}
		);

		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.edgeOrders);
			}
		);
	}
);
