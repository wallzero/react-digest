import {
	DIAGRAM_ADD,
	DIAGRAM_REMOVE,
	DIAGRAM_UPDATE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer, {
	diagramDefaults
} from '../diagrams';

describe(
	'Diagrams reducer',
	(): void => {
		describe(
			'Add diagram',
			(): void => {
				it(
					'with parameters',
					(): void => {
						const diagramId = 3;
						const diagram: Diagram = {
							maxScale: 6,
							minScale: 0.2,
							scale: 2,
							xOffset: 50,
							yOffset: 100
						};

						expect(
							reducer(
								defaultState.diagrams,
								{
									diagram,
									diagramId,
									type: DIAGRAM_ADD
								}
							)
						).toStrictEqual({
							...defaultState.diagrams,
							[diagramId]: diagram
						});
					}
				);

				it(
					'without parameters (use defaults)',
					(): void => {
						const diagramId = 3;

						expect(
							reducer(
								defaultState.diagrams,
								{
									diagramId,
									type: DIAGRAM_ADD
								}
							)
						).toStrictEqual({
							...defaultState.diagrams,
							[diagramId]: diagramDefaults
						});
					}
				);
			}
		);

		describe(
			'Remove diagram',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = 'ComplexDiagram';

						const expected = {
							...defaultState.diagrams
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[diagramId];

						expect(
							reducer(
								defaultState.diagrams,
								{
									diagramId,
									type: DIAGRAM_REMOVE
								}
							)
						).toStrictEqual(expected);
					}
				);
			}
		);

		describe(
			'Update diagram',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = 'ComplexDiagram';
						const diagram: Diagram = {
							maxScale: 6,
							minScale: 0.2,
							scale: 2,
							xOffset: 50,
							yOffset: 100
						};

						expect(
							reducer(
								defaultState.diagrams,
								{
									diagram,
									diagramId,
									type: DIAGRAM_UPDATE
								}
							)
						).toStrictEqual({
							...defaultState.diagrams,
							[diagramId]: diagram
						});
					}
				);
			}
		);

		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.diagrams);
			}
		);
	}
);
