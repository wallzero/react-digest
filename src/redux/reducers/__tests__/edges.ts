import type {
	ReactText
} from 'react';
import {
	DIAGRAM_REMOVE,
	EDGE_ADD,
	EDGES_REMOVE,
	EDGE_UPDATE,
	NODES_REMOVE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../edges';

describe(
	'Edges reducer',
	(): void => {
		describe(
			'Add edge',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = 'newedge';
						const edge: Edge = {
							source: 6,
							target: 6
						};

						expect(
							reducer(
								defaultState.edges,
								{
									diagramId,
									edge,
									edgeId,
									type: EDGE_ADD
								}
							)
						).toStrictEqual({
							...defaultState.edges,
							[diagramId]: {
								...defaultState.edges[diagramId],
								[edgeId]: edge
							}
						});
					}
				);
			}
		);

		describe(
			'Remove edge',
			(): void => {
				it(
					'manually',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = '2';
						const edges: Edges = {
							[edgeId]: {
								source: 1,
								target: 1
							}
						};

						const expected = {
							...defaultState.edges[diagramId]
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[edgeId];

						expect(
							reducer(
								defaultState.edges,
								{
									diagramId,
									edges,
									type: EDGES_REMOVE
								}
							)
						).toStrictEqual({
							...defaultState.edges,
							[diagramId]: expected
						});
					}
				);

				it(
					'via node removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edges = defaultState.edges[diagramId];
						const nodes: ReactText[] = [
							'1',
							'2'
						];

						expect(
							Object.keys(edges).find(
								(edgeId): boolean => {
									const edge = edges[edgeId];

									return nodes.includes(edge.source) || nodes.includes(edge.target);
								}
							)
						).toBeDefined();

						const remaining = reducer(
							defaultState.edges,
							{
								diagramId,
								nodes,
								type: NODES_REMOVE
							}
						)[diagramId];

						expect(
							Object.keys(remaining).find(
								(edgeId): boolean => {
									const edge = remaining[edgeId];

									return nodes.includes(edge.source) || nodes.includes(edge.target);
								}
							)
						).toBeUndefined();
					}
				);

				it(
					'via diagram removal',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];

						const expected = {
							...defaultState.edges
						};

						// eslint-disable-next-line @typescript-eslint/no-dynamic-delete
						delete expected[diagramId];

						expect(
							reducer(
								defaultState.edges,
								{
									diagramId,
									type: DIAGRAM_REMOVE
								}
							)
						).toStrictEqual(expected);
					}
				);
			}
		);

		describe(
			'Update edge',
			(): void => {
				it(
					'default',
					(): void => {
						const diagramId = Object.keys(defaultState.diagrams)[0];
						const edgeId = '2';
						const edge: Edge = {
							source: 1,
							target: 1
						};

						expect(
							reducer(
								defaultState.edges,
								{
									diagramId,
									edge,
									edgeId,
									type: EDGE_UPDATE
								}
							)
						).toStrictEqual({
							...defaultState.edges,
							[diagramId]: {
								...defaultState.edges[diagramId],
								[edgeId]: edge
							}
						});
					}
				);
			}
		);

		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.edges);
			}
		);
	}
);
