import {
	DIAGRAM_REMOVE,
	NODE_ADD,
	NODES_MOVE_DELTA,
	NODE_UPDATE,
	NODES_REMOVE,
	NODES_UPDATE
} from '../actions/ActionTypes';
import type {
	ActionDiagramRemove
} from '../actions/diagrams';
import type {
	ActionNodeAdd,
	ActionNodesMoveDelta,
	ActionNodesRemove,
	ActionNodesUpdate,
	ActionNodeUpdate
} from '../actions/nodes';
import defaultState from '../defaultState';

const defaults: DNode = {
	x: 0,
	y: 0
};

export default function nodes (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.nodes,
	action: ActionDiagramRemove |
	ActionNodeAdd |
	ActionNodesMoveDelta |
	ActionNodesRemove |
	ActionNodesUpdate |
	ActionNodeUpdate
): DiagramNodes {
	switch (action.type) {
		case DIAGRAM_REMOVE: {
			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[action.diagramId]: omit,
				...remaining
			} = state;

			return remaining;
		}

		case NODE_ADD: {
			const {
				diagramId
			} = action;

			return {
				...state,
				[diagramId]: {
					...state[diagramId],
					[action.nodeId]: {
						...defaults,
						...action.node
					}
				}
			};
		}

		case NODES_REMOVE: {
			const {
				diagramId
			} = action;

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: action.nodes.reduce<Nodes>(
					(
						updated,
						nodeId
					): Nodes => {
						const {
							// eslint-disable-next-line @typescript-eslint/no-unused-vars
							[nodeId]: omit,
							...remaining
						} = updated;

						return remaining;
					},
					state[diagramId]
				)
			};
		}

		case NODES_MOVE_DELTA: {
			const {
				diagramId
			} = action;
			const diagramNodes = state[diagramId];

			return {
				...state,
				[diagramId]: {
					...diagramNodes,
					// eslint-disable-next-line unicorn/no-array-reduce,unicorn/prefer-object-from-entries
					...Object.keys(action.nodes).reduce<Nodes>(
						(
							updated,
							nodeId
						): Nodes => {
							const node = diagramNodes[nodeId];
							const delta = action.nodes[nodeId];

							updated[nodeId] = {
								...node,
								x: node.x + delta.x,
								y: node.y + delta.y
							};

							return updated;
						},
						{}
					)
				}
			};
		}

		case NODES_UPDATE: {
			const {
				diagramId
			} = action;
			const diagramNodes = state[diagramId];

			return {
				...state,
				[action.diagramId]: {
					...diagramNodes,
					// eslint-disable-next-line unicorn/no-array-reduce,unicorn/prefer-object-from-entries
					...Object.keys(action.nodes).reduce<Nodes>(
						(
							updated,
							nodeId
						): Nodes => {
							updated[nodeId] = {
								...diagramNodes[nodeId],
								...action.nodes[nodeId]
							};

							return updated;
						},
						{}
					)
				}
			};
		}

		case NODE_UPDATE: {
			const {
				diagramId
			} = action;
			const {
				nodeId
			} = action;

			return {
				...state,
				[diagramId]: {
					...state[diagramId],
					[nodeId]: {
						...state[diagramId][nodeId],
						...action.node
					}
				}
			};
		}

		default: {
			return state;
		}
	}
}
