import {
	DIAGRAM_REMOVE,
	EDGE_ADD,
	EDGES_REMOVE,
	EDGE_UPDATE,
	NODES_REMOVE,
	NODE_UPDATE
} from '../actions/ActionTypes';
import type {
	ActionDiagramRemove
} from '../actions/diagrams';
import type {
	ActionEdgeAdd,
	ActionEdgesRemove,
	ActionEdgeUpdate
} from '../actions/edges';
import type {
	ActionNodesRemove,
	ActionNodeUpdate
} from '../actions/nodes';
import defaultState from '../defaultState';

export default function edgeOrders (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.edgeOrders,
	action: ActionDiagramRemove |
	ActionEdgeAdd |
	ActionEdgesRemove |
	ActionEdgeUpdate |
	ActionNodesRemove |
	ActionNodeUpdate
): EdgeOrders {
	switch (action.type) {
		case DIAGRAM_REMOVE: {
			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[action.diagramId]: omit,
				...remaining
			} = state;

			return remaining;
		}

		case EDGE_ADD: {
			const {
				diagramId
			} = action;
			const order = state[diagramId];

			return {
				...state,
				[diagramId]: [
					...order,
					{
						id: action.edgeId,
						source: action.edge.source,
						target: action.edge.target
					}
				]
			};
		}

		case EDGES_REMOVE: {
			const {
				diagramId
			} = action;

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: Object.keys(action.edges).reduce<EdgeOrder>(
					(
						order,
						edgeId
					): EdgeOrder => {
						const index = order.findIndex(
							(edge): boolean => {
								return edge.id === edgeId;
							}
						);

						return [
							...order.slice(
								0,
								index
							),
							...order.slice(index + 1)
						];
					},
					state[diagramId]
				)
			};
		}

		case EDGE_UPDATE: {
			const {
				diagramId
			} = action;
			const order = state[diagramId];

			const index = order.findIndex(
				(edge): boolean => {
					return edge.id === action.edgeId;
				}
			);

			if (
				index === order.length - 1 ||
				action.edge.target === order[index + 1].target ||
				action.edge.source === order[index + 1].source
			) {
				return state;
			}

			return {
				...state,
				[diagramId]: [
					...order.slice(
						0,
						index
					),
					...order.slice(index + 1),
					{
						id: action.edgeId,
						source: action.edge.source,
						target: action.edge.target
					}
				]
			};
		}

		case NODES_REMOVE: {
			const {
				diagramId
			} = action;
			const order = state[diagramId];

			return {
				...state,
				[diagramId]: order.filter(
					(edge): boolean => {
						if (
							action.nodes.includes(edge.source) ||
							action.nodes.includes(edge.target)
						) {
							return false;
						}

						return true;
					}
				)
			};
		}

		case NODE_UPDATE: {
			const {
				diagramId
			} = action;
			const order = state[diagramId];
			const end: EdgeOrder = [];

			return {
				...state,
				[diagramId]: order.filter(
					(
						edge,
						index
					): boolean => {
						if (index === order.length - 1) {
							return true;
						}

						if (
							action.nodeId === edge.source ||
							action.nodeId === edge.target
						) {
							end.push({
								id: edge.id,
								source: edge.source,
								target: edge.target
							});

							return false;
						}

						return true;
					}
				).concat(end)
			};
		}

		default: {
			return state;
		}
	}
}
