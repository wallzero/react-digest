import {
	DIAGRAM_REMOVE,
	EDGE_ADD,
	EDGES_REMOVE,
	EDGE_UPDATE,
	NODES_REMOVE
} from '../actions/ActionTypes';
import type {
	ActionDiagramRemove
} from '../actions/diagrams';
import type {
	ActionEdgeAdd,
	ActionEdgesRemove,
	ActionEdgeUpdate
} from '../actions/edges';
import type {
	ActionNodesRemove
} from '../actions/nodes';
import defaultState from '../defaultState';

export default function edges (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.edges,
	action: ActionDiagramRemove |
	ActionEdgeAdd |
	ActionEdgesRemove |
	ActionEdgeUpdate |
	ActionNodesRemove
): DiagramEdges {
	switch (action.type) {
		case DIAGRAM_REMOVE: {
			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[action.diagramId]: omit,
				...remaining
			} = state;

			return remaining;
		}

		case EDGE_ADD: {
			const {
				diagramId
			} = action;

			return {
				...state,
				[diagramId]: {
					...state[diagramId],
					[action.edgeId]: action.edge
				}
			};
		}

		case EDGES_REMOVE: {
			const {
				diagramId
			} = action;

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: Object.keys(action.edges).reduce<Edges>(
					(
						updated,
						edgeId
					): Edges => {
						const {
							// eslint-disable-next-line @typescript-eslint/no-unused-vars
							[edgeId]: omit,
							...remaining
						} = updated;

						return remaining;
					},
					state[diagramId]
				)
			};
		}

		case EDGE_UPDATE: {
			const {
				diagramId
			} = action;

			return {
				...state,
				[diagramId]: {
					...state[diagramId],
					[action.edgeId]: {
						source: action.edge.source,
						target: action.edge.target
					}
				}
			};
		}

		case NODES_REMOVE: {
			const {
				diagramId
			} = action;
			const diagram = state[diagramId];
			const {
				nodes
			} = action;

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: Object.keys(diagram).reduce<Edges>(
					(
						updated,
						edgeId
					): Edges => {
						if (
							nodes.includes(updated[edgeId].source) ||
							nodes.includes(updated[edgeId].target)
						) {
							const {
								// eslint-disable-next-line @typescript-eslint/no-unused-vars
								[edgeId]: omit,
								...remaining
							} = updated;

							return remaining;
						}

						return updated;
					},
					diagram
				)
			};
		}

		default: {
			return state;
		}
	}
}
