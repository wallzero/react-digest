import isNil from 'lodash.isnil';
import type {
	Orientation
} from 'ui-router-react-digest';
import {
	ORIENTATION
} from '../actions/ActionTypes';
import type {
	ActionOrientation
} from '../actions/orientation';
import defaultState from '../defaultState';

export default (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.orientation,
	action: ActionOrientation
): Orientation => {
	switch (action.type) {
		case ORIENTATION: {
			if (action.position === 'left') {
				return 'left';
			}

			if (action.position === 'right') {
				return 'right';
			}

			if (isNil(action.position)) {
				return state === 'left' ?
					'right' :
					'left';
			}

			return state;
		}

		default: {
			return state;
		}
	}
};
