import {
	DIAGRAM_ADD,
	DIAGRAM_REMOVE,
	DIAGRAM_UPDATE
} from '../actions/ActionTypes';
import type {
	ActionDiagramAdd,
	ActionDiagramRemove,
	ActionDiagramUpdate
} from '../actions/diagrams';
import defaultState from '../defaultState';

const diagramDefaults: Diagram = {
	maxScale: 2,
	minScale: 0.5,
	scale: 1,
	xOffset: 0,
	yOffset: 0
};

export default function diagrams (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.diagrams,
	action: ActionDiagramAdd |
	ActionDiagramRemove |
	ActionDiagramUpdate
): Diagrams {
	switch (action.type) {
		case DIAGRAM_ADD: {
			return {
				...state,
				[action.diagramId]: {
					...diagramDefaults,
					...action.diagram
				}
			};
		}

		case DIAGRAM_REMOVE: {
			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[action.diagramId]: omit,
				...remaining
			} = state;

			return remaining;
		}

		case DIAGRAM_UPDATE: {
			return {
				...state,
				[action.diagramId]: {
					...state[action.diagramId],
					...action.diagram
				}
			};
		}

		default: {
			return state;
		}
	}
}

export {
	diagramDefaults
};
