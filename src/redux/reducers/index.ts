import {
	combineReducers
} from 'redux';
import diagrams from './diagrams';
import drawerSettings from './drawerSettings';
import drawers from './drawers';
import edgeOrders from './edgeOrders';
import edgeSources from './edgeSources';
import edgeTargets from './edgeTargets';
import edges from './edges';
import miscellaneous from './miscellaneous';
import nodeOrders from './nodeOrders';
import nodes from './nodes';
import orientation from './orientation';
import selections from './selections';
import tabOrder from './tabOrder';
import tabSettings from './tabSettings';
import tabs from './tabs';

const reducers = {
	diagrams,
	drawers,
	drawerSettings,
	edgeOrders,
	edges,
	edgeSources,
	edgeTargets,
	miscellaneous,
	nodeOrders,
	nodes,
	orientation,
	selections,
	tabOrder,
	tabs,
	tabSettings
};

export default combineReducers(reducers);
