import {
	DIAGRAM_REMOVE,
	EDGE_ADD,
	EDGES_REMOVE,
	NODES_REMOVE
} from '../actions/ActionTypes';
import type {
	ActionDiagramRemove
} from '../actions/diagrams';
import type {
	ActionEdgeAdd,
	ActionEdgesRemove
} from '../actions/edges';
import type {
	ActionNodesRemove
} from '../actions/nodes';
import defaultState from '../defaultState';

export default function edgeTargets (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.edgeTargets,
	action: ActionDiagramRemove |
	ActionEdgeAdd |
	ActionEdgesRemove |
	ActionNodesRemove
): DiagramEdgeTargets {
	switch (action.type) {
		case DIAGRAM_REMOVE: {
			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[action.diagramId]: omit,
				...remaining
			} = state;

			return remaining;
		}

		case EDGE_ADD: {
			const {
				diagramId
			} = action;
			const diagram = state[diagramId];
			const targetId = action.edge.target;

			return {
				...state,
				[diagramId]: {
					...diagram,
					[targetId]: {
						...diagram[targetId],
						[action.edgeId]: action.edge.source
					}
				}
			};
		}

		case EDGES_REMOVE: {
			const {
				diagramId
			} = action;

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: Object.keys(action.edges).reduce<EdgeTargets>(
					(
						updated,
						edgeId
					): EdgeTargets => {
						const targetId = action.edges[edgeId].target;
						const {
							// eslint-disable-next-line @typescript-eslint/no-unused-vars
							[edgeId]: omit,
							...remaining
						} = updated[targetId];

						updated[targetId] = remaining;

						return updated;
					},
					{
						...state[diagramId]
					}
				)
			};
		}

		case NODES_REMOVE: {
			const {
				diagramId
			} = action;

			// eslint-disable-next-line unicorn/no-array-reduce
			const diagram = action.nodes.reduce<EdgeTargets>(
				(
					edges,
					nodeId
				): EdgeTargets => {
					const {
						// eslint-disable-next-line @typescript-eslint/no-unused-vars
						[nodeId]: omit,
						...remaining
					} = edges;

					return remaining;
				},
				state[diagramId]
			);

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: Object.keys(diagram).reduce<EdgeTargets>(
					(
						filteredTargets,
						targetId
					): EdgeTargets => {
						return {
							...filteredTargets,
							// eslint-disable-next-line unicorn/no-array-reduce
							[targetId]: Object.keys(filteredTargets[targetId]).reduce<EdgeTarget>(
								(
									edges,
									edgeId
								): EdgeTarget => {
									if (action.nodes.includes(edges[edgeId])) {
										const {
											// eslint-disable-next-line @typescript-eslint/no-unused-vars
											[edgeId]: omit,
											...remaining
										} = edges;

										return remaining;
									}

									return edges;
								},
								filteredTargets[targetId]
							)
						};
					},
					diagram
				)
			};
		}

		default: {
			return state;
		}
	}
}
