import {
	DIAGRAM_REMOVE,
	EDGE_ADD,
	EDGES_REMOVE,
	NODES_REMOVE
} from '../actions/ActionTypes';
import type {
	ActionDiagramRemove
} from '../actions/diagrams';
import type {
	ActionEdgeAdd,
	ActionEdgesRemove
} from '../actions/edges';
import type {
	ActionNodesRemove
} from '../actions/nodes';
import defaultState from '../defaultState';

export default function edgeSources (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.edgeSources,
	action: ActionDiagramRemove |
	ActionEdgeAdd |
	ActionEdgesRemove |
	ActionNodesRemove
): DiagramEdgeSources {
	switch (action.type) {
		case DIAGRAM_REMOVE: {
			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[action.diagramId]: omit,
				...remaining
			} = state;

			return remaining;
		}

		case EDGE_ADD: {
			const {
				diagramId
			} = action;
			const diagram = state[diagramId];
			const sourceId = action.edge.source;

			return {
				...state,
				[diagramId]: {
					...diagram,
					[sourceId]: {
						...diagram[sourceId],
						[action.edgeId]: action.edge.target
					}
				}
			};
		}

		case EDGES_REMOVE: {
			const {
				diagramId
			} = action;

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: Object.keys(action.edges).reduce<EdgeSources>(
					(
						updated,
						edgeId
					): EdgeSources => {
						const sourceId = action.edges[edgeId].source;

						const {
							// eslint-disable-next-line @typescript-eslint/no-unused-vars
							[edgeId]: omit,
							...remaining
						} = updated[sourceId];

						updated[sourceId] = remaining;

						return updated;
					},
					{
						...state[diagramId]
					}
				)
			};
		}

		case NODES_REMOVE: {
			const {
				diagramId
			} = action;

			// eslint-disable-next-line unicorn/no-array-reduce
			const diagram = action.nodes.reduce<EdgeSources>(
				(
					edges,
					nodeId
				): EdgeSources => {
					const {
						// eslint-disable-next-line @typescript-eslint/no-unused-vars
						[nodeId]: omit,
						...remaining
					} = edges;

					return remaining;
				},
				state[diagramId]
			);

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: Object.keys(diagram).reduce<EdgeSources>(
					(
						filteredSources,
						sourceId
					): EdgeSources => {
						return {
							...filteredSources,
							// eslint-disable-next-line unicorn/no-array-reduce
							[sourceId]: Object.keys(filteredSources[sourceId]).reduce<EdgeSource>(
								(
									edges,
									edgeId
								): EdgeSource => {
									if (action.nodes.includes(edges[edgeId])) {
										const {
											// eslint-disable-next-line @typescript-eslint/no-unused-vars
											[edgeId]: omit,
											...remaining
										} = edges;

										return remaining;
									}

									return edges;
								},
								filteredSources[sourceId]
							)
						};
					},
					diagram
				)
			};
		}

		default: {
			return state;
		}
	}
}
