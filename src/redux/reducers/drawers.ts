import defaultState from '../defaultState';
import type State from '../state';

export default function drawers (
	state = defaultState.drawers
): State.drawers {
	return state;
}
