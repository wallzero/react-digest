import {
	DIAGRAM_REMOVE,
	NODES_REMOVE,
	SELECTION_ADD,
	SELECTION_CLEAR,
	SELECTION_REMOVE,
	SELECTION_TOGGLE,
	SELECTION_UPDATE
} from '../actions/ActionTypes';
import type {
	ActionDiagramRemove
} from '../actions/diagrams';
import type {
	ActionNodesRemove
} from '../actions/nodes';
import type {
	ActionSelectionAdd,
	ActionSelectionClear,
	ActionSelectionRemove,
	ActionSelectionUpdate,
	ActionSelectionToggle
} from '../actions/selections';
import defaultState from '../defaultState';

export default function selections (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.selections,
	action: ActionDiagramRemove | ActionNodesRemove | ActionSelectionAdd | ActionSelectionClear | ActionSelectionRemove | ActionSelectionToggle | ActionSelectionUpdate
): DiagramSelections {
	switch (action.type) {
		case DIAGRAM_REMOVE: {
			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[action.diagramId]: omit,
				...remaining
			} = state;

			return remaining;
		}

		case NODES_REMOVE: {
			const {
				diagramId
			} = action;

			return {
				...state,
				// eslint-disable-next-line unicorn/no-array-reduce
				[diagramId]: action.nodes.reduce<Selections>(
					(
						nodes,
						nodeId
					): Selections => {
						const {
							// eslint-disable-next-line @typescript-eslint/no-unused-vars
							[nodeId]: omit,
							...remaining
						} = nodes;

						return remaining;
					},
					state[diagramId]
				)
			};
		}

		case SELECTION_ADD: {
			const {
				diagramId
			} = action;

			return {
				...state,
				[diagramId]: {
					...state[diagramId],
					[action.nodeId]: true
				}
			};
		}

		case SELECTION_CLEAR: {
			return {
				...state,
				[action.diagramId]: {}
			};
		}

		case SELECTION_REMOVE: {
			const {
				diagramId
			} = action;
			const {
				nodeId
			} = action;

			if (
				state[diagramId][nodeId] &&
				Object.keys(state[diagramId]).length === 1
			) {
				return {
					...state,
					[diagramId]: {}
				};
			}

			const {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				[nodeId]: omit,
				...remaining
			} = state[diagramId];

			return {
				...state,
				[diagramId]: remaining
			};
		}

		case SELECTION_TOGGLE: {
			const {
				diagramId
			} = action;
			const {
				nodeId
			} = action;

			if (state[diagramId][nodeId]) {
				const {
					// eslint-disable-next-line @typescript-eslint/no-unused-vars
					[nodeId]: omit,
					...remaining
				} = state[diagramId];

				return {
					...state,
					[diagramId]: remaining
				};
			}

			return {
				...state,
				[diagramId]: {
					...state[diagramId],
					[nodeId]: true
				}
			};
		}

		case SELECTION_UPDATE: {
			return {
				...state,
				[action.diagramId]: action.selections
			};
		}

		default: {
			return state;
		}
	}
}
