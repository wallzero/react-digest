import {
	loadState
} from '../localStorage';

const DATE = Date.now().toString();

process.env = {
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-expect-error
	BUILD: {
		DATE
	}
};

describe(
	'Redux state from local storage',
	(): void => {
		describe(
			'Load state',
			(): void => {
				it(
					'undefined local storage',
					(): void => {
						expect.assertions(1);

						const state = loadState();
						expect(state).toBeNull();
					}
				);

				it(
					'error parsing local storage',
					async (): Promise<void> => {
						expect.hasAssertions();

						try {
							await Promise.resolve(loadState());
						} catch (error) {
							// eslint-disable-next-line jest/no-conditional-expect
							expect(error).toBeNull();
						} finally {
							expect(true).toBe(true);
						}
					}
				);

				it(
					'empty local storage',
					async (): Promise<void> => {
						expect.assertions(1);

						const state = await Promise.resolve(loadState());
						expect(state).toBeNull();
					}
				);

				it(
					'local storage without matching date',
					async (): Promise<void> => {
						expect.assertions(1);

						process.env = {
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							BUILD: {
								DATE: '0'
							}
						};

						const state = await Promise.resolve(loadState());
						expect(state).toBeNull();
					}
				);
			}
		);
	}
);
