import type {
	Orientation
} from 'ui-router-react-digest';

const defaults = {
	drawerDocked: true,
	drawerDrag: false,
	drawerHover: true,
	drawerIndex: 0,
	drawerOpen: false,
	drawers: [
		{
			name: 'tools',
			state: {
				params: {
					title: 'Tools'
				}
			}
		},
		{
			name: 'settings',
			state: {
				params: {
					title: 'Settings'
				}
			}
		}
	],
	drawerWidth: 300,
	onDrawerOpenToggle: (): void => {},
	onDrawerSelect: (): void => {},
	onTabSelect: (): void => {},
	orientation: 'left' as Orientation,
	rootState: {
		name: 'uurd'
	},
	tabIndex: 0,
	tabOrder: [
		'404',
		'components',
		'diagram'
	],
	tabs: {
		'404': {
			hidden: true,
			name: '404',
			state: {
				params: {
					tenured: true,
					title: '404',
					type: '404'
				},
				sticky: true
			},
			swipe: false
		},
		components: {
			name: 'components',
			state: {
				deepStateRedirect: true,
				params: {
					tenured: true,
					title: 'Components',
					type: 'components'
				},
				sticky: true
			},
			swipe: false
		},
		diagram: {
			name: 'diagram',
			state: {
				deepStateRedirect: true,
				params: {
					tenured: true,
					title: 'Diagram',
					type: 'diagram'
				},
				sticky: true
			},
			swipe: false
		}
	},
	title: 'React Digest'
};

export const {
	drawerDocked,
	drawerDrag,
	drawerHover,
	drawerIndex,
	drawerOpen,
	drawers,
	drawerWidth,
	onDrawerOpenToggle,
	onDrawerSelect,
	onTabSelect,
	orientation,
	rootState,
	tabIndex,
	tabOrder,
	tabs,
	title
} = defaults;

export default defaults;
