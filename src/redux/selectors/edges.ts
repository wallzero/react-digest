import {
	createCachedSelector
} from 're-reselect';
import type {
	ReactText
} from 'react';
import type State from '../state';
import {
	selectNodeId
} from './nodes';

const selectEdges = (
	state: State.All,
	{
		diagramId
	}: {diagramId: ReactText}
): Edges => {
	return state.edges[diagramId];
};

const selectEdgeSources = (
	state: State.All,
	{
		diagramId
	}: {
		diagramId: ReactText;
	}
): EdgeSources => {
	return state.edgeSources[diagramId];
};

const selectEdgeSourcesFromNode = (
	state: State.All,
	{
		diagramId,
		nodeId
	}: {
		diagramId: ReactText;
		nodeId: ReactText;
	}
): EdgeSource => {
	return selectEdgeSources(
		state,
		{
			diagramId
		}
	)[nodeId] || {};
};

const selectEdgeTargets = (
	state: State.All,
	{
		diagramId
	}: {
		diagramId: ReactText;
	}
): EdgeTargets => {
	return state.edgeTargets[diagramId];
};

const selectEdgeTargetsFromNode = (
	state: State.All,
	{
		diagramId,
		nodeId
	}: {
		diagramId: ReactText;
		nodeId: ReactText;
	}
): EdgeTarget => {
	return selectEdgeTargets(
		state,
		{
			diagramId
		}
	)[nodeId] || {};
};

const selectEdgesFromNodeSources = createCachedSelector(
	[
		selectNodeId,
		selectEdgeSourcesFromNode
	],
	(
		nodeId,
		EdgeSource
	): Edges => {
		// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
		return Object.keys(EdgeSource).reduce<Edges>(
			(
				sourceEdges,
				edgeId
			): Edges => {
				sourceEdges[edgeId] = {
					source: nodeId,
					target: EdgeSource[edgeId]
				};

				return sourceEdges;
			},
			{}
		);
	}
)(
	(
		_state: State.All,
		props: {
			nodeId: ReactText;
		}
	): ReactText => {
		return props.nodeId;
	}
);

const selectEdgesFromNodeTargets = createCachedSelector(
	[
		selectNodeId,
		selectEdgeTargetsFromNode
	],
	(
		nodeId,
		EdgeTarget
	): Edges => {
		// eslint-disable-next-line unicorn/prefer-object-from-entries,unicorn/no-array-reduce
		return Object.keys(EdgeTarget).reduce<Edges>(
			(
				targetEdges,
				edgeId
			): Edges => {
				targetEdges[edgeId] = {
					source: EdgeTarget[edgeId],
					target: nodeId
				};

				return targetEdges;
			},
			{}
		);
	}
)(
	(
		_state: State.All,
		props: {
			nodeId: ReactText;
		}
	): ReactText => {
		return props.nodeId;
	}
);

const selectEdgesFromNode = createCachedSelector(
	[
		selectEdgesFromNodeSources,
		selectEdgesFromNodeTargets
	],
	(
		SourceEdges,
		TargetEdges
	): Edges => {
		return {
			...SourceEdges,
			...TargetEdges
		};
	}
)(
	(
		_state: State.All,
		props: {
			nodeId: ReactText;
		}
	): ReactText => {
		return props.nodeId;
	}
);

export {
	selectEdgeSources,
	selectEdgeSourcesFromNode,
	selectEdgeTargets,
	selectEdgeTargetsFromNode,
	selectEdges,
	selectEdgesFromNode,
	selectEdgesFromNodeSources,
	selectEdgesFromNodeTargets
};
