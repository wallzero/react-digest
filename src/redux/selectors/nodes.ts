import type {
	ReactText
} from 'react';
import type State from '../state';

/*
	Helper function when nodeId is
	needed but can't be passed
*/
const selectNodeId = (
	_state: State.All,
	props: {nodeId: ReactText}
): ReactText => {
	return props.nodeId;
};

const selectNodes = (
	state: State.All,
	props: {diagramId: ReactText}
): Nodes => {
	return state.nodes[props.diagramId];
};

export {
	selectNodeId,
	selectNodes
};
