import defaultState from '../../defaultState';
import {
	selectNodeId,
	selectNodes
} from '../nodes';

describe(
	'Node selectors',
	(): void => {
		it(
			'return given nodeId',
			(): void => {
				const nodeId = 'anything';

				expect(
					selectNodeId(
						defaultState,
						{
							nodeId
						}
					)
				).toStrictEqual(nodeId);
			}
		);

		it(
			'return nodes given diagramId',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];

				expect(
					selectNodes(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual(defaultState.nodes[diagramId]);
			}
		);
	}
);
