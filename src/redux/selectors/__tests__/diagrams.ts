import defaultState from '../../defaultState';
import {
	selectDiagramId,
	selectDiagram,
	selectDiagramEdges,
	selectDiagramNodes,
	selectDiagrams
} from '../diagrams';

describe(
	'Diagram selectors',
	(): void => {
		it(
			'return given diagramId',
			(): void => {
				const diagramId = 'anything';

				expect(
					selectDiagramId(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual(diagramId);
			}
		);

		it(
			'return all diagrams',
			(): void => {
				expect(
					selectDiagrams(
						defaultState
					)
				).toStrictEqual(defaultState.diagrams);
			}
		);

		it(
			'return diagram',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];

				expect(
					selectDiagram(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual(defaultState.diagrams[diagramId]);
			}
		);

		it(
			'return diagram nodes',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];

				expect(
					selectDiagramNodes(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual(defaultState.nodeOrders[diagramId]);
			}
		);

		it(
			'return no nodes if diagram does not exist',
			(): void => {
				const diagramId = 'awdawdawdawdad';

				expect(
					selectDiagramNodes(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual([]);
			}
		);

		it(
			'return diagram edges',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];

				expect(
					selectDiagramEdges(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual(defaultState.edgeOrders[diagramId]);
			}
		);

		it(
			'return no edges if diagram does not exist',
			(): void => {
				const diagramId = 'awdawdawdawdad';

				expect(
					selectDiagramEdges(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual([]);
			}
		);
	}
);
