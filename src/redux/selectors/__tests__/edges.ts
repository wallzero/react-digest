import defaultState from '../../defaultState';
import {
	selectEdges,
	selectEdgesFromNode,
	selectEdgesFromNodeSources,
	selectEdgesFromNodeTargets,
	selectEdgeSources,
	selectEdgeSourcesFromNode,
	selectEdgeTargets,
	selectEdgeTargetsFromNode
} from '../edges';

describe(
	'Edge selectors',
	(): void => {
		it(
			'return all edges',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];

				expect(
					selectEdges(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual(defaultState.edges[diagramId]);
			}
		);

		it(
			'return edge sources',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];

				expect(
					selectEdgeSources(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual(defaultState.edgeSources[diagramId]);
			}
		);

		it(
			'return edge sources from node',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = '1';

				expect(
					selectEdgeSourcesFromNode(
						defaultState,
						{
							diagramId,
							nodeId
						}
					)
				).toStrictEqual(defaultState.edgeSources[diagramId][nodeId]);
			}
		);

		it(
			'return no sources if node does not exist',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = 'qwerty';

				expect(
					selectEdgeSourcesFromNode(
						defaultState,
						{
							diagramId,
							nodeId
						}
					)
				).toStrictEqual({});
			}
		);

		it(
			'return edge targets',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];

				expect(
					selectEdgeTargets(
						defaultState,
						{
							diagramId
						}
					)
				).toStrictEqual(defaultState.edgeTargets[diagramId]);
			}
		);

		it(
			'return edge targets from node',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = '2';

				expect(
					selectEdgeTargetsFromNode(
						defaultState,
						{
							diagramId,
							nodeId
						}
					)
				).toStrictEqual(defaultState.edgeTargets[diagramId][nodeId]);
			}
		);

		it(
			'return no targets if node does not exist',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = 'qwerty';

				expect(
					selectEdgeTargetsFromNode(
						defaultState,
						{
							diagramId,
							nodeId
						}
					)
				).toStrictEqual({});
			}
		);

		it(
			'return edges of node sources',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = '1';

				expect(
					Object.keys(
						selectEdgesFromNodeSources(
							defaultState,
							{
								diagramId,
								nodeId
							}
						)
					)
				).toStrictEqual(
					Object.keys(defaultState.edgeSources[diagramId][nodeId])
				);
			}
		);

		it(
			'return edges of node targets',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = '2';

				expect(
					Object.keys(
						selectEdgesFromNodeTargets(
							defaultState,
							{
								diagramId,
								nodeId
							}
						)
					)
				).toStrictEqual(
					Object.keys(defaultState.edgeTargets[diagramId][nodeId])
				);
			}
		);

		it(
			'return edges of node',
			(): void => {
				const diagramId = Object.keys(defaultState.diagrams)[0];
				const nodeId = '3';

				expect(
					Object.keys(
						selectEdgesFromNode(
							defaultState,
							{
								diagramId,
								nodeId
							}
						)
					)
				).toStrictEqual(
					Object.keys({
						...defaultState.edgeSources[diagramId][nodeId],
						...defaultState.edgeTargets[diagramId][nodeId]
					})
				);
			}
		);
	}
);
