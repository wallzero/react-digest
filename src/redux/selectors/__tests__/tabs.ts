import defaultState from '../../defaultState';
import {
	selectTabs
} from '../tabs';

describe(
	'Tab selectors',
	(): void => {
		it(
			'return ordered array of all tabs',
			(): void => {
				expect(
					selectTabs(defaultState)
				).toHaveLength(defaultState.tabOrder.length);
			}
		);
	}
);
