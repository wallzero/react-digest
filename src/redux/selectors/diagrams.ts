import type {
	ReactText
} from 'react';
import type State from '../state';

/*
	Helper function when diagramId is
	needed but can't be passed
*/
const selectDiagramId = (
	_state: State.All,
	props: {diagramId: ReactText}
): ReactText => {
	return props.diagramId;
};

const selectDiagrams = (
	state: State.All
): Diagrams => {
	return state.diagrams;
};

const selectDiagram = (
	state: State.All,
	props: {diagramId: ReactText}
): Diagram => {
	return selectDiagrams(state)[props.diagramId];
};

const selectDiagramNodes = (
	state: State.All,
	props: {diagramId: ReactText}
): NodeOrder => {
	return state.nodeOrders[props.diagramId] || [];
};

const selectDiagramEdges = (
	state: State.All,
	props: {diagramId: ReactText}
): EdgeOrder => {
	return state.edgeOrders[props.diagramId] || [];
};

export {
	selectDiagram,
	selectDiagramEdges,
	selectDiagramId,
	selectDiagramNodes,
	selectDiagrams
};
