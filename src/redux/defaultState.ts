import diagrams from 'src/redux/data/diagrams';
import edgeOrders from 'src/redux/data/edgeOrders';
import edgeSources from 'src/redux/data/edgeSources';
import edgeTargets from 'src/redux/data/edgeTargets';
import edges from 'src/redux/data/edges';
import nodeOrders from 'src/redux/data/nodeOrders';
import nodes from 'src/redux/data/nodes';
import selections from 'src/redux/data/selections';
import {
	drawerDocked,
	drawerDrag,
	drawerHover,
	drawerIndex,
	drawerOpen,
	drawers,
	drawerWidth,
	orientation,
	tabIndex,
	tabOrder,
	tabs,
	title
} from 'src/redux/defaults';
import type State from 'src/redux/state';

const defaultState: State.All = {
	diagrams,
	drawers,
	drawerSettings: {
		docked: drawerDocked,
		drag: drawerDrag,
		hover: drawerHover,
		index: drawerIndex,
		open: drawerOpen,
		width: drawerWidth
	},
	edgeOrders,
	edges,
	edgeSources,
	edgeTargets,
	miscellaneous: {
		shortName: 'redig',
		title
	},
	nodeOrders,
	nodes,
	orientation,
	selections,
	tabOrder,
	tabs,
	tabSettings: {
		index: tabIndex
	}
};

export default defaultState;
