import type {
	FunctionComponent,
	ReactElement
} from 'react';

const Loading: FunctionComponent = (): ReactElement => {
	return null;
};

export default Loading;
