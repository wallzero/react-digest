import {
	styled
} from '@mui/system';
import React, {
	Children,
	useMemo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import ReactMasonryCSS from 'react-masonry-css';
import MasonryCards from '../MasonryCards';
import styles from './Masonry.css';

const COLUMN_WIDTH = 400;

const breakpointColumns = {
	default: 1
};

const StyledReactMasonryCSS = styled(ReactMasonryCSS)(
	({
		theme
	}) => {
		return {
			'& > .masonryGridColumn': {
				paddingLeft: theme.spacing(3)
			},
			marginLeft: theme.spacing(-3),
			padding: theme.spacing(3)
		};
	}
);

const Masonry: FunctionComponent<MasonryProps> = (
	{
		children,
		width
	}: MasonryProps
): ReactElement => {
	breakpointColumns.default = useMemo(
		() => {
			return width &&
				Math.floor(width / COLUMN_WIDTH) ?
				Math.floor(width / COLUMN_WIDTH) :
				1;
		},
		[
			width
		]
	);

	const style = useMemo(
		() => {
			const actualWidth = Math.trunc(width / COLUMN_WIDTH) * COLUMN_WIDTH;

			return {
				width: Math.max(
					actualWidth,
					COLUMN_WIDTH
				)
			};
		},
		[
			width
		]
	);

	const cards = Children.map(
		children,
		(child): ReactElement => {
			return (
				<MasonryCards
					key={child.key}
					title={child.key}
				>
					{child}
				</MasonryCards>
			);
		}
	);

	return (
		width ?
			// eslint-disable-next-line @typescript-eslint/no-extra-parens
			(
				<StyledReactMasonryCSS
					breakpointCols={breakpointColumns}
					className=''
					columnClassName={`masonryGridColumn ${styles.masonryGridColumn}`}
					style={style}
					styleName='masonry-grid'
				>
					{cards}
				</StyledReactMasonryCSS>
			) :
			// eslint-disable-next-line @typescript-eslint/no-extra-parens
			(
				<div />
			)
	);
};

export default Masonry;

export type MasonryProps = {
	children?: ReactElement | ReactElement[];
	width?: number;
};
