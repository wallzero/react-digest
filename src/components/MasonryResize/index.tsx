import React from 'react';
import type {
	FunctionComponent,
	LegacyRef,
	ReactElement
} from 'react';
import {
	withResizeDetector
} from 'react-resize-detector';
import Masonry from './Masonry';
import './MasonryResize.css';

const MasonryResize: FunctionComponent<MasonryResizeProps> = (
	{
		children,
		targetRef,
		width
	}: MasonryResizeProps
): ReactElement => {
	return (
		<div
			ref={targetRef}
			styleName='content'
		>
			<Masonry
				width={width}
			>
				{children}
			</Masonry>
		</div>
	);
};

export default withResizeDetector<MasonryResizeProps>(
	MasonryResize,
	{
		refreshMode: 'throttle',
		refreshOptions: {
			leading: true,
			trailing: true
		},
		refreshRate: 250
	}
);

export type MasonryResizeProps = {
	children?: ReactElement | ReactElement[];
	height: number;
	targetRef?: LegacyRef<HTMLDivElement>;
	width: number;
};
