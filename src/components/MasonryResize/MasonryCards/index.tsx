import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Divider from '@mui/material/Divider';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Card.css';

const styles = {
	card: {
		marginBottom: 3
	}
};

const MasonryCards: FunctionComponent<MasonryCardsProps> = (
	{
		children,
		className = '',
		title
	}: MasonryCardsProps
): ReactElement => {
	return (
		<Card
			className={className}
			styleName='card'
			sx={styles.card}
		>
			<CardHeader title={title} />
			<Divider />
			<CardContent>
				{children}
			</CardContent>
		</Card>
	);
};

export default MasonryCards;

export type MasonryCardsProps = {
	children?: ReactElement | ReactElement[];
	className?: string;
	styleName?: string;
	title?: number | string;
};
