import type {
	FunctionComponent,
	ReactElement
} from 'react';

const Error: FunctionComponent = (): ReactElement => {
	return null;
};

export default Error;
