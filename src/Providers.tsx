import CssBaseline from '@mui/material/CssBaseline';
import {
	createTheme,
	StyledEngineProvider,
	ThemeProvider
} from '@mui/material/styles';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import {
	Provider
} from 'react-redux';
import App from 'src/containers/App';
import store from 'src/redux/store';
import './vendor';

const theme = createTheme();

const Providers: FunctionComponent = (): ReactElement => {
	return (
		<StyledEngineProvider injectFirst>
			<ThemeProvider theme={theme}>
				<Provider store={store}>
					<CssBaseline />
					<App />
				</Provider>
			</ThemeProvider>
		</StyledEngineProvider>
	);
};

export default Providers;
