declare module '*.css';
declare module '*.scss';

declare var System: {
	import: any
};

declare module '*.json' {
	const value: any;
	export default value;
}

declare var BUILD: {
	DATE: string;
};

declare var PACKAGE: {
	DESCRIPTION: string,
	NAME: string,
	TITLE: string,
	VERSION: string
};

interface PathType {}
interface HookMatchCriterion {}

interface Miscellaneous {
	shortName: string;
	title: string;
}

interface DrawerSettings {
	docked: boolean;
	drag: boolean;
	hover: boolean;
	index: number;
	open: boolean;
	width: string | number;
}

interface TabSettings {
	index: number;
}

interface DNode {
	height?: number;
	label?: string;
	type?: string;
	width?: number;
	x?: number;
	y?: number;
}

interface Nodes {
	[key: string]: DNode;
}

interface DiagramNodes {
	[key: string]: Nodes;
}

type NodeOrder = (string | number)[];

interface NodeOrders {
	[key: string]: NodeOrder
}

interface Edge {
	line?: 'curve' | 'direct' | 'step';
	source: string | number;
	target: string | number;
}

interface Edges {
	[key: string]: Omit<Edge, 'id'>;
}

interface DiagramEdges {
	[key: string]: Edges;
}

type EdgeOrder = (Pick<Edge, 'source' | 'target'> & {id: string | number})[];

interface EdgeOrders {
	[key: string]: EdgeOrder
}

interface EdgeSource {
	[key: string]: string | number;
}

interface EdgeSources {
	[key: string]: EdgeSource;
}

interface DiagramEdgeSources {
	[key: string]: EdgeSources;
}

interface EdgeTarget {
	[key: string]: string | number;
}

interface EdgeTargets {
	[key: string]: EdgeTarget;
}

interface DiagramEdgeTargets {
	[key: string]: EdgeTargets;
}

interface Selections {
	[key: string]: boolean;
}

interface DiagramSelections {
	[key: string]: Selections;
}

interface Diagram {
	maxScale: number;
	minScale: number;
	scale: number;
	xOffset: number;
	yOffset: number;
}

interface Diagrams {
	[key: string]: Diagram;
}
