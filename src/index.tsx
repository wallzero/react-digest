import React, {
	lazy,
	StrictMode,
	Suspense
} from 'react';
import {
	render
} from 'react-dom';
import {
	ErrorBoundary
} from 'react-error-boundary';
import Error from 'src/components/Error';
import Loading from 'src/components/Loading';
import './vendor';

if (process.env.NODE_ENV === 'development') {
	/*
	// eslint-disable-next-line promise/prefer-await-to-then
	import('react-dom').then(
		(ReactDOM): void => {
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			return import('@axe-core/react').then( // eslint-disable-line promise/prefer-await-to-then
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				(axe: any): void => {
					return axe(
						React,
						ReactDOM,
						5_000,
						{
							rules: [{
								enabled: false,
								id: 'region'
							}]
						}
					);
				}
			// eslint-disable-next-line promise/prefer-await-to-then
			).catch((): void => {
				//
			});
		}
	// eslint-disable-next-line promise/prefer-await-to-then
	).catch((): void => {
		//
	});
	*/

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	import('@welldone-software/why-did-you-render').then((whyDidYouRender: any): void => {
		return whyDidYouRender(
			React,
			{
				collapseGroups: true,
				groupByComponent: true,
				include: [
					/^Node$/u
				]
			}
		);
	}).catch((): void => {
		//
	});
}

const Providers = lazy(
	// eslint-disable-next-line @typescript-eslint/consistent-type-imports
	(): Promise<typeof import('src/Providers')> => {
		return import(

			/* webpackChunkName: "Providers" */
			/* webpackPrefetch: true */
			'src/Providers'
		);
	}
);

const rootElement = document.querySelector('#app') as HTMLElement ||
	document.createElement('div') as HTMLElement;

render(
	<StrictMode>
		<ErrorBoundary
			FallbackComponent={Error}
		>
			<Suspense
				fallback={<Loading />}
			>
				<Providers />
			</Suspense>
		</ErrorBoundary>
	</StrictMode>,
	rootElement
);

export {
	render as default
} from 'react-dom';
